/*!
@file Sprite.h
@brief UIなど
*/

#pragma once
#include "stdafx.h"

namespace basecross{

	// 背景クラス
	class BackGround : public GameObject
	{
		// 大きさ、位置
		Vec2 m_Scale;
		Vec2 m_Position;
		// 透明化処理
		bool m_Trace;

		// テクスチャ
		wstring m_TX;
	public :
		// 構築と破棄
		BackGround(
			const shared_ptr<Stage>& StagePtr,
			const bool Trans,
			const Vec2& Scale,
			const Vec2& Position,
			const wstring& TX = WstringKey::tx_BG
		);
		~BackGround();

		// 初期化
		virtual void OnCreate()override;

	};

	// タイトルスプライト
	class TitleSprite : public GameObject
	{
		// 大きさ、位置
		Vec2 m_Scale;
		Vec2 m_Position;
		// 透明化処理するか
		bool m_Trace;
		// 透明処理
		float m_alpha;
		// テクスチャ
		wstring m_TX;


	public :
		// 構築と破棄
		TitleSprite(
			const shared_ptr<Stage>& StagePtr,
			const bool Trans,
			const Vec2& Scale,
			const Vec2& Position,
			const wstring& TX = WstringKey::tx_Title
		);
		~TitleSprite();

		// 初期化
		virtual void OnCreate()override;
	};

	// スタートスプライト
	class StartSprite : public GameObject
	{
		// 大きさ、位置
		Vec3 m_Scale;
		Vec3 m_Position;
		// 透明化処理
		bool m_Trace;

		// テクスチャ
		wstring m_TX;

		// scale用
		float m_TotalTime = 0.0f;

		// 小さくなったフラグ
		bool m_smallFlg = false;
		// 大きくなったフラグ
		bool m_bigFlg = false;

		// 動き終わったフラグ
		bool m_EndFlg = false;

	public:
		// 構築と破棄
		StartSprite(
			const shared_ptr<Stage>& StagePtr,
			const bool Trans,
			const Vec3& Scale,
			const Vec3& Position,
			const wstring& TX = WstringKey::tx_StartButton
		);
		~StartSprite();

		// 初期化
		virtual void OnCreate()override;
		// 更新
		virtual void OnUpdate()override;

		void ResetTotalTime();
		// ゲッターセッター
		bool GetEndFlg();
	};


	// SpriteStudioの著作権表記アニメーション
	class SSCopyRight : public SS5ssae
	{
		// 大きさ、回転、位置
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;

	public:
		// 構築と破棄
		SSCopyRight(
			const shared_ptr<Stage>& StagePtr,
			const wstring& BaseDir,
			const wstring& XmlfileName,
			const wstring& StartAnimeName,
			const Vec3& Scale,
			const Vec3& Rotation,
			const Vec3& Position
		);
		virtual ~SSCopyRight();

		// 初期化
		virtual void OnCreate() override;

		// 更新
		virtual void OnUpdate()override;
	};

	// 汎用スプライト
	class Sprite : public GameObject
	{
		// 大きさ、回転、位置
		Vec2 m_Scale;
		Vec2 m_Position;

		// 透明
		bool m_Trace;

		// テクスチャ
		wstring m_TX;

		// レイヤー
		int m_Layer = 0;

		// 色(入力なしなら白)
		Col4 m_Color;

	public:
		// 構築と破棄
		Sprite(
			const shared_ptr<Stage>& StagePtr,
			const bool Trace,
			const Vec2& Scale,
			const Vec2& Position,
			const wstring& Tx,
			const int& Layer = 0,
			const Col4& Color = Col4(1.0f, 1.0f, 1.0f, 1.0f));
		virtual ~Sprite();

		// 初期化
		virtual void OnCreate()override;

	};

	// ミッション用ゲージ(スプライト)
	class MissionGage : public GameObject
	{
		// 大きさ、位置
		Vec2 m_Scale;
		Vec2 m_Position;

		// 透過するか
		bool m_Trace;
		// テクスチャ
		wstring m_TX;
		// 白
		Col4 White = Col4(1.0f, 1.0f, 1.0f, 1.0f);

		// 画像半分
		const float HalfSize = 0.5f;
		// 画像幅最大値
		float m_MaxSize = 1.0f;
		// ひとつ前の頂点
		vector<VertexPositionColorTexture> m_Vertices;
		// インデックス
		vector<uint16_t> m_Indices;
		// UV用
		Rect2D<float> m_AnimTip[1][1];
		// 自身の配列の要素
		int m_AnimTipCol = 0;
		int m_AnimTipRow = 0;

		int m_BeforeGage = 0;

		// 画像更新
		void SpriteUpdate();

	public :
		// 構築と破棄
		MissionGage(
			const shared_ptr<Stage>& StagePtr,
			const bool Trace,
			const Vec2& Scale,
			const Vec2& Position,
			const wstring& Tx);
		virtual ~MissionGage();

		// 初期化
		virtual void OnCreate()override;
		// 更新
		virtual void OnUpdate()override;

	};

	// ミッション用ゲージのフレーム(スプライト)
	class MissionGageFrame : public GameObject
	{
		// 大きさ、位置
		Vec2 m_Scale;
		Vec2 m_Position;

		// 透過するか
		bool m_Trace;
		// テクスチャ
		wstring m_TX;
		// 白
		Col4 White = Col4(1.0f, 1.0f, 1.0f, 1.0f);

		int m_Layer = 0;

		// 画像半分
		const float HalfSize = 0.5f;
		// 画像幅最大値
		float m_MaxSize = 1.0f;
		// 頂点
		vector<VertexPositionColorTexture> m_Vertices;
		// インデックス
		vector<uint16_t> m_Indices;
		// UV用
		Rect2D<float> m_AnimTip[1][1];
		// 自身の配列の要素
		int m_AnimTipCol = 0;
		int m_AnimTipRow = 0;

	public:
		// 構築と破棄
		MissionGageFrame(
			const shared_ptr<Stage>& StagePtr,
			const bool Trace,
			const Vec2& Scale,
			const Vec2& Position,
			const wstring& Tx,
			const int& Layer = 0);
		virtual ~MissionGageFrame();

		// 初期化
		virtual void OnCreate()override;

	};

	// ミッション演出用スプライト(PCTStaticDrawになりました)
	class MissionSprite : public GameObject
	{
		// 大きさ、回転、位置
		Vec3 m_Scale;
		Vec3 m_Position;
		Vec3 m_Rotation = Vec3(0.0f, 0.0f, 0.0f);
		Vec3 m_BeforeRot = Vec3(0.0f, 0.0f, 0.0f);

		// まわった回数
		int m_turnNum = 0;


		// テクスチャ
		wstring m_TX;

		// レイヤー
		int m_Layer = 0;

		// 色(入力なしなら白)
		Col4 m_Color;

		// degrad変換
		float ConvertRadian(float deg)
		{
			return deg * (XM_PI / 180.0f);
		}
		float ConvertDegree(float rad)
		{
			return rad * (180.0f / XM_PI);
		}

	public:
		// 構築と破棄
		MissionSprite(
			const shared_ptr<Stage>& StagePtr,
			const Vec3& Scale,
			const Vec3& Rotation,
			const Vec3& Position,
			const wstring& Tx,
			const int& Layer = 0,
			const Col4& Color = Col4(1.0f, 1.0f, 1.0f, 1.0f));
		virtual ~MissionSprite();

		// 初期化
		virtual void OnCreate()override;

		// 更新
		virtual void OnUpdate()override;

	};

	// スコア用（数字用）スプライト
	class ScoreSprite : public GameObject
	{
		// 桁数
		UINT m_NumOfDigits;

		// 大きさ、位置
		Vec2 m_Scale;
		Vec2 m_Position;

		// 透過するか
		bool m_Trace;
		// テクスチャ
		wstring m_TX;
		
		// 画像半分
		const float HalfSize = 0.5f;

		// バックアップ用頂点データ
		vector<VertexPositionColorTexture> m_BackupVertices;
		// インデックス
		vector<uint16_t> m_Indices;
	public:
		// 構築と破棄
		ScoreSprite(
			const shared_ptr<Stage>& StagePtr,
			const bool Trace,
			const Vec2& Scale,
			const Vec2& Position,
			const wstring& Tx,
			const UINT& NumOfDigits
		);
		virtual ~ScoreSprite();

		// 初期化
		virtual void OnCreate()override;

		// 更新
		//virtual void OnUpdate()override;

	};

	// 切り替え演出
	// 仮白フェード
	class Fade : public GameObject
	{
		// 大きさ、位置
		Vec2 m_Scale;
		Vec2 m_Position;

		// 透過するか
		bool m_Trace;
		// テクスチャ
		wstring m_TX;

		// 色
		Col4 m_Color = Col4(1.0f, 1.0f, 1.0f, 1.0f);
		// 透明度
		float m_Alpha = 1.0f;
		// フェードの速さ
		float m_Speed = 1.5f;
	public :
		// 構築と破棄
		Fade(
			const shared_ptr<Stage>& StagePtr,
			const bool Trace,
			const Vec2& Scale,
			const Vec2& Position,
			const wstring& Tx,
			const float& Alpha = 1.0f);
		virtual ~Fade();

		// 初期化
		virtual void OnCreate()override;
		// 更新
		virtual void OnUpdate() override;

		// フェードイン
		void FadeIn();
		// フェードアウト
		void FadeOut();
	};
}
//end basecross
