/*!
@file Sprite.cpp
@brief UIなど実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross{

	// 背景クラス
	// 構築と破棄
	BackGround::BackGround(
		const shared_ptr<Stage>& StagePtr,
		const bool Trace,
		const Vec2& Scale,
		const Vec2& Position,
		const wstring& TX)
		:
		GameObject(StagePtr),
		m_Trace(Trace),
		m_Scale(Scale),
		m_Position(Position),
		m_TX(TX)
	{
	}
	BackGround::~BackGround(){}

	// 初期化
	void BackGround::OnCreate()
	{

		// タグをつける
		AddTag(WstringKey::tag_BG);

		// 描画するテクスチャの設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);// 白
		float halfSize = 0.5f;
		vector<VertexPositionColorTexture> vertices =
		{
			{Vec3(-halfSize, +halfSize, 0), white, Vec2(0.0f, 0.0f)},
			{Vec3(+halfSize, +halfSize, 0), white, Vec2(1.0f, 0.0f)},
			{Vec3(-halfSize, -halfSize, 0), white, Vec2(0.0f, 1.0f)},
			{Vec3(+halfSize, -halfSize, 0), white, Vec2(1.0f, 1.0f)}
		};
		vector<uint16_t> indices = {
			0, 1, 2,
			1, 3, 2
		};
		// 透明処理
		SetAlphaActive(m_Trace);
		// 大きさ、回転、位置
		auto ptrTransform = GetComponent<Transform>();
		ptrTransform->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		ptrTransform->SetPosition(m_Position.x, m_Position.y, 0.0f);

		// 頂点とインデックスを指定してスプライト作成
		auto ptrDraw = this->AddComponent<PCTSpriteDraw>(vertices, indices);
		ptrDraw->SetTextureResource(m_TX);
		ptrDraw->SetDepthStencilState(DepthStencilState::None);
		// レイヤー
		SetDrawLayer(-1);

	}

	// タイトルスプライト
	// 構築と破棄
	TitleSprite::TitleSprite(
		const shared_ptr<Stage>& StagePtr,
		const bool Trans,
		const Vec2& Scale,
		const Vec2& Position,
		const wstring& TX)
		:
		GameObject(StagePtr),
		m_Trace(Trans),
		m_Scale(Scale),
		m_Position(Position),
		m_TX(TX)
	{
	}
	TitleSprite::~TitleSprite() {}

	// 初期化
	void TitleSprite::OnCreate()
	{
		// 描画するテクスチャの設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);// 白
		float halfSize = 0.5f;
		vector<VertexPositionColorTexture> vertices =
		{
			{Vec3(-halfSize, +halfSize, 0), white, Vec2(0.0f, 0.0f)},
			{Vec3(+halfSize, +halfSize, 0), white, Vec2(1.0f, 0.0f)},
			{Vec3(-halfSize, -halfSize, 0), white, Vec2(0.0f, 1.0f)},
			{Vec3(+halfSize, -halfSize, 0), white, Vec2(1.0f, 1.0f)}
		};
		vector<uint16_t> indices = {
			0, 1, 2,
			1, 3, 2
		};
		// 透明処理
		SetAlphaActive(m_Trace);
		// 大きさ、回転、位置
		auto ptrTransform = GetComponent<Transform>();
		ptrTransform->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		ptrTransform->SetPosition(m_Position.x, m_Position.y, 0.0f);

		// 頂点とインデックスを指定してスプライト作成
		auto ptrDraw = this->AddComponent<PCTSpriteDraw>(vertices, indices);
		ptrDraw->SetTextureResource(m_TX);
		ptrDraw->SetDepthStencilState(DepthStencilState::None);
		// レイヤー
		SetDrawLayer(9);

	}


	// スタートスプライト
	// 構築と破棄
	StartSprite::StartSprite(
		const shared_ptr<Stage>& StagePtr,
		const bool Trans,
		const Vec3& Scale,
		const Vec3& Position,
		const wstring& TX)
		:
		GameObject(StagePtr),
		m_Trace(Trans),
		m_Scale(Scale),
		m_Position(Position),
		m_TX(TX)
	{
	}
	StartSprite::~StartSprite() {}

	// 初期化
	void StartSprite::OnCreate()
	{
		// 描画するテクスチャの設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);// 白
		float halfSize = 0.5f;
		vector<VertexPositionColorTexture> vertices =
		{
			{Vec3(-halfSize, +halfSize, 0), white, Vec2(0.0f, 0.0f)},
			{Vec3(+halfSize, +halfSize, 0), white, Vec2(1.0f, 0.0f)},
			{Vec3(-halfSize, -halfSize, 0), white, Vec2(0.0f, 1.0f)},
			{Vec3(+halfSize, -halfSize, 0), white, Vec2(1.0f, 1.0f)}
		};
		vector<uint16_t> indices = {
			0, 1, 2,
			1, 3, 2
		};
		//vector<VertexPositionColorTexture> vertex =
		//{
		//	{Vec3(-halfSize, +halfSize, 0), white, Vec2(0,0)},
		//	{Vec3(+halfSize, +halfSize, 0), white, Vec2(1,0)},
		//	{Vec3(-halfSize, -halfSize, 0), white, Vec2(0,1)},
		//	{Vec3(+halfSize, -halfSize, 0), white, Vec2(1,1)}
		//};
		//vector<uint16_t> indices =
		//{
		//	0, 1, 2,
		//	1, 3, 2
		//};
		// 透明処理
		SetAlphaActive(m_Trace);
		// 大きさ、回転、位置
		auto ptrTransform = GetComponent<Transform>();
		ptrTransform->SetScale(m_Scale);
		ptrTransform->SetPosition(m_Position);

		//衝突判定
		auto ptrCol = AddComponent<CollisionObb>();
		ptrCol->SetAfterCollision(AfterCollision::None);
		//ptrCol->SetDrawActive(true);

		// 頂点とインデックスを指定してスプライト作成
		//auto ptrDraw = this->AddComponent<PCTSpriteDraw>(vertices, indices);
		//ptrDraw->SetTextureResource(m_TX);
		//ptrDraw->SetDepthStencilState(DepthStencilState::None);

		Mat4x4 mat;
		mat.affineTransformation(
			Vec3(2.0f),
			Vec3(0.0f),
			Vec3(0.0f),
			Vec3(0.0f)
		);

		auto ptrStaticDraw = AddComponent<PCTStaticDraw>();
		ptrStaticDraw->CreateOriginalMesh(vertices, indices);
		ptrStaticDraw->SetOriginalMeshUse(true);
		ptrStaticDraw->SetTextureResource(m_TX);
		ptrStaticDraw->SetMeshToTransformMatrix(mat);
		ptrStaticDraw->SetDepthStencilState(DepthStencilState::None);
		SetAlphaActive(true);
		// レイヤー
		SetDrawLayer(9);

		// 文字列をつける
		auto ptrString = AddComponent<StringSprite>();
		ptrString->SetText(L"");
		ptrString->SetTextRect(Rect2D<float>(16.0f, 160.0f, 640.0f, 480.0f));
	}

	// 更新
	void StartSprite::OnUpdate()
	{
		auto ptrTrans = GetComponent<Transform>();
		auto scale = ptrTrans->GetScale();
		m_TotalTime += App::GetApp()->GetElapsedTime();


		// ステージがTitleなら
		if (auto stage = dynamic_pointer_cast<TitleStage>(GetStage()))
		{
			if (stage->GetChangeFlg())
			{
				// 仮アニメーション
				if (!m_smallFlg && scale.length() > 2.5f)
				{
					//ptrTrans->SetScale(Vec3(scale.x - m_TotalTime * 1.0f, scale.y - m_TotalTime * 1.0f, 0.0f));

				}
				else if (!m_bigFlg && (scale.length() <= 1.5f || scale.length() < 2.5f))
				{
					//ptrTrans->SetScale(Vec3(scale.x + m_TotalTime * 1.0f, scale.y + m_TotalTime * 1.0f, 0.0f));
					m_smallFlg = true;
				}
				else if (m_smallFlg)
				{
					m_bigFlg = true;
					if (scale.length() >= 5.0f) {
						//ptrTrans->SetScale(Vec3(scale.x - m_TotalTime * 5.0f, scale.y - m_TotalTime * 5.0f, 0.0f));
					}
					else
					{
					}
				}
						m_EndFlg = true;

			}
			else
			{
				// 拡大縮小をsinカーブで繰り返し
				//ptrTrans->SetScale(Vec3(scale.x + sinf(m_TotalTime * 3.0f) / 2.0f, scale.y + sinf(m_TotalTime * 3.0f)/2.0f, 0.0f));
			}
		}
	}

	// TotalTimeリセット
	void StartSprite::ResetTotalTime()
	{
		m_TotalTime = 0.0f;
	}

	// ゲッターセッター
	bool StartSprite::GetEndFlg()
	{
		return m_EndFlg;
	}


	// SpriteStudioの著作権表記アニメーション
	// 構築と破棄
	SSCopyRight::SSCopyRight(
		const shared_ptr<Stage>& StagePtr,
		const wstring& BaseDir,
		const wstring& XmlfileName,
		const wstring& StartAnimeName,
		const Vec3& Scale,
		const Vec3& Rotation,
		const Vec3& Position
	) :
		SS5ssae(StagePtr, BaseDir, XmlfileName, StartAnimeName),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	SSCopyRight::~SSCopyRight(){}

	// 初期化
	void SSCopyRight::OnCreate()
	{
		// 透明処理
		SetAlphaActive(false);
		// 大きさ、回転、位置
		auto ptrTransform = GetComponent<Transform>();
		ptrTransform->SetScale(m_Scale);
		ptrTransform->SetRotation(m_Rotation);
		ptrTransform->SetPosition(m_Position);

		Mat4x4 spanMat; // モデルとトランスフォームの間の差分行列
		//上からスケール、回転(origin)、回転(vec)、ポジション
		spanMat.affineTransformation(
			Vec3(0.15f, 0.15f, 0.15f),
			Vec3(0.0f),
			Vec3(0.0f),
			Vec3(0.0f)
		);
		// 親クラスの初期化呼び出し
		SS5ssae::OnCreate();
		SetToAnimeMatrix(spanMat);


		//値は秒あたりのフレーム数
		SetFps(30.0f);
		// アニメーションをループさせるか
		SetLooped(false);
		// レイヤー
		SetDrawLayer(2);
	}

	// 更新
	void SSCopyRight::OnUpdate()
	{
		// アニメーションの更新
		float elapsedTime = App::GetApp()->GetElapsedTime();
		UpdateAnimeTime(elapsedTime);
	}



	// 汎用スプライト
	// 構築と破棄
	Sprite::Sprite(
		const shared_ptr<Stage>& StagePtr,
		const bool Trace,
		const Vec2& Scale,
		const Vec2& Position,
		const wstring& Tx,
		const int& Layer,
		const Col4& Color)
		:
		GameObject(StagePtr),
		m_Trace(Trace),
		m_Scale(Scale),
		m_Position(Position),
		m_TX(Tx),
		m_Layer(Layer),
		m_Color(Color)
	{
	}
	Sprite::~Sprite() {}

	// 初期化
	void Sprite::OnCreate()
	{
		// 描画するテクスチャの設定
		float halfSize = 0.5f;
		vector<VertexPositionColorTexture> vertices =
		{
			{Vec3(-halfSize, +halfSize, 0), m_Color, Vec2(0.0f, 0.0f)},
			{Vec3(+halfSize, +halfSize, 0), m_Color, Vec2(1.0f, 0.0f)},
			{Vec3(-halfSize, -halfSize, 0), m_Color, Vec2(0.0f, 1.0f)},
			{Vec3(+halfSize, -halfSize, 0), m_Color, Vec2(1.0f, 1.0f)}
		};
		vector<uint16_t> indices = {
			0, 1, 2,
			1, 3, 2
		};
		// 透明処理
		SetAlphaActive(m_Trace);
		// 大きさ、位置
		auto ptrTransform = GetComponent<Transform>();
		ptrTransform->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		ptrTransform->SetPosition(m_Position.x, m_Position.y, 0.0f);

		// レイヤー
		SetDrawLayer(m_Layer);

		// 頂点とインデックスを指定してスプライト作成
		auto ptrDraw = this->AddComponent<PCTSpriteDraw>(vertices, indices);
		ptrDraw->SetTextureResource(m_TX);
		ptrDraw->SetDepthStencilState(DepthStencilState::None);
	}

	// ミッションゲージ
	// 構築と破棄
	MissionGage::MissionGage(
		const shared_ptr<Stage>& StagePtr,
		const bool Trace,
		const Vec2& Scale,
		const Vec2& Position,
		const wstring& Tx)
		:
		GameObject(StagePtr),
		m_Trace(Trace),
		m_Scale(Scale),
		m_Position(Position),
		m_TX(Tx)
	{
	}
	MissionGage::~MissionGage() {}

	// 初期化
	void MissionGage::OnCreate()
	{
		// 大きさ、位置
		auto ptrTrans = GetComponent<Transform>();
		ptrTrans->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		ptrTrans->SetPosition(m_Position.x, m_Position.y, 0.0f);

		auto gm = GameManager::GetInstance();
		float gageRatio = (float)gm->GetMission() / (float)gm->GetMaxMission();
		m_MaxSize = 1.0f * gageRatio;

		// rightにgageRatioをかける

		// アニメチップ作成
		float tipWidth = 1.0f;
		float tipHeight = 1.0f;
		for (int col = 0; col < 1; col++)
		{
			for (int row = 0; row < 1; row++)
			{
				// 幅
				m_AnimTip[col][row].left = tipWidth * row;							// 左
				m_AnimTip[col][row].right = m_AnimTip[col][row].left + tipWidth;	// 右
				// 高さ
				m_AnimTip[col][row].top = tipHeight * col;							// 上
				m_AnimTip[col][row].bottom = m_AnimTip[col][row].top + tipHeight;	// 下
			}
		}

		// 描画するテクスチャの設定
		m_Vertices =
		{
			{Vec3( -0.0f,      -HalfSize, 0.0f), White, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},	// 左上
			{Vec3( +m_MaxSize, -HalfSize, 0.0f), White, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right * gageRatio,m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},	// 右上
			{Vec3( -0.0f,      +HalfSize, 0.0f), White, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},	// 左下
			{Vec3( +m_MaxSize, +HalfSize, 0.0f), White, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right * gageRatio, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}	// 右下

		};
		m_Indices = {
			0, 1, 2,
			1, 3, 2
		};

		// 頂点とインデックスを指定してスプライトの生成
		auto ptrDraw = AddComponent<PCTSpriteDraw>(m_Vertices, m_Indices);
		ptrDraw->SetTextureResource(m_TX);
		ptrDraw->SetDepthStencilState(DepthStencilState::None);
	
		// 透明処理
		SetAlphaActive(m_Trace);
		SetDrawLayer(12);
	}

	// 更新
	void MissionGage::OnUpdate()
	{
		SpriteUpdate();
	}

	// 画像更新
	void MissionGage::SpriteUpdate()
	{
		// ゲージが切り替わったなら更新,変わってないなら帰る
		auto gm = GameManager::GetInstance();
		if (m_BeforeGage == gm->GetMission())return;

		// ゲージの比率
		float gageRatio = (float)gm->GetMission() / (float)gm->GetMaxMission();
		m_MaxSize = 1.0f * gageRatio;

		// rightにgageRatioをかける

		// 新しい頂点の設定
		vector<VertexPositionColorTexture> newVertices =
		{
			{Vec3(-0.0f,      -HalfSize, 0.0f), White, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},	// 左上
			{Vec3(+m_MaxSize, -HalfSize, 0.0f), White, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right * gageRatio,m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},	// 右上
			{Vec3(-0.0f,      +HalfSize, 0.0f), White, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},	// 左下
			{Vec3(+m_MaxSize, +HalfSize, 0.0f), White, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right * gageRatio, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}	// 右下
		};
		// 更新
		auto ptrDraw = GetComponent<PCTSpriteDraw>();
		ptrDraw->UpdateVertices<VertexPositionColorTexture>(newVertices);

		m_BeforeGage = gm->GetMission();
	}

	// ミッションゲージフレーム
// 構築と破棄
	MissionGageFrame::MissionGageFrame(
		const shared_ptr<Stage>& StagePtr,
		const bool Trace,
		const Vec2& Scale,
		const Vec2& Position,
		const wstring& Tx,
		const int& Layer)
		:
		GameObject(StagePtr),
		m_Trace(Trace),
		m_Scale(Scale),
		m_Position(Position),
		m_TX(Tx),
		m_Layer(Layer)
	{
	}
	MissionGageFrame::~MissionGageFrame() {}

	// 初期化
	void MissionGageFrame::OnCreate()
	{
		// 大きさ、位置
		auto ptrTrans = GetComponent<Transform>();
		ptrTrans->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		ptrTrans->SetPosition(m_Position.x, m_Position.y, 0.0f);

		// アニメチップ作成
		float tipWidth = 1.0f;
		float tipHeight = 1.0f;
		for (int col = 0; col < 1; col++)
		{
			for (int row = 0; row < 1; row++)
			{
				// 幅
				m_AnimTip[col][row].left = tipWidth * row;							// 左
				m_AnimTip[col][row].right = m_AnimTip[col][row].left + tipWidth;	// 右
				// 高さ
				m_AnimTip[col][row].top = tipHeight * col;							// 上
				m_AnimTip[col][row].bottom = m_AnimTip[col][row].top + tipHeight;	// 下
			}
		}

		// 描画するテクスチャの設定
		m_Vertices =
		{
			{Vec3(-0.0f,      -HalfSize, 0.0f), White, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},	// 左上
			{Vec3(+m_MaxSize, -HalfSize, 0.0f), White, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right,m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},	// 右上
			{Vec3(-0.0f,      +HalfSize, 0.0f), White, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},	// 左下
			{Vec3(+m_MaxSize, +HalfSize, 0.0f), White, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}	// 右下

		};
		m_Indices = {
			0, 1, 2,
			1, 3, 2
		};

		// 頂点とインデックスを指定してスプライトの生成
		auto ptrDraw = AddComponent<PCTSpriteDraw>(m_Vertices, m_Indices);
		ptrDraw->SetTextureResource(m_TX);
		ptrDraw->SetDepthStencilState(DepthStencilState::None);

		// 透明処理
		SetAlphaActive(m_Trace);
		SetDrawLayer(m_Layer);
	}

	// ミッション演出用スプライト
	// 構築と破棄
	MissionSprite::MissionSprite(
		const shared_ptr<Stage>& StagePtr,
		const Vec3& Scale,
		const Vec3& Rotation,
		const Vec3& Position,
		const wstring& Tx,
		const int& Layer,
		const Col4& Color)
		:
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position),
		m_TX(Tx),
		m_Layer(Layer),
		m_Color(Color)
	{
	}
	MissionSprite::~MissionSprite() {}

	// 初期化
	void MissionSprite::OnCreate()
	{
		// 大きさ、回転、位置
		auto ptrTransform = GetComponent<Transform>();
		ptrTransform->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		ptrTransform->SetRotation(m_Rotation);
		ptrTransform->SetPosition(m_Position.x, m_Position.y, 0.0f);

		// 描画するテクスチャの設定
		float halfSize = 0.5f;
		vector<VertexPositionColorTexture> vertices =
		{
			{Vec3(-halfSize, +halfSize, 0), m_Color, Vec2(0.0f, 0.0f)},
			{Vec3(+halfSize, +halfSize, 0), m_Color, Vec2(1.0f, 0.0f)},
			{Vec3(-halfSize, -halfSize, 0), m_Color, Vec2(0.0f, 1.0f)},
			{Vec3(+halfSize, -halfSize, 0), m_Color, Vec2(1.0f, 1.0f)}
		};
		vector<uint16_t> indices = {
			0, 1, 2,
			1, 3, 2
		};
		// 透明処理
		SetAlphaActive(true);



		// レイヤー
		SetDrawLayer(m_Layer);

		// 頂点とインデックスを指定してスプライト作成
		auto ptrDraw = this->AddComponent<PCTStaticDraw>();
		ptrDraw->CreateOriginalMesh(vertices, indices);
		ptrDraw->SetOriginalMeshUse(true);
		ptrDraw->SetTextureResource(m_TX);
		ptrDraw->SetDepthStencilState(DepthStencilState::None);
	}

	// 更新
	void MissionSprite::OnUpdate()
	{
		auto gm = GameManager::GetInstance();
		if (gm->GetMissionFlg()) {
			if (m_Rotation == Vec3(0.0f, 0.0f, 0.0f)) {
				if (m_Rotation != m_BeforeRot)
				{
					m_turnNum++;
					if (m_turnNum >= 2)
					{
						m_turnNum = 0;
						m_Rotation = Vec3(0.0f, 0.0f, 0.0f);
						m_BeforeRot = Vec3(0.0f, 0.0f, 0.0f);
						gm->SetMissionFlg(false);
						GetComponent<Transform>()->SetRotation(m_Rotation);

						return;
					}
				}
			}
			auto elapsedTime = App::GetApp()->GetElapsedTime();
			m_Rotation.y -= elapsedTime * 3.0f;
			m_BeforeRot = m_Rotation;
			if (ConvertDegree(m_Rotation.y) <= -180.0f)
			{
				auto particlePtr = GetStage()->GetSharedGameObject<MoodParticle>(L"MoodParticle");
				particlePtr->InsertMoodParticle(m_Position);

				m_Rotation = Vec3(0.0f, 0.0f, 0.0f);
			}
			GetComponent<Transform>()->SetRotation(m_Rotation);
		}
	}

	// スコア用(数字用)スプライト
	// 構築と破棄
	ScoreSprite::ScoreSprite(
		const shared_ptr<Stage>& StagePtr,
		const bool Trace,
		const Vec2& Scale,
		const Vec2& Position,
		const wstring& Tx,
		const UINT& NumOfDigits)
		:
		GameObject(StagePtr),
		m_Trace(Trace),
		m_Scale(Scale),
		m_Position(Position),
		m_TX(Tx),
		m_NumOfDigits(NumOfDigits)
	{
	}
	ScoreSprite::~ScoreSprite() {}

	// 初期化
	void ScoreSprite::OnCreate()
	{
		Col4 White(1.0f, 1.0f, 1.0f, 1.0f);
		float xPieceSize = 1.0f / (float)m_NumOfDigits;
		vector<uint16_t> indices;
		for (UINT i = 0; i < m_NumOfDigits; i++)
		{
			float vertex0 = -HalfSize + xPieceSize * (float)i;
			float vertex1 = vertex0 + xPieceSize;
			// 0
			m_BackupVertices.push_back(
				VertexPositionColorTexture(Vec3(vertex0, HalfSize, 0.0f), White, Vec2(0.0f, 0.0f))
			);
			// 1
			m_BackupVertices.push_back(
				VertexPositionColorTexture(Vec3(vertex1, HalfSize, 0.0f), White, Vec2(1.0f, 0.0f))
			);
			// 2
			m_BackupVertices.push_back(
				VertexPositionColorTexture(Vec3(vertex0, -HalfSize, 0.0f), White, Vec2(0.0f, 1.0f))
			);
			// 3
			m_BackupVertices.push_back(
				VertexPositionColorTexture(Vec3(vertex1, -HalfSize, 0.0f), White, Vec2(1.0f, 1.0f))
			);
			indices.push_back(i * 4 + 0);
			indices.push_back(i * 4 + 1);
			indices.push_back(i * 4 + 2);
			indices.push_back(i * 4 + 1);
			indices.push_back(i * 4 + 3);
			indices.push_back(i * 4 + 2);
		}
		// 0〜9までの数字で1.0f

		SetAlphaActive(true);
		auto ptrTrans = GetComponent<Transform>();
		ptrTrans->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		ptrTrans->SetPosition(m_Position.x, m_Position.y, 0.0f);
		// 頂点とインデックスを指定してスプライト生成
		auto ptrDraw = AddComponent<PCTSpriteDraw>(m_BackupVertices, indices);
		ptrDraw->SetTextureResource(m_TX);
		GetStage()->SetSharedGameObject(L"ScoreSprite", GetThis<ScoreSprite>());

	}

	// 更新
	//void ScoreSprite::OnUpdate()
	//{

	//}

	// 切り替え演出
	// フェード
	// 構築と破棄
	Fade::Fade(
		const shared_ptr<Stage>& StagePtr,
		const bool Trace,
		const Vec2& Scale,
		const Vec2& Position,
		const wstring& Tx,
		const float& Alpha)
		:
		GameObject(StagePtr),
		m_Trace(Trace),
		m_Scale(Scale),
		m_Position(Position),
		m_TX(Tx),
		m_Alpha(Alpha)
	{
	}
	Fade::~Fade(){}

	// 初期化
	void Fade::OnCreate()
	{
		// 描画するテクスチャの設定
		float halfSize = 0.5f;
		m_Color.w = m_Alpha;
		vector<VertexPositionColorTexture> vertices = 
		{
			{Vec3(-halfSize, +halfSize, 0.0f), m_Color, Vec2(0.0f, 0.0f)},
			{Vec3(+halfSize, +halfSize, 0.0f), m_Color, Vec2(1.0f, 0.0f)},
			{Vec3(-halfSize, -halfSize, 0.0f), m_Color, Vec2(0.0f, 1.0f)},
			{Vec3(+halfSize, -halfSize, 0.0f), m_Color, Vec2(1.0f, 1.0f)}
		};
		vector<uint16_t> indices =
		{
			0, 1, 2,
			1, 3, 2
		};
		// 透過するか
		SetAlphaActive(m_Trace);
		// 大きさ、位置
		auto ptrTrans = GetComponent<Transform>();
		ptrTrans->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		ptrTrans->SetPosition(m_Position.x, m_Position.y, 0.0f);
	
		// 頂点とインデックスを指定してスプライト作成
		auto ptrDraw = AddComponent<PCTSpriteDraw>(vertices, indices);
		ptrDraw->SetTextureResource(m_TX);
		ptrDraw->SetDiffuse(m_Color);
		ptrDraw->SetDepthStencilState(DepthStencilState::None);

		SetDrawLayer(10);
	}

	// 更新
	void Fade::OnUpdate()
	{
		auto gm = GameManager::GetInstance();
		if (gm->GetFadeFlg())
		{
			FadeIn();
		}
		if (!gm->GetFadeFlg())
		{
			FadeOut();
		}
	}

	// フェードイン
	void Fade::FadeIn()
	{
		auto gm = GameManager::GetInstance();
		if (!gm->GetFadeEndFlg()) {
			float elapsedTime = App::GetApp()->GetElapsedTime();
			// 透明度の更新
			m_Alpha -= m_Speed * elapsedTime;
			// 透明度が0になったら
			if (m_Alpha <= 0.0f)
			{
				m_Alpha = 0.0f;
				gm->SetFadeEndFlg(true);
			}
			m_Color.w = m_Alpha;
			auto ptrDraw = GetComponent<PCTSpriteDraw>();
			ptrDraw->SetDiffuse(m_Color);
		}
	}

	// フェードアウト
	void Fade::FadeOut()
	{
		auto gm = GameManager::GetInstance();
		if (!gm->GetFadeEndFlg()) {
			float elapsedTime = App::GetApp()->GetElapsedTime();
			// 透明度の更新
			m_Alpha += m_Speed * elapsedTime;
			// 透明度が1.0になったら
			if (m_Alpha >= 1.0f)
			{
				m_Alpha = 1.0f;
				gm->SetFadeEndFlg(true);
			}
			m_Color.w = m_Alpha;
			auto ptrDraw = GetComponent<PCTSpriteDraw>();
			ptrDraw->SetDiffuse(m_Color);
		}
	}

}
//end basecross
