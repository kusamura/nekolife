/*!
@file Cat.cpp
@brief 猫関係実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {
	// 仮って入っているところは後から消すかもしれない部分

	// ねこクラス実体
	// 構築と破棄
	Cat::Cat(
		const shared_ptr<Stage>& StagePtr,
		const wstring& BaseDir,
		const wstring& XmlfileName,
		const wstring& StartAnimeName,
		const Vec3& Scale,
		const Vec3& Rotation, 
		const Vec3& Position,
		const shared_ptr<ObjState<Cat>>& FirstState,
		const CatType& Type)
		:
		SS5ssae(StagePtr, BaseDir,XmlfileName,StartAnimeName),
		m_currentAnim(StartAnimeName),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position),
		m_FirstState(FirstState),
		m_Type(Type)
	{
	}
	Cat::~Cat(){}

	// 初期化
	void Cat::OnCreate()
	{
		//各パフォーマンスを得る
		GetStage()->SetCollisionPerformanceActive(true);
		GetStage()->SetUpdatePerformanceActive(true);
		GetStage()->SetDrawPerformanceActive(true);

		// 大きさ、回転、位置
		auto ptrTransform = GetComponent<Transform>();
		ptrTransform->SetScale(m_Scale);
		ptrTransform->SetRotation(m_Rotation);
		ptrTransform->SetPosition(m_Position);

		// モデルとトランスフォームの間の差分行列
		m_spanMat.affineTransformation(
			Vec3(0.1f, 0.1f, 0.1f),
			Vec3(0.0f),
			Vec3(0.0f),
			Vec3(0.0f)
		);
		// モデルとトランスフォームの間の差分行列(喧嘩の煙用)
		m_smokeSpanMat.affineTransformation(
			Vec3(0.2f, 0.2f, 0.2f),
			Vec3(0.0f),
			Vec3(0.0f),
			Vec3(0.0f)
		);
		// 親クラスの初期化呼び出し
		SS5ssae::OnCreate();
		SetToAnimeMatrix(m_spanMat);
		// アニメーションの更新
		float elapsedTime = App::GetApp()->GetElapsedTime();
		UpdateAnimeTime(elapsedTime);

		// 猫のステータス設定
		InitParameters();

		// CollisionSphere衝突判定をつける
		auto ptrColl = this->AddComponent<CollisionSphere>();
		ptrColl->SetMakedDiameter(m_CollDiameter);

		// タグをつける
		AddTag(WstringKey::tag_Cat);

		//値は秒あたりのフレーム数
		SetFps(30.0f);

		// 透明処理
		SetAlphaActive(true);
		// レイヤー
		SetDrawLayer(2);

		// ステートマシンの構築
		m_StateMachine.reset(new StateMachine<Cat>(GetThis<Cat>()));
		// 最初のステートをm_FirstStateに設定
		m_StateMachine->ChangeState(m_FirstState);

		// 子オブジェクトの生成
		m_ChildVisual = GetStage()->AddGameObject<CatChildrenVisual>(m_Scale * m_childVisualMag, m_Rotation, m_Position, GetThis<GameObject>())->GetThis<GameObject>();
		m_ExclamationMark = GetStage()->AddGameObject<CatExclamationMark>(m_Scale * m_exclamationMag, m_Rotation, m_Position, GetThis<GameObject>())->GetThis<GameObject>();
		SetChildExclamationMarkActive(false);
		// コリジョン除外
		ptrColl->AddExcludeCollisionGameObject(m_ChildVisual);

		// TitleStageなら
		if (dynamic_pointer_cast<TitleStage>(GetStage()))
		{
			m_MoodGage = MaxMoodGage;
		}

		// 文字列をつける
		auto ptrString = AddComponent<StringSprite>();
		ptrString->SetText(L"");
		ptrString->SetTextRect(Rect2D<float>(16.0f, 160.0f, 640.0f, 480.0f));
	}

	// 更新
	void Cat::OnUpdate()
	{
		// フェード中なら入らない
		if (!GameManager::GetInstance()->GetFadeEndFlg())return;

		SetChangeStateFlg(false);
		// 出入り中でなければ
		if (!GetEnterExitFlg()) {

			// 当たったオブジェクトとの反応(更新する用)
			CollisionUpdate();

			//if (m_standingflg) {
				// 放置時間測る
				CalcStandingTime();
			//}
		}
		// アニメーションの更新
		float elapsedTime = App::GetApp()->GetElapsedTime();
		UpdateAnimeTime(elapsedTime);


		// ステートマシンを更新
		m_StateMachine->Update();
	}

	void Cat::OnUpdate2()
	{
		//auto colObjs = m_CollisionObjs;// 確認用
		//int mm = GameManager::GetInstance()->GetMaxMission();
		//DrawStrings();
	}

	// 文字列表示
	void Cat::DrawStrings()
	{
		wstring updatePerStr(L"UpdatePerformance:\t");
		updatePerStr += Util::FloatToWStr(GetStage()->GetUpdatePerformanceTime());
		updatePerStr += L"\tmillisecond\n";

		wstring drawPerStr(L"DrawPerformance:\t");
		drawPerStr += Util::FloatToWStr(GetStage()->GetDrawPerformanceTime());
		drawPerStr += L"\tmillisecond\n";

		wstring collPerStr(L"CollisionPerform:\t");
		collPerStr += Util::FloatToWStr(GetStage()->GetCollisionPerformanceTime(), 5);
		collPerStr += L"\tmillisecond\n";

		wstring collMiscStr(L"ColMiscPerform:\t");
		collMiscStr += Util::FloatToWStr(GetStage()->GetCollisionManager()->GetMiscPerformanceTime(), 5);
		collMiscStr += L"\tmillisecond\n";


		auto pos = GetComponent<Transform>()->GetPosition();
		wstring positionStr(L"Position:\t");
		positionStr += L"X=" + Util::FloatToWStr(pos.x, 6, Util::FloatModify::Fixed) + L",\t";
		positionStr += L"Y=" + Util::FloatToWStr(pos.y, 6, Util::FloatModify::Fixed) + L",\t";
		positionStr += L"Z=" + Util::FloatToWStr(pos.z, 6, Util::FloatModify::Fixed) + L"\n";


		auto randomPosi = randomPos;
		wstring randomPosStr(L"randomPos:\t");
		randomPosStr += L"X=" + Util::FloatToWStr(randomPosi.x, 6, Util::FloatModify::Fixed) + L",\t";
		randomPosStr += L"Y=" + Util::FloatToWStr(randomPosi.y, 6, Util::FloatModify::Fixed) + L",\t";
		randomPosStr += L"Z=" + Util::FloatToWStr(randomPosi.z, 6, Util::FloatModify::Fixed) + L"\n";


		wstring randomStaStr(L"RandomState:\t");
		randomStaStr += Util::IntToWStr(ranState);
		randomStaStr += L"\n";

		//auto scale = GetComponent<Transform>()->GetScale();
		//wstring scaleStr(L"Scale:\t");
		//scaleStr += L"X=" + Util::FloatToWStr(scale.x, 6, Util::FloatModify::Fixed) + L",\t";
		//scaleStr += L"Y=" + Util::FloatToWStr(scale.y, 6, Util::FloatModify::Fixed) + L",\t";
		//scaleStr += L"Z=" + Util::FloatToWStr(scale.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring gageStr(L"moodGage:\t");
		gageStr += Util::IntToWStr(m_MoodGage) + L"\n";

		//auto childPos = GetChildVisual()->GetComponent<Transform>()->GetPosition();
		//wstring childPosStr(L"childPos:\t");
		//childPosStr += L"X=" + Util::FloatToWStr(childPos.x, 6, Util::FloatModify::Fixed) + L",\t";
		//childPosStr += L"Y=" + Util::FloatToWStr(childPos.y, 6, Util::FloatModify::Fixed) + L",\t";
		//childPosStr += L"Z=" + Util::FloatToWStr(childPos.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring stateStr = L"State:\t";
		stateStr += myState + L"\n";

		wstring standStr = L"StandingTime:\t";
		standStr += Util::FloatToWStr(standingTotalTime, 6, Util::FloatModify::Fixed) + L"\n";

		wstring finishEatStr = L"finishedEatFlg:\t";
		finishEatStr += Util::IntToWStr((int)m_finishedEatFlg) + L"\n";

		wstring eatFlatStr = L"FlatDis:\t";
		eatFlatStr += L"X=" + Util::FloatToWStr(flatDiseat.x, 6, Util::FloatModify::Fixed) + L",\t";
		eatFlatStr += L"Y=" + Util::FloatToWStr(flatDiseat.y, 6, Util::FloatModify::Fixed) + L",\t";
		eatFlatStr += L"Z=" + Util::FloatToWStr(flatDiseat.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring gmMissionStr = L"Mission:\t";
		auto gm = GameManager::GetInstance();
		gmMissionStr += Util::IntToWStr(gm->GetCurrentMissionNum()) + L"\n" + Util::IntToWStr(gm->GetMission()) + L"\n";
		wstring str = updatePerStr + drawPerStr + collPerStr + collMiscStr + positionStr + randomPosStr + randomStaStr + gageStr + stateStr + standStr + finishEatStr + eatFlatStr + gmMissionStr + upps;
		auto ptrString = GetComponent<StringSprite>();
		ptrString->SetText(str);

	}

	// 当たったオブジェクトとの反応(更新する用)
	void Cat::CollisionUpdate()
	{
		// 画面外に出ない処理
		auto myPos = GetComponent<Transform>()->GetPosition();
		float MaxHeightScreen = 1.9f;
		float MaxWidthScreen = 3.5f;
		if (MaxWidthScreen < std::abs(myPos.x) || MaxHeightScreen < std::abs(myPos.y)) {
			if (MaxWidthScreen < myPos.x) {
				myPos.x = MaxWidthScreen;
				GetComponent<Transform>()->SetPosition(myPos);
			}
			if (-MaxWidthScreen > myPos.x) {
				myPos.x = -MaxWidthScreen;
				GetComponent<Transform>()->SetPosition(myPos);
			}
			if (MaxHeightScreen < myPos.y) {
				myPos.y = MaxHeightScreen;
				GetComponent<Transform>()->SetPosition(myPos);
			}
			if (-MaxHeightScreen > myPos.y) {
				myPos.y = -MaxHeightScreen;
				GetComponent<Transform>()->SetPosition(myPos);
			}
			// 待機ステート
			GetStateMachine()->ChangeState(CatWaitState::Instance());
			return;
		}

		// 毛糸玉の処理
		// 毛糸玉が消えた時の処理
		if (auto yarnballObj = GetCollisionObject<YarnBallInstallation>())
		{
			if (!yarnballObj->GetDrawActive())
			{
				RemoveCollisionObject(yarnballObj);
				m_StateMachine->ChangeState(CatWaitState::Instance());
				return;
			}
		}

		// 放置時間が帰る時間になっていて、かつご飯食べ終わったなら
		if (standingTotalTime >= m_ExitTime && m_finishedEatFlg)
		{
			m_StateMachine->ChangeState(CatExitState::Instance());
			return;
		}

		//	ご飯食べるステートじゃないなら
		if (!dynamic_pointer_cast<CatEatingState>(m_StateMachine->GetCurrentState())) {
		// 当たっているオブジェクトの中にご飯があるなら
			if (auto dining = GetCollisionObject<CatDining>())
			{
				auto diningPos = dining->GetComponent<Transform>()->GetPosition();
					auto myPos = GetComponent<Transform>()->GetPosition();

					if (myPos.y <= diningPos.y)
					{
						SetDrawLayer(5);
					}
					else
					{
						SetDrawLayer(2);
					}
			}
		}

		// 猫を見つけていて、放置時間が一定時間たっていたら
		if (m_findCatFlg && standingTotalTime >= standingTime)
		{
			// ねことじゃれるステートに
			// 猫を映す

			if (auto otherCat = dynamic_pointer_cast<Cat>(GetCollisionObject<Cat>()))
			{
				if (!m_catDrawFlg && !otherCat->m_catDrawFlg) {
					m_catDrawFlg = true;
					otherCat->SetDrawActive(false);
				}
				otherCat->SetCatfightFlg(true);
				otherCat->AddCollisionObject(GetThis<GameObject>());
				otherCat->GetStateMachine()->ChangeState(CatCanPlayState::Instance());
			}
			SetCatfightFlg(true);
			m_StateMachine->ChangeState(CatCanPlayState::Instance());
			upps = L"jarete";
			return;
			//}

		}
		else if (m_findCatFlg && !GetCollisionObject<PlayerPointer>())// 見つけていて、おもちゃが当たってないなら
		{
			standingTotalTime = 0.0f;
			m_findCatFlg = false;
			// 移動する位置を変えて移動ステートに
			//randomFlg = false;
			//if (!GetChangeStateFlg()) {
			//	SetChangeStateFlg(true);
			m_StateMachine->ChangeState(CatMoveState::Instance());
			return;
			//}

		}

		// ネズミのおもちゃなら
		if (auto MouseObj = GetCollisionObjectTag(WstringKey::tag_MouseRC))
		{
			SetRandomPos(MouseObj->GetComponent<Transform>()->GetPosition());
			standingTotalTime = 0;
			m_StateMachine->ChangeState(CatMoveState::Instance());
			return;
		}

		// 後で消す
		if (GetCollisionObject<PlayerPointer>())
		{
			auto catnip = GetStage()->GetSharedGameObject<PlayerPointer>(L"PlayerPointer");
			// 条件終わって1ゲージ判断したらここに戻ってこれるようにしたい
			// もしくは↓のやつをフラグでゲージあげる感じにするか
			//Obj->CalcGage(true);
			//if (catnip->GetMouseMeasu() <= 50.0f) {
				SetCalcGageFlg(calcGageFlg::Add);
			//}
		}

		// 当たっている猫の猫vs猫フラグが立ってたら自分も立てる
		if (auto otherCat = dynamic_pointer_cast<Cat>(GetCollisionObject<Cat>()))
		{
			if (otherCat->GetCatfightFlg())
			{
				if (!m_catDrawFlg && !otherCat->m_catDrawFlg)
				{
					m_catDrawFlg = true;
					otherCat->SetDrawActive(false);
				}
				// ねことじゃれるステートに
				SetCatfightFlg(true);
				m_StateMachine->ChangeState(CatCanPlayState::Instance());
			}
		}
		// 猫と当たっていたのがなくなったら
		if (!GetCollisionObject<Cat>())
		{
			m_findCatFlg = false;
		}
	}

	// 入退室
	void Cat::EnterExitMove(Move move)
	{
		auto ptrTrans = GetComponent<Transform>();
		auto myPos = ptrTrans->GetPosition();
		Vec3 flatDis;
		switch (move)
		{
		case Move::Enter:
			// 左向いてたら右にする
			if (m_Dir == Left)
			{
				m_Dir = Right;
				ptrTrans->SetScale(Vec3(GetDir(), 1.0f, 1.0f));
			}
			// 入る位置まで移動
			// 入る位置と自分の位置との距離
			flatDis = m_EnterPos - myPos;

			// 距離がx,yとも大体0.1以上なら
			if (std::abs(flatDis.x) >= 0.1f || std::abs(flatDis.y) >= 0.1f)
			{
				// その位置に向かうように移動
				myPos += flatDis.normalize() * App::GetApp()->GetElapsedTime() * 0.5f;
			}
			else
			{
				// そうでないなら待機に
				m_StateMachine->ChangeState(CatWaitState::Instance());
				return;
			}
			break;
		case Move::Exit:
			// 右向いてたら左にする
			if (m_Dir == Right)
			{
				m_Dir = Left;
				ptrTrans->SetScale(Vec3(GetDir(), 1.0f, 1.0f));
			}
			// 出る位置まで移動
			// 出る位置と自分の位置との距離
			flatDis = m_ExitPos - myPos;

			// 距離がx,yとも大体0.1以上なら
			if (std::abs(flatDis.x) >= 0.1f || std::abs(flatDis.y) >= 0.1f)
			{
				// その位置に向かうように移動
				myPos += flatDis.normalize() * App::GetApp()->GetElapsedTime();
			}
			else
			{
				// そうでないなら新しい子を生成して、自分(と子オブジェクト)を消す
				// 仲良くなったねこカウント
				auto gm = GameManager::GetInstance();
				gm->AddCatNum(1);
				wstring dataDir;
				App::GetApp()->GetDataDirectory(dataDir);
				wstring animDir = dataDir + L"Animations\\";

				// どの種類の猫出すか(とりあえず3種類)
				int catType = rand() % 3;

				GetStage()->AddGameObject<Cat>(animDir, GetCatTypeAnim((CatType)catType) , L"wait", Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), m_ExitPos, CatEnterState::Instance(), (CatType)catType);
				GetStage()->RemoveGameObject<GameObject>(m_ChildVisual->GetThis<CatChildrenVisual>());
				GetStage()->RemoveGameObject<GameObject>(GetThis<Cat>());
			}
			break;
		default:
			break;
		}
		ptrTrans->SetPosition(myPos);
	}

	// 猫のパラメータ設定
	void Cat::InitParameters()
	{
		Col4 White = Col4(1.0f, 1.0f, 1.0f, 1.0f);// 白

		// 種類がどれならどうっていう感じでスイッチ文で書く予定
		switch (m_Type)
		{
		case CatType::Normal:
			// 気分ゲージ
			m_MoodGage = 3;
			// 空腹ゲージ
			m_HungerGage = 0;
			// 放置時間
			standingTime = 10.0f;
			// 気まぐれ時間
			whimTime = 6.0f;
			// 猫vs猫時間
			m_CatfightTime = 5.0f;
			// ご飯中気にするか
			m_careFlg = true;
			// ご飯中に気にする時間
			careTime = 5.0f;
			// 飯食う時間
			eatTime = 10.0f;
			// 離れる距離
			m_LeaveDis = Vec3(3.0f, 2.0f, 0.0f);
			// 見つめる距離
			m_StareDis = Vec3(3.0f, 1.5f, 0.0f);

			break;
		case CatType::RussianBlue:
			// 気分ゲージ
			m_MoodGage = 2;
			// 空腹ゲージ
			m_HungerGage = 0;
			// 放置時間
			standingTime = 15.0f;
			// 気まぐれ時間
			whimTime = 3.0f;
			// 猫vs猫時間
			m_CatfightTime = 10.0f;
			// ご飯中気にするか
			m_careFlg = false;
			// ご飯中に気にする時間
			careTime = 5.0f;
			// 飯食う時間
			eatTime = 5.0f;
			// 離れる距離
			m_LeaveDis = Vec3(3.0f, 2.0f, 0.0f);
			// 見つめる距離
			m_StareDis = Vec3(3.0f, 1.5f, 0.0f);

			break;
		case CatType::Brown:
			// 気分ゲージ
			m_MoodGage = 4;
			// 空腹ゲージ
			m_HungerGage = 0;
			// 放置時間
			standingTime = 10.0f;
			// 気まぐれ時間
			whimTime = 10.0f;
			// 猫vs猫時間
			m_CatfightTime = 2.0f;
			// ご飯中気にするか
			m_careFlg = true;
			// ご飯中に気にする時間
			careTime = 10.0f;
			// 飯食う時間
			eatTime = 10.0f;
			// 離れる距離
			m_LeaveDis = Vec3(3.0f, 2.0f, 0.0f);
			// 見つめる距離
			m_StareDis = Vec3(3.0f, 1.5f, 0.0f);

			break;
		default:
			break;
		}
	}

	// 猫vs猫時間計算
	bool Cat::CalcCatfightTime()
	{
		float elpasedTime = App::GetApp()->GetElapsedTime();
		m_CatfightTotalTime += elpasedTime;
		if (m_CatfightTotalTime >= m_CatfightTime)
		{
			m_CatfightTotalTime = 0.0f;
			return true;
		}
		return false;
	}

	// クールタイム計算
	void Cat::CalcCoolTime()
	{
		float elpasedTime = App::GetApp()->GetElapsedTime();
		totalTime += elpasedTime;
		// クールタイムが終わったら次の位置決め
		if (totalTime >= coolTime)
		{
			totalTime = 0.0f;
			randomFlg = false;
			moveEndFlg = false;
			// Waitになる(仮)
			//if (!GetChangeStateFlg()) {
			//	SetChangeStateFlg(true);
				m_StateMachine->ChangeState(CatWaitState::Instance());
				return;
			//}
		}
	}

	// ランダム計算
	void Cat::CalcRandom(Random calc)
	{
		switch (calc)
		{
		case Random::Pos:
			// ランダムな位置をとる（小数）
			randomPos = Vec3((float)(rand() * 5 /(1.0f * RAND_MAX) - 2.5f), (float)(rand() * 3 / (1.0f * RAND_MAX) - 1.5f), 0.5f);
			randomFlg = true;
			break;
		case Random::State:
			// ランダムステート計算	(2/10でmoveState,仮で半分)
			totalWaitTime += App::GetApp()->GetElapsedTime();
			if (totalWaitTime >= waitTime) {
				totalWaitTime = 0.0f;
				randomState = rand() % 12;
				// デバッグ用(後で消す)
				ranState = randomState;
				// 出目が4以下でMoveステート(出目後で変数に)
				if (GetRandomState() <= 4)
				{
					//if (!GetChangeStateFlg()) {
					//	SetChangeStateFlg(true);
						GetStateMachine()->ChangeState(CatMoveState::Instance());
						return;
					//}

				}
				else
				{
					if (GetRandomState() <= 6)
					{
						if (GetCurrentAnimation() != L"wait")
						{
							SetCurrentAnimation(L"wait");
							ChangeAnimation(GetCurrentAnimation());
						}

					}
					else if (GetRandomState() <= 8)
					{
						if (GetCurrentAnimation() != L"sleep")
						{
							SetCurrentAnimation(L"sleep");
							ChangeAnimation(GetCurrentAnimation());
						}

					}
					else if(GetRandomState() <= 10)
					{
						if (GetCurrentAnimation() != L"stretch")
						{
							SetCurrentAnimation(L"stretch");
							ChangeAnimation(GetCurrentAnimation());
						}

					}
					else
					{
						if (GetCurrentAnimation() != L"nails")
						{
							SetCurrentAnimation(L"nails");
							ChangeAnimation(GetCurrentAnimation());
						}
					}
				}

			}
			break;
		default:
			break;
		}
	}

	// 放置時間測る用
	void Cat::CalcStandingTime()
	{
		// ステートがWaitかMoveであれば
		auto st = m_StateMachine->GetCurrentState();
		if (st == CatWaitState::Instance() || st == CatMoveState::Instance() || st == CatFindState::Instance())
		{
			standingTotalTime += App::GetApp()->GetElapsedTime();
		}
		else
		{
			// それ以外は
			standingTotalTime = 0.0f;
		}

	}

	// 気まぐれ時間測る(仮)とりあえず一定時間にしてみました
	bool Cat::CalcTime(Calc calc)
	{
		switch(calc)
		{
			case Calc::Whimsical:
				// 気まぐれ時間なら
				whimTotalTime += App::GetApp()->GetElapsedTime();
				if (whimTotalTime >= whimTime)
				{
					whimTotalTime = 0.0f;
					return true;
				}

				break;
			case Calc::Care:
				// 気にする時間なら
				careTotalTime += App::GetApp()->GetElapsedTime();
				if (careTotalTime >= careTime)
				{
					careTotalTime = 0.0f;
					return true;
				}
				break;
			default:
				// その他何もしない
				break;
		}
		return false;
	}

	// おもちゃと猫の距離
	Vec3 Cat::ToyCatDistance()
	{
		Vec3 myPos = GetComponent<Transform>()->GetPosition();
		// おもちゃ(仮)
		// おもちゃはシェアオブジェクトにまとめてつっこんでステージのほうで今何出してるか分かるようにしよう
		auto toyObj = GetStage()->GetSharedGameObject<PlayerPointer>(L"PlayerPointer");
		auto toyPos = toyObj->GetComponent<Transform>()->GetPosition();

		return toyPos - myPos;
	}

	// 後で書き直す予定
	// ご飯食べるステートの中身
	void Cat::StateEating()
	{
		// 先にご飯に近づく
		// ご飯と猫との距離
		if (!m_nearMealFlg) {
			m_EnterExitFlg = true;
			if (GetCurrentAnimation() != L"walk")
			{
				SetCurrentAnimation(L"walk");
				ChangeAnimation(GetCurrentAnimation());
			}

			auto myPos = GetComponent<Transform>()->GetPosition();// 猫位置
			Vec3 mealPos =GetStage()->GetSharedGameObject<CatDining>(L"CatDining")->GetComponent<Transform>()->GetPosition() + nearMealPosDis; //ご飯位置(ちょっと上)
			auto flatDis = mealPos - myPos;
			// 位置が自分より左にあって向きが右なら(-なら)
			if (flatDis.x < 0.0f && GetDir() > 0.0f)
			{
				SetDir(Left);
			}
			// 位置が自分より右にあって向きが左なら(+なら)
			else if (flatDis.x > 0.0f && (int)GetDir() < 0.0f)
			{
				SetDir(Right);
			}
			GetComponent<Transform>()->SetScale(Vec3(GetDir(), 1.0f, 1.0f));
			// 距離がx,yとも大体0.1以上なら
			if (std::abs(flatDis.x) >= 0.05f || std::abs(flatDis.y) >= 0.05f)
			{
				// その位置に向かうように移動
				myPos += flatDis.normalize() * App::GetApp()->GetElapsedTime();
			}
			else
			{
				m_nearMealFlg = true;
				m_EnterExitFlg = false;
			}
			// 位置更新
			GetChildVisual()->GetComponent<Transform>()->SetPosition(Vec3(0.0f, 0.0f, 0.0f));// Vec3 All0.0fは親の位置
			GetComponent<Transform>()->SetPosition(myPos);
		}
		else
		{
			// おもちゃが衝突してて(タグで)、こいつが気にする猫なら
			if (GetCollisionObject<PlayerPointer>() && GetEatingCareFlg())
			{
				// 少し見て食べるを繰り返したい
				lookTotalTime += App::GetApp()->GetElapsedTime();
				if (lookTotalTime >= lookTime)
				{
					// 見てないなら
					if (!lookFlg)
					{
						lookTotalTime = 0.0f;
						lookFlg = true;
						// おもちゃを気にする(見るモーション？)
						if (GetCurrentAnimation() != L"discovery")
						{
							SetDrawLayer(2);
							SetCurrentAnimation(L"discovery");
							ChangeAnimation(GetCurrentAnimation());
							if (GetSEKey() == L"karikari")
							{
								StopSE();
							}

						}
					}
					else
					{
						lookTotalTime = 0.0f;
						lookFlg = false;
						// ご飯食べるモーション
						if (GetCurrentAnimation() != L"karikari")
						{
							SetDrawLayer(5);
							SetCurrentAnimation(L"karikari");
							ChangeAnimation(GetCurrentAnimation());
							if (GetSEKey() != L"karikari")
							{
								StopSE();
								StartSE(L"karikari");
							}
						}
					}
				}
				// 一定時間経ってもおもちゃが衝突しているなら(タグでおもちゃとってくるように後で直す)
				if (CalcTime(Calc::Care) && GetCollisionObject<PlayerPointer>())
				{
					// Approach(ついてくる)ステートに
					SetDir(Right);
					GetComponent<Transform>()->SetScale(Vec3(GetDir(), 1.0f, 1.0f));
					GetStateMachine()->ChangeState(CatApproachState::Instance());
					return;
				}
			}
			else
			{
				// ご飯食べるアニメーション
				if (GetCurrentAnimation() != L"karikari")
				{
					SetDrawLayer(5);
					SetCurrentAnimation(L"karikari");
					ChangeAnimation(GetCurrentAnimation());
					StopSE();
					StartSE(L"karikari");
				}
			}

			// 気にしないなら食うまでこのまま
			// 食べ終わったらボーナスタイム
			eatTotalTime += App::GetApp()->GetElapsedTime();
			if (eatTotalTime >= eatTime)
			{
				auto gm = GameManager::GetInstance();
				gm->SubDiningNum(1);
				gm->AddMissionCatNum(1);
				SetHungerGage(MaxHungerGage);
				SetFinishedEatFlg(true);
				SetBonusFlg(true);
				// ボーナスタイム(前フレームにボーナスタイムフラグ立ってて、今フレームが消えてたら)が終わったら
				m_StateMachine->ChangeState(CatWaitState::Instance());
				return;
			}
		}


	}

	// SE流す(setTimer:true(10秒ごとにループ), false(1回のみ))
	void Cat::StartSE(wstring seKey, bool setTimer)
	{
		if(dynamic_pointer_cast<TitleStage>(GetStage())) return;
		m_SEKey = seKey;
		auto ptrXA = App::GetApp()->GetXAudio2Manager();
		if (setTimer)
		{
			m_seTotalTime += App::GetApp()->GetElapsedTime();
			if (m_seTotalTime >= 10.0f)
			{
				m_seTotalTime = 0.0f;
				m_SE = ptrXA->Start(seKey, 0, 0.5f);
			}
		}
		else
		{
			m_SE = ptrXA->Start(seKey, 0, 0.5f);
		}
	}

	// 今鳴ってるSE止める
	void Cat::StopSE()
	{
		auto XAPtr = App::GetApp()->GetXAudio2Manager();
		XAPtr->Stop(m_SE);
	}

	// ゲッターセッター
	// SE
	void Cat::SetSEKey(wstring setSE)
	{
		m_SEKey = setSE;
	}
	wstring Cat::GetSEKey()const
	{
		return m_SEKey;
	}
	shared_ptr<SoundItem> Cat::GetSE()const
	{
		return m_SE;
	}
	// 前のアニメーション
	void Cat::SetCurrentAnimation(wstring setAnim)
	{
		m_currentAnim = setAnim;
	}
	wstring Cat::GetCurrentAnimation()const
	{
		return m_currentAnim;
	}
	// モデルとトランスフォームの間の差分行列
	Mat4x4 Cat::GetSpanMatrix()const
	{
		return m_spanMat;
	}
	// モデルとトランスフォームの間の差分行列（煙）
	Mat4x4 Cat::GetSmokeSpanMatrix()const
	{
		return m_smokeSpanMat;
	}

	// 猫のタイプ
	Cat::CatType Cat::GetCatType()const
	{
		return m_Type;
	}
	void Cat::SetCatType(CatType Type)
	{
		m_Type = Type;
	}
	// 猫のタイプ別のアニメーション取得
	wstring Cat::GetCatTypeAnim(CatType Type)
	{
		switch (Type)
		{
		case CatType::Normal:
			return L"RussianBule3.ssae";
			break;
		case CatType::RussianBlue:
			return L"RussianBule (1).ssae";
			break;
		case CatType::Brown:
			return L"RussianBule2.ssae";
			break;
		default:
			return L"RussianBule.ssae";
			break;
		}
	}

	// 向き
	void Cat::SetDir(float setDir)
	{
		m_Dir = setDir;
	}
	float Cat::GetDir()const
	{
		return m_Dir;
	}
	// 移動速度
	void Cat::SetSpeed(float setSpeed)
	{
		m_Speed = setSpeed;
	}
	float Cat::GetSpeed()const
	{
		return m_Speed;
	}
	// 気分ゲージ
	void Cat::SetMoodGage(int setGage)
	{
		m_MoodGage = setGage;
	}
	// 気分ゲージ計算
	void Cat::AddMoodGage(int addGage)
	{
		if (m_MoodGage >= MaxMoodGage)return;
		m_MoodGage += addGage;
		auto particlePtr = GetStage()->GetSharedGameObject<MoodParticle>(L"MoodParticle");
		if (particlePtr)
		{
			if (addGage > 0) {
				particlePtr->InsertMoodParticle(GetComponent<Transform>()->GetPosition() + m_ParticlePos, Col4(1.0f, 1.0f, 1.0f, 0.7f));// 元の色半透明度70
			}
			else
			{
				particlePtr->InsertMoodParticle(GetComponent<Transform>()->GetPosition() + m_ParticlePos, Col4(0.5f, 0.5f, 1.0f, 0.7f));// 青半透明度70
			}
		}
	}
	int Cat::GetMoodGage()const
	{
		return m_MoodGage;
	}
	// 気分マックス状態
	int Cat::GetMaxMoodGage()const
	{
		return MaxMoodGage;
	}
	// 空腹ゲージ
	void Cat::SetHungerGage(int setGage)
	{
		m_HungerGage = setGage;
	}
	// 気分ゲージ計算
	void Cat::AddHungerGage(int addGage)
	{
		if (m_HungerGage >= MaxHungerGage)return;
		m_HungerGage += addGage;
	}
	int Cat::GetHungerGage()const
	{
		return m_HungerGage;
	}
	int Cat::GetMaxHungerGage()const	// 空腹ゲージマックス状態
	{
		return MaxHungerGage;
	}

	// 放置時間
	float Cat::GetStandingTime()const
	{
		return standingTime;
	}
	// 猫vs猫時間初期化
	void Cat::InitCatfightTime()
	{
		standingTotalTime = 0.0f;
		m_CatfightTotalTime = 0.0f;
	}

	// フラグ
	// ステートが一回切り替わったらそのフレームはもう変えないフラグ
	void Cat::SetChangeStateFlg(bool setFlg)
	{
		m_ChangeStateFlg = setFlg;
	}
	bool Cat::GetChangeStateFlg()const
	{
		return m_ChangeStateFlg;
	}
	// 出入り中フラグ
	void Cat::SetEnterExitFlg(bool setFlg)
	{
		m_EnterExitFlg = setFlg;
	}
	bool Cat::GetEnterExitFlg()const
	{
		return m_EnterExitFlg;
	}
	// 放置中フラグ
	void Cat::SetStandingFlg(bool setFlg)
	{
		m_standingFlg = setFlg;
	}
	bool Cat::GetStandingFlg()const
	{
		return m_standingFlg;
	}
	// 猫見たフラグ
	void Cat::SetFindCatFlg(bool setFlg)
	{
		m_findCatFlg = setFlg;
	}
	bool Cat::GetFindCatFlg()const
	{
		return m_findCatFlg;
	}
	// 猫vs猫フラグ
	void Cat::SetCatfightFlg(bool setFlg)
	{
		m_catfightFlg = setFlg;
	}
	bool Cat::GetCatfightFlg()const
	{
		return m_catfightFlg;
	}
	// 片方の猫描画フラグ
	void Cat::SetCatDrawFlg(bool setFlg)
	{
		m_catDrawFlg = setFlg;
	}
	bool Cat::GetCatDrawFlg()const
	{
		return m_catDrawFlg;
	}

	// ゲージ計算するかしないかフラグ
	void Cat::SetCalcGageFlg(calcGageFlg setFlg)
	{
		m_CalcGageFlg = setFlg;
	}
	Cat::calcGageFlg Cat::GetCalcGageFlg()
	{
		return m_CalcGageFlg;
	}

	// 気まぐれフラグ
	void Cat::SetWhimsicalFlg(bool setFlg)
	{
		m_WhimFlg = setFlg;
	}
	bool Cat::GetWhimsicalFlg()
	{
		return m_WhimFlg;
	}
	void Cat::InitWhimsicalFlg()	// 初期化
	{
		m_WhimFlg = false;
		whimTotalTime = 0.0f;
	}
	// ランダムフラグ
	void Cat::SetRandomFlg(bool setFlg)
	{
		randomFlg = setFlg;
	}
	bool Cat::GetRandomFlg()const
	{
		return randomFlg;
	}
	// 移動終了フラグ
	void Cat::SetMoveEndFlg(bool setFlg)
	{
		moveEndFlg = setFlg;
	}
	bool Cat::GetMoveEndFlg()const
	{
		return moveEndFlg;
	}
	// ご飯食べたかフラグ
	void Cat::SetFinishedEatFlg(bool setFlg)
	{
		m_finishedEatFlg = setFlg;
	}
	bool Cat::GetFinishedEatFlg()const
	{
		return m_finishedEatFlg;
	}
	// ご飯中に周り気にするかフラグ
	void Cat::SetEatingCareFlg(bool setFlg)
	{
		m_eatingCareFlg = setFlg;
	}
	bool Cat::GetEatingCareFlg()const
	{
		return m_eatingCareFlg;
	}
	// ご飯の近くまで来たかフラグ
	void Cat::SetNearMealFlg(bool setFlg)
	{
		m_nearMealFlg = setFlg;
	}
	bool Cat::GetNearMealFlg()const
	{
		return m_nearMealFlg;
	}
	// ボーナスフラグ
	void Cat::SetBonusFlg(bool setFlg)
	{
		m_BonusFlg = setFlg;
	}
	bool Cat::GetBonusFlg()const
	{
		return m_BonusFlg;
	}
	// ランダム位置
	Vec3 Cat::GetRandomPos()const
	{
		return randomPos;
	}
	// ランダム位置を強制的に変える
	void Cat::SetRandomPos(Vec3 pos)
	{
		randomPos = pos;
		randomFlg = true;
	}
	void Cat::SetRandomPos(float x, float y, float z)
	{
		randomPos = Vec3(x, y, z);
		randomFlg = true;
	}
	// ランダムのモーション
	int Cat::GetRandomState()const
	{
		return randomState;
	}
	// ステートマシン
	const unique_ptr<StateMachine<Cat>>& Cat::GetStateMachine()
	{
		return m_StateMachine;
	}
	// 子オブジェクトの取得
	// 視界
	shared_ptr<GameObject>& Cat::GetChildVisual()
	{
		return m_ChildVisual;
	}
	// ビックリマーク
	shared_ptr<GameObject>& Cat::GetChildExclamationMark()
	{
		return m_ExclamationMark;
	}
	// ビックリマーク描画設定
	void Cat::SetChildExclamationMarkActive(bool setFlg)
	{
		auto childExMark = dynamic_pointer_cast<CatExclamationMark>(m_ExclamationMark);
		childExMark->GetChild()->SetUpdateActive(setFlg);
		childExMark->GetChild()->SetDrawActive(setFlg);
		childExMark->SetUpdateActive(setFlg);
		childExMark->SetDrawActive(setFlg);
	}

	// 離れる距離
	Vec3 Cat::GetLeaveDistance() const
	{
		return m_LeaveDis;
	}
	// 近づく距離
	Vec3 Cat::GetStareDistance() const
	{
		return m_StareDis;
	}

	// 複数
	// 当たっているオブジェクトの追加
	void Cat::AddCollisionObject(shared_ptr<GameObject>& addObj)
	{
		for (auto& colObj : m_CollisionObjs)
		{
			// 何も入ってなかったら
			if (!colObj)
			{
				colObj = addObj;
				return;
			}
			
		}
		return;
	}

	// 当たっているオブジェクトの削除
	void Cat::RemoveCollisionObject(shared_ptr<GameObject>& removeObj)
	{
		for (auto& colObj : m_CollisionObjs)
		{
			// 何も入ってないなら次を見る
			if (!colObj)continue;
			if (colObj == removeObj)
			{
				colObj = nullptr;
				return;
			}
		}
		return;
	}

	// 当たっているオブジェクトのリセット
	void Cat::ResetCollisionObject()
	{
		for (auto& colObj : m_CollisionObjs)
		{
			// 何も入ってないなら次を見る
			if (!colObj)
			{
				continue;
			}
			else
			{
				colObj = nullptr;
			}
		}
	}

	// オブジェクトが入っているか確認
	bool Cat::CheckCollisionObject(shared_ptr<GameObject>& checkObj)
	{
		for (auto& colObj : m_CollisionObjs)
		{
			// 何も入ってないならループを出る
			if (!colObj)continue;
			if (colObj == checkObj)
			{
				return true;
			}
		}
		return false;
	}

	shared_ptr<GameObject>& Cat::GetCollisionObjects()
	{
		return *m_CollisionObjs;
	}

	// タグでオブジェクトを探す
	shared_ptr<GameObject> Cat::GetCollisionObjectTag(wstring tag) 
	{
		for (auto colObj : m_CollisionObjs)
		{
			// 何も入ってないならループを出る
			if (!colObj)continue;
			if (colObj->FindTag(tag))
			{
				return colObj;
			}
		}
		return nullptr;
	}
	// クラス名で探す
	template <typename T>
	shared_ptr<GameObject> Cat::GetCollisionObject()
	{
		for (auto colObj : m_CollisionObjs)
		{
			// 何も入ってないならループを出る
			if (!colObj)break;
			if (dynamic_pointer_cast<T>(colObj))
			{
				return colObj;
			}
		}
		return nullptr;
	}

	// 猫ステートクラス
	// Wait(待機)
	shared_ptr<CatWaitState> CatWaitState::Instance()
	{
		static shared_ptr<CatWaitState> instance(new CatWaitState);
		return instance;
	}
	// ステートに入った瞬間
	void CatWaitState::Enter(const shared_ptr<Cat>& Obj)
	{
		Obj->myState = L"Wait";
		Obj->SetCurrentAnimation(L"wait");
		Obj->ChangeAnimation(Obj->GetCurrentAnimation());
		Obj->SetStandingFlg(true);

	}
	// このステートの間
	void CatWaitState::Execute(const shared_ptr<Cat>& Obj)
	{
		// 待機アニメーション

		// ステートをランダムで決める
		Obj->CalcRandom(Obj->Random::State);
		
	}
	// ステートを抜ける瞬間
	void CatWaitState::Exit(const shared_ptr<Cat>& Obj)
	{
	}

	// Move(移動)
	shared_ptr<CatMoveState> CatMoveState::Instance()
	{
		static shared_ptr<CatMoveState> instance(new CatMoveState);
		return instance;
	}
	// ステートに入った瞬間
	void CatMoveState::Enter(const shared_ptr<Cat>& Obj)
	{
		Obj->myState = L"Move";
		if (Obj->GetCollisionObjectTag(WstringKey::tag_MouseRC))
		{
			Obj->SetCurrentAnimation(L"run");
		}
		else
		{
			Obj->SetCurrentAnimation(L"walk");
		}
		Obj->ChangeAnimation(Obj->GetCurrentAnimation());
		Obj->SetStandingFlg(true);
		Obj->StopSE();
		Obj->StartSE(L"nya");


	}
	// このステートの間
	void CatMoveState::Execute(const shared_ptr<Cat>& Obj)
	{
		Vec3 myPos = Obj->GetComponent<Transform>()->GetPosition();

		// 走っているなら（ネズミ追いかけてきたなら）
		// ネズミラジコンが画面外に出るまで
		if (Obj->GetCurrentAnimation() == L"run") {
			auto MouseRCObj = Obj->GetStage()->GetSharedGameObject<MouseRCInstallation>(L"MouseRC");
			if (MouseRCObj->GetDrawActive())
			{
				// 少し早めに
				Obj->SetSpeed(1.6f);
				Obj->SetEnterExitFlg(true);
				Obj->SetRandomPos(MouseRCObj->GetComponent<Transform>()->GetPosition());
			}
			else
			{
				if (Obj->CheckCollisionObject(MouseRCObj->GetThis<GameObject>()))
				{
					Obj->RemoveCollisionObject(MouseRCObj->GetThis<GameObject>());
				}
				// 元の速さに
				Obj->SetSpeed(1.0f);
				Obj->SetRandomFlg(false);
				Obj->GetStateMachine()->ChangeState(CatWaitState::Instance());
				return;
			}
		}

		// 移動が終わったら
		if (Obj->GetMoveEndFlg())
		{
			Obj->CalcCoolTime();
		}
		// 次の位置が画面外なら次の位置決める
		//float MaxHeightScreen = 1.7f;
		//float MaxWidthScreen = 3.0f;
		//if (MaxWidthScreen < std::abs(Obj->GetRandomPos().x) || MaxHeightScreen < std::abs(Obj->GetRandomPos().y)) {
		//	Obj->SetRandomFlg(false);
		//}

		// ランダムで次の位置を決める
		if (!Obj->GetRandomFlg())
		{
			Obj->CalcRandom(Obj->Random::Pos);
		}

		// 次の位置と自分の位置との距離
		Vec3 flatDis = Obj->GetRandomPos() - myPos;

		// 次の位置が自分より左にあって向きが右なら(-なら)
		if (flatDis.x < 0.0f && Obj->GetDir() > 0.0f)
		{
			Obj->SetDir(Obj->Left);
		}
		// 次の位置が自分より右にあって向きが左なら(+なら)
		else if (flatDis.x > 0.0f && (int)Obj->GetDir() < 0.0f)
		{
			Obj->SetDir(Obj->Right);
		}
		Obj->GetComponent<Transform>()->SetScale(Vec3(Obj->GetDir(), 1.0f, 1.0f));


		// 距離がx,yとも大体0.1以上なら
		if (std::abs(flatDis.x) >= 0.1f || std::abs(flatDis.y) >= 0.1f)
		{
			// その位置に向かうように移動
			myPos += flatDis.normalize() * App::GetApp()->GetElapsedTime() * Obj->GetSpeed();
		}
		else
		{
			// そうでないなら移動を終わる
			Obj->SetMoveEndFlg(true);
		}

		Obj->GetComponent<Transform>()->SetPosition(myPos);

	}
	// ステートを抜ける瞬間
	void CatMoveState::Exit(const shared_ptr<Cat>& Obj)
	{
		if (Obj->GetEnterExitFlg())
		{
			Obj->SetEnterExitFlg(false);
		}
	}

	// Find(見つける)
	shared_ptr<CatFindState> CatFindState::Instance()
	{
		static shared_ptr<CatFindState> instance(new CatFindState);
		return instance;
	}
	// ステートに入った瞬間
	void CatFindState::Enter(const shared_ptr<Cat>& Obj)
	{
		Obj->myState = L"Find";
		// アニメーションの切り替え
		Obj->SetCurrentAnimation(L"discovery");
		Obj->ChangeAnimation(Obj->GetCurrentAnimation());
		Obj->SetStandingFlg(false);
		// ビックリマークの表示
		Obj->SetChildExclamationMarkActive(true);
	}
	// このステートの間
	void CatFindState::Execute(const shared_ptr<Cat>& Obj)
	{
		// 成功・失敗の判断は多分ここorおもちゃ毎or衝突判定

		// 当たっているオブジェクトの中にご飯があるなら
		if (Obj->GetCollisionObject<CatDining>())
		{
			// ご飯食べてなくて、気分ゲージがマックスなら
			if (!Obj->GetFinishedEatFlg() && Obj->GetMoodGage() >= Obj->GetMaxMoodGage())
			{
				// ひとつ前のステートがApproachなら
				if (Obj->GetStateMachine()->GetPreviousState() == CatApproachState::Instance()) {
					// おもちゃと猫の距離
					Vec3 flatDis = Obj->ToyCatDistance();
					// 指定した距離以内なら
					if (std::abs(flatDis.x) <= Obj->GetStareDistance().x && std::abs(flatDis.y) <= Obj->GetStareDistance().y)
					{
						// ご飯食べるステートに
						Obj->GetStateMachine()->ChangeState(CatEatingState::Instance());
						return;
					}
				}
			}

		}

		// 猫
		if (Obj->GetCollisionObject<Cat>())
		{
			// 入退室中でなければ
			if (!dynamic_pointer_cast<Cat>(Obj->GetCollisionObject<Cat>())->GetEnterExitFlg()) {
				Obj->SetFindCatFlg(true);
			}
		}

		// 当たっているオブジェクトがおもちゃなら(仮)
		if (Obj->GetCollisionObject<PlayerPointer>())
		{
			Obj->InitCatfightTime();
			if (Obj->GetMoodGage() >= Obj->GetMaxMoodGage())// 仮判断()
			{
				Obj->GetStateMachine()->ChangeState(CatApproachState::Instance());
				return;
			}

			if (Obj->GetMoodGage() <= 0)
			{
				Obj->GetStateMachine()->ChangeState(CatEscapeState::Instance());
				return;
			}


		}

		// 当たっているオブジェクトが毛糸玉なら
		if (auto yarnball = Obj->GetCollisionObject<YarnBallInstallation>())
		{
			Obj->SetRandomPos(yarnball->GetComponent<Transform>()->GetPosition());
			Obj->GetStateMachine()->ChangeState(CatMoveState::Instance());
			return;
		}

		// 家具に当たっていたら確率でそこで休む
		if (auto furnitureObj = Obj->GetCollisionObjectTag(L"tag_Furniture"))
		{
			Obj->RemoveCollisionObject(furnitureObj);
			int randNum = rand() % 5;
			// 5分の2の確率で家具に移動
			if (randNum <= 2)
			{
				auto objPos = furnitureObj->GetComponent<Transform>()->GetPosition();
				// 移動場所を家具に変更し、移動ステートへ
				Obj->SetRandomPos(objPos);
				Obj->GetStateMachine()->ChangeState(CatMoveState::Instance());
				return;
			}
			else
			{
				// そうでないなら元々行く予定だった位置へ移動するため移動ステートへ
				Obj->GetStateMachine()->ChangeState(CatMoveState::Instance());
				return;
			}
		}

		// 当たっているオブジェクトたちに何も入ってなかったら(もしくはごはんの前の部分で反応なくてここまでたどりついたら)
		if (!Obj->GetCollisionObjects() || Obj->GetCollisionObject<CatDining>() )
		{
			// Waitに
			Obj->GetStateMachine()->ChangeState(CatWaitState::Instance());
			return;
		}
	}
	// ステートを抜ける瞬間
	void CatFindState::Exit(const shared_ptr<Cat>& Obj)
	{
		// ビックリマークの非表示
		Obj->SetChildExclamationMarkActive(false);
	}

	// Approach(ついてくる)
	shared_ptr<CatApproachState> CatApproachState::Instance()
	{
		static shared_ptr<CatApproachState> instance(new CatApproachState);
		return instance;
	}
	// ステートに入った瞬間
	void CatApproachState::Enter(const shared_ptr<Cat>& Obj)
	{
		Obj->myState = L"Approach";
		Obj->ChangeAnimation(L"walk");
		Obj->InitWhimsicalFlg();
		Obj->SetStandingFlg(false);
		//Obj->StopSE();
		//Obj->StartSE(L"nya");

	}
	// このステートの間
	void CatApproachState::Execute(const shared_ptr<Cat>& Obj)
	{
		// 1ゲージごとに
		// 喜ぶアニメーション再生

		// 自分の位置
		Vec3 myPos = Obj->GetComponent<Transform>()->GetPosition();

		// おもちゃの位置と自分の位置との距離
		Vec3 flatDis = Obj->ToyCatDistance();
		//Vec3 flatDis = collObjPos - myPos;
		Obj->flatDiseat = flatDis;// 後で消す
		// 指定した距離以内なら
		if (std::abs(flatDis.x) <= Obj->GetStareDistance().x && std::abs(flatDis.y) <= Obj->GetStareDistance().y)
		{

				// 最大ならおもちゃを追いかける(なお気まぐれでどっか行く)気まぐれをどう判断するか。一定時間？
				if (Obj->GetMoodGage() >= Obj->GetMaxMoodGage())
				{
					// おもちゃの位置が自分の向いている方向なら
					if ((flatDis.x > 0 && Obj->GetDir() > 0) || (flatDis.x < 0 && Obj->GetDir() < 0)) {
						// 一定距離離れていたら
						if (std::abs(flatDis.x) > 0.7f || std::abs(flatDis.y) > 0.5f) {

							myPos += flatDis.normalize() * App::GetApp()->GetElapsedTime();
							myPos.z = 0.5f;
							Obj->GetComponent<Transform>()->SetPosition(myPos);

						}
						else
						{
							// 離れていないならじゃれる（CanPlay）
							Obj->GetStateMachine()->ChangeState(CatCanPlayState::Instance());
							return;
						}
					}
					else
					{
						// 向いてなかったら待機
						Obj->GetStateMachine()->ChangeState(CatWaitState::Instance());
						return;
					}
					Obj->SetWhimsicalFlg(Obj->CalcTime(Obj->Calc::Whimsical));
					// 気分ゲージがマックスでなく、気まぐれフラグが立っている状態
					if (Obj->GetWhimsicalFlg()/* && !(Obj->GetMoodGage() >= Obj->GetMaxMoodGage())*/)
					{
						Obj->GetStateMachine()->ChangeState(CatEscapeState::Instance());
						return;
					}
				}
				else
				{
					// 8未満なら
					// まだものが当たっていたらFindそうでないならWait
					if (Obj->GetCollisionObjects()) {
						Obj->GetStateMachine()->ChangeState(CatFindState::Instance());
						return;
					}
					else
					{
						Obj->GetStateMachine()->ChangeState(CatWaitState::Instance());
						return;
					}
				}
			
		}
		else
		{
			// 指定した距離の範囲外なら待機ステート
			Obj->GetStateMachine()->ChangeState(CatWaitState::Instance());
			return;
		}

		//// 気分値が最大値なら
		//if (Obj->GetMoodGage() >= Obj->GetMaxMoodGage())
		//{
		//	// ごろごろつける。消さない
		//	Obj->StartSE(WstringKey::se_Gorogoro, true);
		//}

	}
	// ステートを抜ける瞬間
	void CatApproachState::Exit(const shared_ptr<Cat>& Obj)
	{
		//Obj->InitCatfightTime();
	}

	// Escape(逃げる)
	shared_ptr<CatEscapeState> CatEscapeState::Instance()
	{
		static shared_ptr<CatEscapeState> instance(new CatEscapeState);
		return instance;
	}
	// ステートに入った瞬間
	void CatEscapeState::Enter(const shared_ptr<Cat>& Obj)
	{
		Obj->myState = L"Escape";
		Obj->SetCurrentAnimation(L"run");
		Obj->ChangeAnimation(Obj->GetCurrentAnimation());
		Obj->SetStandingFlg(false);

	}
	// このステートの間
	void CatEscapeState::Execute(const shared_ptr<Cat>& Obj)
	{
		// 1ゲージごとに
		// 嫌そうなアニメーション(アニメーション終わったら待機ステートor見つけるステート)

		// 自分の位置
		Vec3 myPos = Obj->GetComponent<Transform>()->GetPosition();

		// 気分ゲージが0で嫌そうな顔しながらおもちゃから離れる
		// 気まぐれでも離れる
		// 離れたら気分ゲージを[どのくらいか]回復して待機ステート

		// 仮でねこじゃらしから離れる
		// おもちゃの位置と自分の位置との距離
		Vec3 flatDis = Obj->ToyCatDistance();

		// 向き
		// おもちゃの位置が自分より左にあるなら(-なら)
		if (flatDis.x < 0.0f)
		{
			Obj->SetDir(Obj->Right);
		}
		// おもちゃの位置が自分より右にあるなら(+なら)
		else if (flatDis.x > 0.0f)
		{
			Obj->SetDir(Obj->Left);
		}
		Obj->GetComponent<Transform>()->SetScale(Vec3(Obj->GetDir(), 1.0f, 1.0f));

		// 移動
		// 距離が一定値離れていなければ(m_LeaveDis分)
		if (std::abs(flatDis.x) <= Obj->GetLeaveDistance().x && std::abs(flatDis.y) <= Obj->GetLeaveDistance().y)
		{
			// その位置から離れるように移動(足早に?)左か右に全力疾走
			flatDis.y = 0.0f;
			myPos -= flatDis.normalize() * App::GetApp()->GetElapsedTime() * 2.0f;

		}
		else
		{
			// 待機ステート
			Obj->GetStateMachine()->ChangeState(CatWaitState::Instance());
			return;
		}

		myPos.z = 0.5f;
		Obj->GetComponent<Transform>()->SetPosition(myPos);

	}
	// ステートを抜ける瞬間
	void CatEscapeState::Exit(const shared_ptr<Cat>& Obj)
	{
		// 気まぐれなら気まぐれ終わり
		if (Obj->GetWhimsicalFlg())
		{
			Obj->SetWhimsicalFlg(false);
		}
		else
		{
			//そうでないならMoodGageを初期値(3)に
			Obj->SetMoodGage(3);
		}

	}

	// CanPlay(じゃれる)
	shared_ptr<CatCanPlayState> CatCanPlayState::Instance()
	{
		static shared_ptr<CatCanPlayState> instance(new CatCanPlayState);
		return instance;
	}
	// ステートに入った瞬間
	void CatCanPlayState::Enter(const shared_ptr<Cat>& Obj)
	{
		Obj->myState = L"CanPlay";
		Obj->SetStandingFlg(false);

	}
	// このステートの間
	void CatCanPlayState::Execute(const shared_ptr<Cat>& Obj)
	{
		// じゃれるモーション
		// 猫同士のフラグが立ってたら猫vs猫
		if (Obj->GetCatfightFlg())
		{
			// 終わったら待機モーション
			//if (!Obj->GetChangeStateFlg()) {
			//	Obj->SetChangeStateFlg(true);
			// ほかの猫の位置と自分の位置との距離
			Vec3 myPos = Obj->GetComponent<Transform>()->GetPosition();
			Vec3 otherPos =  Obj->GetCollisionObject<Cat>()->GetComponent<Transform>()->GetPosition();
			Vec3 flatDis = otherPos - myPos;
			// 一定距離離れているなら近づく
			if (std::abs(flatDis.x) > 0.7f && std::abs(flatDis.y) > 0.1f) {
				// ほかの猫の位置が自分より左にあって向きが右なら(-なら)
				if (flatDis.x < 0.0f && Obj->GetDir() > 0.0f)
				{
					Obj->SetDir(Obj->Left);
				}
				// ほかの猫の位置が自分より右にあって向きが左なら(+なら)
				else if (flatDis.x > 0.0f && (int)Obj->GetDir() < 0.0f)
				{
					Obj->SetDir(Obj->Right);
				}
				Obj->GetComponent<Transform>()->SetScale(Vec3(Obj->GetDir(), 1.0f, 1.0f));

				// 移動
				if (Obj->GetCurrentAnimation() != L"walk")
				{
					Obj->SetCurrentAnimation(L"walk");
					Obj->ChangeAnimation(Obj->GetCurrentAnimation());
				}
				//if (!Obj->GetChangeStateFlg()) {
				//	Obj->SetChangeStateFlg(true);
				if (flatDis.length() >= 0.0001f) {
					myPos += flatDis.normalize() * App::GetApp()->GetElapsedTime();
					Obj->GetComponent<Transform>()->SetPosition(myPos);
				}
				//}
			}else
			{
				// 近かったら
				// 猫vs猫モーション
				if (Obj->GetCurrentAnimation() != L"fight")
				{
					Obj->StopSE();
					Obj->StartSE(L"se_Catfight");
					Obj->SetCurrentAnimation(L"fight");
					Obj->SetToAnimeMatrix(Obj->GetSmokeSpanMatrix());
					Obj->ChangeAnimation(Obj->GetCurrentAnimation());
				}
			// 猫vs猫時間終わったら
				if (Obj->CalcCatfightTime()) {
					if (auto otherCat = dynamic_pointer_cast<Cat>(Obj->GetCollisionObject<Cat>()))
					{
						Obj->RemoveCollisionObject(otherCat->GetThis<GameObject>());
						otherCat->InitCatfightTime();
						otherCat->SetCatfightFlg(false);
						otherCat->RemoveCollisionObject(Obj->GetThis<GameObject>());
						otherCat->SetToAnimeMatrix(Obj->GetSpanMatrix());
						otherCat->GetStateMachine()->ChangeState(CatWaitState::Instance());
					}
					Obj->InitCatfightTime();
					Obj->SetCatfightFlg(false);
					Obj->SetToAnimeMatrix(Obj->GetSpanMatrix());
					// 当たっているオブジェクトの初期化
					Obj->ResetCollisionObject();
					Obj->GetStateMachine()->ChangeState(CatWaitState::Instance());
					return;
				}
			}
			//}
		}
		else
		{
			// そうでないならおもちゃに合わせてモーション
			if (Obj->GetCurrentAnimation() != L"play")
			{
				// 気分値が最大値なら
				if (Obj->GetMoodGage() >= Obj->GetMaxMoodGage())
				{
					// ごろごろつける。
					Obj->StopSE();
					Obj->StartSE(WstringKey::se_Gorogoro, true);
				}

				Obj->SetCurrentAnimation(L"play");
				Obj->ChangeAnimation(Obj->GetCurrentAnimation());
			}
			// おもちゃの位置と自分の位置との距離
			Vec3 flatDis = Obj->ToyCatDistance();
			// おもちゃの位置が自分の向いている方向なら
			if ((flatDis.x > 0 && Obj->GetDir() > 0) || (flatDis.x < 0 && Obj->GetDir() < 0)) {
				// 一定距離離れているならついてくる（Approach）
				if (std::abs(flatDis.x) > 0.7f && std::abs(flatDis.y) > 0.1f) {
					//if (!Obj->GetChangeStateFlg()) {
					//	Obj->SetChangeStateFlg(true);
						Obj->GetStateMachine()->ChangeState(CatApproachState::Instance());
						return;
					//}
				}
			}
			else
			{
				// 向いてないなら待機
				//if (!Obj->GetChangeStateFlg()) {
				//	Obj->SetChangeStateFlg(true);
					Obj->GetStateMachine()->ChangeState(CatWaitState::Instance());
					return;
				//}
			}
		}

	}
	// ステートを抜ける瞬間
	void CatCanPlayState::Exit(const shared_ptr<Cat>& Obj)
	{

		//Obj->InitCatfightTime();
		if (Obj->GetCatDrawFlg())
		{
			Obj->SetCatDrawFlg(false);
		}
		Obj->SetDrawActive(true);
		//Obj->StopSE();
	}

	// Eating(ご飯食べる)
	shared_ptr<CatEatingState> CatEatingState::Instance()
	{
		static shared_ptr<CatEatingState> instance(new CatEatingState);
		return instance;
	}
	// ステートに入った瞬間
	void CatEatingState::Enter(const shared_ptr<Cat>& Obj)
	{
		Obj->myState = L"Eating";
		Obj->GetChildVisual()->GetComponent<Transform>()->SetPosition(Vec3(0.0f, 0.0f, 0.0f));// Vec3 All0.0fは親の位置
		Obj->SetStandingFlg(false);

	}
	// このステートの間
	void CatEatingState::Execute(const shared_ptr<Cat>& Obj)
	{
		Obj->StateEating();
	}
	// ステートを抜ける瞬間
	void CatEatingState::Exit(const shared_ptr<Cat>& Obj)
	{
		if (Obj->GetSEKey() == L"karikari")
		{
			Obj->StopSE();
		}
		Obj->SetNearMealFlg(false);
		Obj->InitCatfightTime();
		Obj->SetDrawLayer(2);
	}

	// Enter(入る)
	shared_ptr<CatEnterState> CatEnterState::Instance()
	{
		static shared_ptr<CatEnterState> instance(new CatEnterState);
		return instance;
	}
	// ステートに入った瞬間
	void CatEnterState::Enter(const shared_ptr<Cat>& Obj)
	{
		Obj->myState = L"Enter";
		Obj->SetCurrentAnimation(L"walk");
		Obj->ChangeAnimation(Obj->GetCurrentAnimation());
		Obj->SetEnterExitFlg(true);
	}
	// このステートの間
	void CatEnterState::Execute(const shared_ptr<Cat>& Obj)
	{
		// 入ってくる
		Obj->EnterExitMove(Obj->Move::Enter);
	}
	// ステートを抜ける瞬間
	void CatEnterState::Exit(const shared_ptr<Cat>& Obj)
	{
		Obj->InitCatfightTime();
		Obj->SetEnterExitFlg(false);
		Obj->StopSE();
		Obj->StartSE(L"nya");
	}

	// Exit(出ていく)
	shared_ptr<CatExitState> CatExitState::Instance()
	{
		static shared_ptr<CatExitState> instance(new CatExitState);
		return instance;
	}
	// ステートに入った瞬間
	void CatExitState::Enter(const shared_ptr<Cat>& Obj)
	{
		Obj->myState = L"Exit";
		Obj->SetCurrentAnimation(L"walk");
		Obj->ChangeAnimation(Obj->GetCurrentAnimation());
		Obj->SetEnterExitFlg(true);
		Obj->StartSE(L"nya");
	}
	// このステートの間
	void CatExitState::Execute(const shared_ptr<Cat>& Obj)
	{
		// 出る
		Obj->EnterExitMove(Obj->Move::Exit);
	}
	// ステートを抜ける瞬間
	void CatExitState::Exit(const shared_ptr<Cat>& Obj)
	{
		Obj->SetEnterExitFlg(false);
	}


	// 猫の視界(子オブジェクト)
	// 構築と破棄
	CatChildrenVisual::CatChildrenVisual(const shared_ptr<Stage>& StagePtr, const Vec3& Scale, const Vec3& Rotation, const Vec3& Position, const shared_ptr<GameObject>& ParentPtr)
		:
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position),
		m_ParentPtr(ParentPtr)
	{

	}
	CatChildrenVisual::~CatChildrenVisual(){}

	// 初期化
	void CatChildrenVisual::OnCreate()
	{
		// 親オブジェクトの設定
		auto ptrTransform = GetComponent<Transform>();
		ptrTransform->SetParent(m_ParentPtr);


		// 親に合わせた大きさ、回転、位置
		auto ptrMyTrans = AddComponent<Transform>();
		ptrMyTrans->SetScale(m_Scale);
		ptrMyTrans->SetRotation(Vec3(0.0f, 0.0f, 0.0f));
		ptrMyTrans->SetPosition(ParentDistance);


		// CollisionSphere衝突判定をつける
		auto ptrColl = AddComponent<CollisionSphere>();
		ptrColl->SetAfterCollision(AfterCollision::None);// Trigger設定
		//ptrColl->SetDrawActive(true);// デバッグ用
		// コリジョン除外
		ptrColl->AddExcludeCollisionGameObject(GetComponent<Transform>()->GetParent());

		// タグをつける
		AddTag(L"tag_CatVisual");

		// レイヤー
		SetDrawLayer(1);
	}

	// 更新
	void CatChildrenVisual::OnUpdate()
	{
		auto ptrMyTrans = GetComponent<Transform>();
		//auto ptrParentPos = ptrMyTrans->GetParent()->GetComponent<Transform>()->GetPosition();
		float parentDir = 1.0f;

		// 親が猫なら
		auto ptrParent = dynamic_pointer_cast<Cat>(ptrMyTrans->GetParent());
		if (ptrParent)
		{
			parentDir = ptrParent->GetDir();

		}
		// ご飯食べるステートでなければ
		if (!dynamic_pointer_cast<CatEatingState>(ptrParent->GetStateMachine()->GetCurrentState()))
		{
			ptrMyTrans->SetPosition(Vec3(ParentDistance.x * parentDir, ParentDistance.y, ParentDistance.z));
		}
	}

	// 衝突判定
	// 衝突した瞬間
	void CatChildrenVisual::OnCollisionEnter(shared_ptr<GameObject>& other)
	{
		auto ptrMyTrans = GetComponent<Transform>();
		//// 当たっているのが自分の親でないなら
		//if (other->GetThis<GameObject>() != ptrMyTrans->GetParent()) {
			// 親が猫なら
			if (auto ptrParent = dynamic_pointer_cast<Cat>(ptrMyTrans->GetParent()))
			{
				// 猫が逃げてるときは見ない
				if (ptrParent->GetStateMachine()->IsInState(CatEscapeState::Instance()))return;

				// 猫vs猫中なら帰る
				if (ptrParent->GetCatfightFlg())return;

				// 出入り中でなければ
				if (!ptrParent->GetEnterExitFlg()) {


					// 当たったのが猫視界なら
					if (other->FindTag(L"tag_CatVisual") || other->FindTag(WstringKey::tag_Icon) || other->FindTag(WstringKey::tag_IconBG) || other->FindTag(WstringKey::tag_BackIcon)) {
						//ptrParent->SetCollisionObject(other);
						return;
					}

					// 当たったのがご飯で食べ終わったなら見ない
					if (other->FindTag(WstringKey::tag_Meal) && ptrParent->GetFinishedEatFlg())
					{
						return;
					}

					// 当たっているオブジェクト配列に追加
					ptrParent->AddCollisionObject(other);

					// Findステートに切り替え
					//if (!ptrParent->GetChangeStateFlg()) {
					//	ptrParent->SetChangeStateFlg(true);
					// ご飯中ならEating内で処理(切り替えない)
					if (ptrParent->GetStateMachine()->GetCurrentState() != CatEatingState::Instance()) {
						ptrParent->GetStateMachine()->ChangeState(CatFindState::Instance());
						ptrParent->upps = L"!";
						return;
					}
					//}
				}
			}
		//}
	}

	// 衝突している間
	void CatChildrenVisual::OnCollisionExcute(shared_ptr<GameObject>& other)
	{
		// 背景以外なら

		auto ptrMyTrans = GetComponent<Transform>();
		//// 当たっているのが自分の親でないなら
		//if (other->GetThis<GameObject>() != ptrMyTrans->GetParent()) {
			// 親が猫なら
			if (auto ptrParent = dynamic_pointer_cast<Cat>(ptrMyTrans->GetParent()))
			{
				// 猫が逃げてるときは見ない
				if (ptrParent->GetStateMachine()->IsInState(CatEscapeState::Instance()))return;

				// 猫vs猫中なら帰る
				if (ptrParent->GetCatfightFlg())return;

				// 出入り中でなければ
				if (!ptrParent->GetEnterExitFlg()) {

					// すでに入っているものなら帰る
					if (ptrParent->CheckCollisionObject(other)) return;

					// 当たったのが猫視界なら何もしない（タグで）(家具は当たっている間は見ない)
					if (other->FindTag(L"tag_CatVisual") || other->FindTag(WstringKey::tag_Icon) || other->FindTag(WstringKey::tag_IconBG) || other->FindTag(WstringKey::tag_BackIcon) || other->FindTag(L"tag_Furniture")) {
						return;
						// m_CollisionObjがnullptrなら
						//if (!ptrParent->GetCollisionObject())
						//{
						//	ptrParent->SetCollisionObject(other);
						//	// Findステートに切り替え
						//	//ptrParent->GetStateMachine()->ChangeState(CatFindState::Instance());
						//	//ptrParent->upps = L"!";
						//}
					}

					// 当たったのがご飯で食べ終わったなら見ない
					if (other->FindTag(WstringKey::tag_Meal) && ptrParent->GetFinishedEatFlg())
					{
						return;
					}

					// 当たっているオブジェクト配列に追加
					ptrParent->AddCollisionObject(other);

					// Findステートに切り替え
					//ptrParent->GetStateMachine()->ChangeState(CatFindState::Instance());
					//ptrParent->upps = L"!";
				}
			}
		//}
	}

	void CatChildrenVisual::OnCollisionExit(shared_ptr<GameObject>& other)
	{
		// 背景以外なら

		auto ptrMyTrans = GetComponent<Transform>();
		//// 当たっているのが自分の親でないなら
		//if (other->GetThis<GameObject>() != ptrMyTrans->GetParent()) {

			// 親が猫なら
			if (auto ptrParent = dynamic_pointer_cast<Cat>(ptrMyTrans->GetParent()))
			{
				// 猫が逃げてるときは見ない
				if (ptrParent->GetStateMachine()->IsInState(CatEscapeState::Instance()))return;

				// 猫vs猫中なら帰る
				if (ptrParent->GetCatfightFlg())return;

				// 出入り中でなければ
				if (!ptrParent->GetEnterExitFlg()) {

					// 待機ステートに切り替え
					//ptrParent->GetStateMachine()->ChangeState(CatWaitState::Instance());

					// 気分ゲージマックスならApproachステート
					//if (ptrParent->GetMoodGage() >= ptrParent->GetMaxMoodGage())
					//{
					//	ptrParent->GetStateMachine()->ChangeState(CatApproachState::Instance());
					//}

					// 当たったのが猫視界なら
					if (other->FindTag(L"tag_CatVisual") || other->FindTag(WstringKey::tag_Icon) || other->FindTag(WstringKey::tag_IconBG) || other->FindTag(WstringKey::tag_BackIcon)) {
						// nullptrに戻しておく（何も入ってない）
						//ptrParent->SetCollisionObject(nullptr);
						return;
					}
					// 当たったのがご飯で食べ終わったなら見ない
					if (other->FindTag(WstringKey::tag_Meal) && ptrParent->GetFinishedEatFlg())
					{
						return;
					}
					// 当たっているオブジェクト配列からotherを削除
					ptrParent->RemoveCollisionObject(other);

					// デバッグ用
					ptrParent->upps = L"";
				}
			}
		//}
	}

	// ゲッター
	// 猫と視界の距離
	Vec3 CatChildrenVisual::GetParentDistance()
	{
		return ParentDistance;
	}

	// 親（絶対猫入ってるはず）
	shared_ptr<Cat> CatChildrenVisual::GetMyParent()
	{
		return dynamic_pointer_cast<Cat>(GetComponent<Transform>()->GetParent());
	}

	// 猫ビックリマーク本体
	// 構築と破棄
	CatExclamationMark::CatExclamationMark(const shared_ptr<Stage>& StagePtr, const Vec3& Scale, const Vec3& Rotation, const Vec3& Position, const shared_ptr<GameObject>& ParentPtr)
		:
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position),
		m_ParentPtr(ParentPtr)
	{
	}
	CatExclamationMark::~CatExclamationMark(){}

	// 初期化
	void CatExclamationMark::OnCreate()
	{
		// 親オブジェクトの設定
		auto ptrTrans = GetComponent<Transform>();
		ptrTrans->SetParent(m_ParentPtr);
		// 親が猫なら
		auto ptrParent = dynamic_pointer_cast<Cat>(ptrTrans->GetParent());
		if (ptrParent)
		{
			m_parentDir = ptrParent->GetDir();
		}


		// 親に合わせた大きさ、回転、位置
		auto ptrMyTrans = AddComponent<Transform>();
		ptrMyTrans->SetScale(Vec3(m_Scale.x * -m_parentDir, m_Scale.y, m_Scale.z));
		ptrMyTrans->SetRotation(Vec3(0.0f, 0.0f, 0.0f));
		ptrMyTrans->SetPosition(ParentDistance);
		
		// 親の気分ゲージ(親が猫か判断して)
		auto parentCat = dynamic_pointer_cast<Cat>(m_ParentPtr);
		m_beforeMoodGage = parentCat->GetMoodGage();
		float gageRatio = (float)parentCat->GetMoodGage() / (float)parentCat->GetMaxMoodGage();
		m_maxSize = 1.0f * gageRatio;
		// アニメチップ作成
		float tipWidth = 1.0f;
		float tipHeight = 1.0f;
		for (int col = 0; col < 1; col++)
		{
			for (int row = 0; row < 1; row++)
			{
				// 幅
				m_AnimTip[col][row].left = tipWidth * row;							// 左
				m_AnimTip[col][row].right = m_AnimTip[col][row].left + tipWidth;	// 右
				// 高さ
				m_AnimTip[col][row].top = tipHeight * col;							// 上
				m_AnimTip[col][row].bottom = m_AnimTip[col][row].top + tipHeight;	// 下
			}
		}

		// 描画するテクスチャの設定
		m_Vertices = 
		{
			{Vec3(-HalfSize, m_maxSize, 0.0f),White, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, (m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom - m_maxSize))},	// 左上
			{Vec3(HalfSize, m_maxSize, 0.0f),White, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right,(m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom - m_maxSize))},	// 右上
			{Vec3(-HalfSize, -0.0f,   0.0f),White, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},	// 左下
			{Vec3( HalfSize, -0.0f,   0.0f),White, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}	// 右下

		};
		m_Indices = {
			0, 1, 2,
			1, 3, 2
		};

		// 頂点とインデックスを指定してスプライトを作成
		auto PtrDraw = this->AddComponent<PCTStaticDraw>();
		PtrDraw->CreateOriginalMesh(m_Vertices, m_Indices);
		PtrDraw->SetOriginalMeshUse(true);
		PtrDraw->SetTextureResource(WstringKey::tx_CatExclamationMark);
		PtrDraw->SetDepthStencilState(DepthStencilState::None);
		
		// 透明処理
		SetAlphaActive(true);
		SetDrawLayer(6);

		// 子オブジェクト生成
		m_childPtr = GetStage()->AddGameObject<CatExclamationMarkFrame>(m_Scale, m_Rotation, m_Position, GetThis<GameObject>())->GetThis<GameObject>();


	}
	// 更新
	void CatExclamationMark::OnUpdate()
	{
		// 画像の更新
		SpriteUpdate();

		auto ptrMyTrans = GetComponent<Transform>();

		// 親が猫なら
		auto ptrParent = dynamic_pointer_cast<Cat>(ptrMyTrans->GetParent());
		if (ptrParent)
		{
			m_parentDir = ptrParent->GetDir();
		}

		// 位置更新
		ptrMyTrans->SetPosition(Vec3(m_parentDir * ParentDistance.x, ParentDistance.y, ParentDistance.z));

		// 向き更新
		ptrMyTrans->SetScale(Vec3(-m_parentDir * m_Scale.x, m_Scale.y, m_Scale.z));
	}

	// 画像更新
	void CatExclamationMark::SpriteUpdate()
	{
		// ゲージが切り替わったなら更新,変わってないなら帰る
		auto parentCat = dynamic_pointer_cast<Cat>(GetComponent<Transform>()->GetParent());
		auto parentMoodGage = parentCat->GetMoodGage();
		if (m_beforeMoodGage == parentMoodGage)return;

		// ゲージの比率
		float gageRatio = (float)parentMoodGage / (float)parentCat->GetMaxMoodGage();
		m_maxSize = 1.0f * gageRatio;

		// 新しい頂点の設定
		vector<VertexPositionColorTexture> newVertices =
		{
			{Vec3(-HalfSize, m_maxSize, 0.0f),White, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, (m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom - m_maxSize))},	// 左上
			{Vec3( HalfSize, m_maxSize, 0.0f),White, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right,(m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom - m_maxSize))},	// 右上
			{Vec3(-HalfSize, -0.0f,     0.0f),White, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},	// 左下
			{Vec3( HalfSize, -0.0f,     0.0f),White, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}	// 右下
		};
		// 更新
		auto ptrDraw = GetComponent<PCTStaticDraw>();
		ptrDraw->UpdateVertices<VertexPositionColorTexture>(newVertices);
		m_beforeMoodGage = parentMoodGage;
	}

	// ゲッター
	// 親の向き
	float CatExclamationMark::GetParentDir()const
	{
		return m_parentDir;
	}
	// 子オブジェクト
	shared_ptr<GameObject>& CatExclamationMark::GetChild()
	{
		return m_childPtr;
	}

	// 猫ビックリマークフレーム
	// 構築と破棄
	CatExclamationMarkFrame::CatExclamationMarkFrame(const shared_ptr<Stage>& StagePtr, const Vec3& Scale, const Vec3& Rotation, const Vec3& Position, const shared_ptr<GameObject>& ParentPtr)
		:
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position),
		m_ParentPtr(ParentPtr)
	{
	}
	CatExclamationMarkFrame::~CatExclamationMarkFrame(){}

	// 初期化
	void CatExclamationMarkFrame::OnCreate()
	{
		// 親オブジェクトの設定
		auto ptrTrans = GetComponent<Transform>();
		ptrTrans->SetParent(m_ParentPtr);

		auto ptrParent = dynamic_pointer_cast<CatExclamationMark>(ptrTrans->GetParent());
		//auto ptrParent = dynamic_pointer_cast<Cat>(ptrMyTrans->GetParent()->GetComponent<Transform>()->GetParent());
		if (ptrParent)
		{
			m_parentDir = ptrParent->GetParentDir();
		}



		// 親に合わせた大きさ、回転、位置
		auto ptrMyTrans = AddComponent<Transform>();
		ptrMyTrans->SetScale(Vec3(m_Scale.x * -m_parentDir, m_Scale.y, m_Scale.z));
		ptrMyTrans->SetRotation(Vec3(0.0f, 0.0f, 0.0f));
		ptrMyTrans->SetPosition(Vec3(0.0f, 0.0f, 0.0f));

		// 描画するテクスチャの設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);// 白
		vector<VertexPositionColorTexture> vertices =
		{
			{Vec3(-HalfSize, m_maxSize,  0), white, Vec2(0.0f, 0.0f)},
			{Vec3(HalfSize,  m_maxSize,  0), white, Vec2(1.0f, 0.0f)},
			{Vec3(-HalfSize, -0.0f,    0), white, Vec2(0.0f, 1.0f)},
			{Vec3( HalfSize, -0.0f,    0), white, Vec2(1.0f, 1.0f)}
		};
		vector<uint16_t> indices = {
			0, 1, 2,
			1, 3, 2
		};

		// 頂点とインデックスを指定してスプライト作成
		auto ptrSpriteDraw = this->AddComponent<PCTStaticDraw>();
		ptrSpriteDraw->CreateOriginalMesh(vertices, indices);
		ptrSpriteDraw->SetOriginalMeshUse(true);
		ptrSpriteDraw->SetTextureResource(WstringKey::tx_CatExclamationMarkFrame);
		ptrSpriteDraw->SetDepthStencilState(DepthStencilState::None);
		// 透明処理
		SetAlphaActive(true);


		// レイヤー
		SetDrawLayer(5);

	}

	// 更新
	void CatExclamationMarkFrame::OnUpdate()
	{
		auto ptrMyTrans = GetComponent<Transform>();

		// 親がゲージ本体なら
		auto ptrParent = dynamic_pointer_cast<CatExclamationMark>(ptrMyTrans->GetParent());
		//auto ptrParent = dynamic_pointer_cast<Cat>(ptrMyTrans->GetParent()->GetComponent<Transform>()->GetParent());
		if (ptrParent)
		{
			m_parentDir = ptrParent->GetParentDir();
		}

		// 位置更新
		//ptrMyTrans->SetPosition(Vec3(parentDir * ParentDis.x, ParentDis.y, ParentDis.z));

		// 向き更新
		ptrMyTrans->SetScale(Vec3(-m_parentDir * m_Scale.x, m_Scale.y, m_Scale.z));
	}

// ロードシーン用ねこクラス実体
// 構築と破棄
	LoadCat::LoadCat(
		const shared_ptr<Stage>& StagePtr,
		const wstring& BaseDir,
		const wstring& XmlfileName,
		const wstring& StartAnimeName,
		const Vec3& Scale,
		const Vec3& Rotation,
		const Vec3& Position)
		:
		SS5ssae(StagePtr, BaseDir, XmlfileName, StartAnimeName),
		m_currentAnim(StartAnimeName),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	LoadCat::~LoadCat() {}
	// 初期化
	void LoadCat::OnCreate()
	{
		// 大きさ、回転、位置
		auto ptrTransform = GetComponent<Transform>();
		ptrTransform->SetScale(m_Scale);
		ptrTransform->SetRotation(m_Rotation);
		ptrTransform->SetPosition(m_Position);

		Mat4x4 spanMat; // モデルとトランスフォームの間の差分行列
		spanMat.affineTransformation(
			Vec3(0.1f, 0.1f, 0.1f),
			Vec3(0.0f),
			Vec3(0.0f),
			Vec3(0.0f)
		);
		// 親クラスの初期化呼び出し
		SS5ssae::OnCreate();
		SetToAnimeMatrix(spanMat);



		//値は秒あたりのフレーム数
		SetFps(29.0f);

		// 透明処理
		SetAlphaActive(true);
		// レイヤー
		SetDrawLayer(2);

		m_beforeAnim = m_currentAnim;
		m_animNum = rand() % State::karikari;
	}
	void LoadCat::OnUpdate()
	{
		switch (m_animNum)
		{
		case State::wait:
			m_currentAnim = L"wait";
			break;
		case State::sleep:
			m_currentAnim = L"sleep";
			break;
		case State::stretch:
			m_currentAnim = L"stretch";
			break;
		case State::nails:
			m_currentAnim = L"nails";
			break;
		case State::walk:
			m_currentAnim = L"walk";
			break;

		case State::play:
			m_currentAnim = L"play";

			break;
		case State::karikari:
			m_currentAnim = L"karikari";
			break;

		default:
			break;
		}
		if (m_currentAnim != m_beforeAnim) {
			m_beforeAnim = m_currentAnim;
			ChangeAnimation(m_currentAnim);
		}
		// アニメーションの更新
		float elapsedTime = App::GetApp()->GetElapsedTime();
		UpdateAnimeTime(elapsedTime);

	}
}
//end basecross
