/*!
@file Particle.cpp
@brief パーティクルクラス実体
*/

#include "stdafx.h"
#include "Project.h"


namespace basecross {

	// 気分パーティクルクラス
	// 構築と破棄
	MoodParticle::MoodParticle(shared_ptr<Stage>& StagePtr, const wstring& TxKey)
		:
		MultiParticle(StagePtr),
		m_TextureKey(TxKey)
	{
	}
	MoodParticle::~MoodParticle() {}

	// 初期化
	void MoodParticle::OnCreate()
	{
		SetDrawLayer(20);
	}

	// 表示
	void MoodParticle::InsertMoodParticle(const Vec3& Pos, const Col4& Color)
	{
		// パーティクルの数、位置、画像
		auto particlePtr = InsertParticle(6);
		particlePtr->SetEmitterPos(Pos);
		particlePtr->SetTextureResource(m_TextureKey);
		particlePtr->SetMaxTime(0.5f);
		for (auto& particleSprite : particlePtr->GetParticleSpriteVec())
		{
			// パーティクルの大きさ	
			particleSprite.m_LocalScale *= 0.15f;
			// パーティクルの生成位置
			particleSprite.m_LocalPos.x = Util::RandZeroToOne() * 0.1f - 0.05f;
			particleSprite.m_LocalPos.y = Util::RandZeroToOne() * 0.1f;
			particleSprite.m_LocalPos.z = Util::RandZeroToOne() * -0.1f - 0.05f;
			// パーティクルの移動速度の指定
			particleSprite.m_Velocity = particleSprite.m_LocalPos * 5.0f;
			// 色指定
			particleSprite.m_Color = Color;
		}
	}
}

//end basecross

