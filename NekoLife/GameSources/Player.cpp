/*!
@file Player.cpp
@brief プレイヤーなど実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross{

	//コンストラクタ
	PlayerMouse::PlayerMouse(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr),
		m_Latepos(0.0f,0.0f,0.0f),
		m_Timer(0.0f)
	{

	}

	//デストラクタ
	PlayerMouse::~PlayerMouse() {

	}

	//初期化
	void PlayerMouse::OnCreate() {
		//文字列を追加
		auto ptrString = AddComponent<StringSprite>();
		ptrString->SetText(L"");
		ptrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));
	}

	//更新
	void PlayerMouse::OnUpdate() {
		auto Pad = App::GetApp()->GetInputDevice().GetControlerVec()[0];
		auto KeyState = App::GetApp()->GetInputDevice().GetKeyState();
		m_MousePoint = KeyState.m_MouseClientPoint;
		auto deltaTime = App::GetApp()->GetElapsedTime();
		m_Timer += deltaTime;

		//左クリックを押した瞬間
		if (KeyState.m_bPressedKeyTbl[VK_LBUTTON]) {
			OnLButtonClick();
		}
		// 左クリックを離した瞬間
		else if (KeyState.m_bUpKeyTbl[VK_LBUTTON]) {
			OnLButtonUp();
		}

		if (Pad.bConnected) {
			if (Pad.wPressedButtons & XINPUT_GAMEPAD_A) {
			}
		}

		if (m_Timer > 3.0f) {
			GetMouseMeasu();
			m_Timer = 0.0f;
		}
	}

	void PlayerMouse::OnUpdate2() {
		//DrawString();
	}

	//マウスからレイを飛ばしベクトルを返す
	void PlayerMouse::GetMouseRay(Vec3& Near, Vec3& Far) {
		Mat4x4 wolde, view, proj;
		wolde.affineTransformation(
			Vec3(1.0f, 1.0f, 1.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			Vec3(0.0f, 0.0f, 0.0f)
		);

		auto stage = GetStage();
		auto PtrCamera = stage->GetView()->GetTargetCamera();
		view = PtrCamera->GetViewMatrix();
		proj = PtrCamera->GetProjMatrix();
		auto viewport = stage->GetView()->GetTargetViewport();

		// 画面空間からゲーム内にベクトルを飛ばしゲーム内のベクトルを返す
		Near = XMVector3Unproject(
			Vec3((float)m_MousePoint.x, (float)m_MousePoint.y, 0.0f),
			viewport.TopLeftX,
			viewport.TopLeftY,
			viewport.Width,
			viewport.Height,
			viewport.MinDepth,
			viewport.MaxDepth,
			proj,
			view,
			wolde
		);

		Far = XMVector3Unproject(
			Vec3((float)m_MousePoint.x, (float)m_MousePoint.y, 1.0f),
			viewport.TopLeftX,
			viewport.TopLeftY,
			viewport.Width,
			viewport.Height,
			viewport.MinDepth,
			viewport.MaxDepth,
			proj,
			view,
			wolde
		);
	}

	// 選択されたオブジェクトを解除
	void PlayerMouse::SelectClear() {
		for (auto& v : GetStage()->GetGameObjectVec()) {
			auto PsPtr = dynamic_pointer_cast<ActivePsObject>(v);
			if (PsPtr) {
				PsPtr->SetSelect(false);
			}
		}
	}

	//左クリックされた時の処理
	void PlayerMouse::OnLButtonClick() {
		SelectClear();
		auto stage = GetStage();
		auto PtrCamera = stage->GetView()->GetTargetCamera();
		Vec3 Eye = PtrCamera->GetEye();

		vector<shared_ptr<ActivePsObject>> ObjVec;
		Vec3 NearPos, FarPos;
		GetMouseRay(NearPos, FarPos);
		for (auto& v : stage->GetGameObjectVec()) {
			auto PsPtr = dynamic_pointer_cast<ActivePsObject>(v);
			if (PsPtr) {
				auto ColObb = PsPtr->GetComponent<CollisionObb>(false);
				if (ColObb) {
					auto Obb = ColObb->GetObb();
					if (HitTest::SEGMENT_OBB(NearPos, FarPos, Obb)) {
						ObjVec.push_back(PsPtr);
						auto ptrCatnip = GetStage()->GetSharedGameObject<Catnip>(L"Catnip");
						ptrCatnip->GetComponent<RigidbodyBox>()->SetMotionType(PsMotionType::MotionTypeActive);
					}
				}
			}
		}
		if (ObjVec.size() > 0) {
			float MinSpan = 1000.0f;
			shared_ptr<ActivePsObject> SelectObj = nullptr;
			for (auto& v : ObjVec) {
				float span = length(v->GetComponent<Transform>()->GetPosition() - Eye);
				if (span < MinSpan) {
					MinSpan = span;
					SelectObj = v;
				}
			}
			if (SelectObj) {
				SelectObj->SetSelect(true);
			}
		}
	}

	//左クリックが離された時
	void PlayerMouse::OnLButtonUp() {
		SelectClear();
		auto ptrCatnip = GetStage()->GetSharedGameObject<Catnip>(L"Catnip");
		ptrCatnip->GetComponent<RigidbodyBox>()->SetMotionType(PsMotionType::MotionTypeFixed);
	}

	//マウスの速度を測定
	float PlayerMouse::GetMouseMeasu() {
		m_Timer += App::GetApp()->GetElapsedTime();
		float spMagnitude = 0.0f;
		if (m_Timer >= 1.0f) {
			auto catnip = GetStage()->GetSharedGameObject<Catnip>(L"Catnip");
			auto pos = catnip->GetComponent<Transform>()->GetPosition();
			auto mouseSpeed = ((pos - m_Latepos) / App::GetApp()->GetElapsedTime());
			spMagnitude = mouseSpeed.x * mouseSpeed.x + mouseSpeed.y * mouseSpeed.y + mouseSpeed.z * mouseSpeed.z;
			m_Latepos = pos;
			m_Timer = 0.0f;
		}

		return spMagnitude;
	}

	//文字列表示
	void PlayerMouse::DrawString() {
		//デバッグ用の文字列を表示
		wstring Message(L"デバッグ\n");
		Message += L"X=" + Util::UintToWStr(m_MousePoint.x) += L"\n";
		Message += L"Y=" + Util::UintToWStr(m_MousePoint.y) += L"\n";

		auto CatnipPos = GetTypeStage<GameStage>()->GetSharedGameObject<Catnip>(L"Catnip")->GetComponent<Transform>()->GetPosition();
		wstring Catnip(L"CatnipPos:\t");
		Catnip += L"X=" + Util::FloatToWStr(CatnipPos.x, 6, Util::FloatModify::Fixed) + L",\t";
		Catnip += L"Y=" + Util::FloatToWStr(CatnipPos.y, 6, Util::FloatModify::Fixed) + L",\t";
		Catnip += L"Z=" + Util::FloatToWStr(CatnipPos.z, 6, Util::FloatModify::Fixed) + L"\n";

		Vec3 NearPos, FarPos;
		GetMouseRay(NearPos, FarPos);

		wstring MouseRayNear(L"MouseRayNear:\t");
		MouseRayNear += L"X=" + Util::FloatToWStr(NearPos.x, 6, Util::FloatModify::Fixed) + L",\t";
		MouseRayNear += L"Y=" + Util::FloatToWStr(NearPos.y, 6, Util::FloatModify::Fixed) + L",\t";
		MouseRayNear += L"Z=" + Util::FloatToWStr(NearPos.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring MouseRayFar(L"MouseRayFar:\t");
		MouseRayFar += L"X=" + Util::FloatToWStr(FarPos.x, 6, Util::FloatModify::Fixed) + L",\t";
		MouseRayFar += L"Y=" + Util::FloatToWStr(FarPos.y, 6, Util::FloatModify::Fixed) + L",\t";
		MouseRayFar += L"Z=" + Util::FloatToWStr(FarPos.z, 6, Util::FloatModify::Fixed) + L"\n";

		m_Timer += App::GetApp()->GetElapsedTime();
		if (m_Timer >= 0.5f) {
			m_speed = GetMouseMeasu();
			m_Timer = 0.0f;
		}

		wstring MouseSpeed(L"MouseSpeed:\t");
		MouseSpeed += L"Speed" + Util::FloatToWStr(m_speed, 6, Util::FloatModify::Fixed);

		wstring st = Message + Catnip + MouseSpeed;

		//文字列をつける
		auto PtrString = GetComponent<StringSprite>();
		PtrString->SetText(st);
	}

	//-----------------------------------------------------------------------------------------------------
	//プレイヤーのポインター
	//-----------------------------------------------------------------------------------------------------
	PlayerPointer::PlayerPointer(const shared_ptr<Stage>& StagePtr,
		const Vec3& Scale,
		const Vec3& Rotate,
		const Vec3& Position,
		const wstring& BaseDir
	) :
		SS5ssae(StagePtr, BaseDir, L"RussianBule (1).ssae", L"nekojarasi"),
		m_Scale(Scale),
		m_Rotate(Rotate),
		m_Position(Position),
		m_Status(PointerStatus),
		m_BeforStatus(PointerStatus),
		debug(L"false"),
		m_CheckColOut(false),
		m_CheckHitOther(false),
		m_StartTime(false),
		m_EndTime(true),
		m_MoodUpFlg(true),
		m_CatSeeFlg(false),
		m_isInIconBG(false),
		m_YarnballSelect(false),
		m_AnimLoop(false),
		m_LandingPoint(-1.0f),
		m_CatnipTimer(0.0f),
		m_CatSeeTimer(0.0f),
		m_ButtonHoldTimer(0.0f),
		m_CatnipRapidCount(0)
	{
	}

	PlayerPointer::~PlayerPointer() {};

	//プレイヤーの移動
	void PlayerPointer::PlayerMove(CONTROLER_STATE cntlVec) {
		float deltaTime = App::GetApp()->GetElapsedTime();

		//コントローラーの取得
		float fThumbLX = 0.0f;
		float fThumbLY = 0.0f;
		float speed = 5.0f;
		if (cntlVec.bConnected) {
			fThumbLX = cntlVec.fThumbLX;
			fThumbLY = cntlVec.fThumbLY;
		}

		//移動の処理
		Vec3 dir(fThumbLX, fThumbLY, 0.0f);
		auto pos = GetComponent<Transform>()->GetPosition();
		pos += dir * deltaTime * speed;
		GetComponent<Transform>()->SetPosition(pos);
	}

	//ポインターの操作
	void PlayerPointer::PointerMove() {
		auto cntlVec = App::GetApp()->GetInputDevice().GetControlerVec()[0];
		PlayerMove(cntlVec);

		auto ptrTrans = GetComponent<Transform>();
		ptrTrans->SetScale(Vec3(0.5f));

		if (cntlVec.wButtons & XINPUT_GAMEPAD_A) {
			GetComponent<PCTStaticDraw>()->SetTextureResource(WstringKey::tx_DownPointer);
		}
		if (cntlVec.wReleasedButtons & XINPUT_GAMEPAD_A) {
			SelectItem();
		}
	}

	//猫じゃらしの操作
	void PlayerPointer::CatnipOperate() {
		auto ptrTrans = GetComponent<Transform>();
		auto scale = ptrTrans->GetScale();
		float deltaTime = App::GetApp()->GetElapsedTime();

		//コントローラーの取得
		auto cntlVec = App::GetApp()->GetInputDevice().GetControlerVec()[0];
		PlayerMove(cntlVec);

		//タイマー開始
		if (m_StartTime) {
			m_CatnipTimer += deltaTime;
		}

		if (m_CatSeeFlg && m_CatSeeTimer < 3.0f) {
			m_CatSeeTimer += deltaTime;
		}
		else if (m_CatSeeTimer > 3.0f && m_CatSeeFlg) {
			m_CatSeeFlg = false;
			m_CatSeeTimer = 0.0f;
		}

		//連打制限時間
		if (m_CatnipTimer > 1.0f) {
			m_CatnipTimer = 0.0f;
			m_CatnipRapidCount = 0;
			m_StartTime = false;
			m_EndTime = true;
		}

		if (!m_MoodUpFlg && m_EndTime) {
			m_MoodUpFlg = true;
		}

		//Aボタンの処理
		if (cntlVec.wPressedButtons & XINPUT_GAMEPAD_A) {
			float minTime = 0.0f;
			float maxTime = 1.0f;
			//タイマーが動いてなければture
			if (m_EndTime) {
				m_StartTime = true;
				m_EndTime = false;
			}

			//タイマーが一秒未満の時、連打カウントを追加
			if (m_CatnipTimer > minTime && m_CatnipTimer < maxTime) {
				m_CatnipRapidCount++;
			}

			m_AnimLoop = true;

			//SE再生
			auto XAPtr = App::GetApp()->GetXAudio2Manager();
			XAPtr->Start(WstringKey::se_CatnipSwing, 0, 1.0f);
		}
	}

	//ネズミラジコンの操作
	void PlayerPointer::MouseRCOperate() {
		auto ptrTrans = GetComponent<Transform>();
		auto cntlVec = App::GetApp()->GetInputDevice().GetControlerVec()[0];

		//コントローラーの取得
		float fThumbLX = 0.0f;
		float fThumbLY = 0.0f;
		float speed = 5.0f;
		if (cntlVec.bConnected) {
			fThumbLX = cntlVec.fThumbLX;
			fThumbLY = cntlVec.fThumbLY;
		}

		//移動の処理
		Vec3 dir(0.0f, fThumbLY, 0.0f);
		auto pos = GetComponent<Transform>()->GetPosition();
		pos.x = 3.0f;
		float deltaTime = App::GetApp()->GetElapsedTime();
		pos += dir * deltaTime * speed;
		GetComponent<Transform>()->SetPosition(pos);

		//上下の稼働範囲
		if (pos.y < -1.0f) {
			pos.y = -1.0f;
			GetComponent<Transform>()->SetPosition(pos);
		}
		if (pos.y > 1.2f) {
			pos.y = 1.2f;
			GetComponent<Transform>()->SetPosition(pos);
		}

		auto stage = GetStage();
		auto ptrMouse = stage->GetSharedGameObject<MouseRCInstallation>(L"MouseRC");

		if (ptrMouse->GetOffScreen()) {
			if (cntlVec.wPressedButtons & XINPUT_GAMEPAD_A) {
				m_Status = PointerStatus;
				m_BeforStatus = m_Status;

				auto pos = ptrTrans->GetPosition();
				ptrMouse->Installation();
				ptrMouse->GetComponent<Transform>()->SetPosition(pos);
			}
		}
	}

	//毛糸玉の操作
	void PlayerPointer::YarnballOperate() {
		auto ptrTrans = GetComponent<Transform>();
		float deltaTime = App::GetApp()->GetElapsedTime();
		auto cntlVec = App::GetApp()->GetInputDevice().GetControlerVec()[0];
		auto XAPtr = App::GetApp()->GetXAudio2Manager();
		//コントローラーの取得
		float fThumbLX = 0.0f;
		float fThumbLY = 0.0f;
		float speed = 5.0f;
		if (cntlVec.bConnected) {
			fThumbLX = cntlVec.fThumbLX;
			fThumbLY = cntlVec.fThumbLY;
		}

		//移動の処理
		Vec3 dir(fThumbLX, 0.0f, 0.0f);
		auto pos = GetComponent<Transform>()->GetPosition();
		pos += dir * deltaTime * speed;
		pos.y = -1.0f;
		GetComponent<Transform>()->SetPosition(pos);
		ptrTrans->SetScale(Vec3(1.25f));

		//毛糸玉の選択・毛糸玉を投げる
		if (cntlVec.wPressedButtons & XINPUT_GAMEPAD_A && !m_YarnballSelect) {
			m_YarnballSelect = true;
		}
		else if (cntlVec.wButtons & XINPUT_GAMEPAD_A && m_YarnballSelect) {
			float HoldLimitTime = 2.3f;
			m_ButtonHoldTimer += App::GetApp()->GetElapsedTime();
			if (m_ButtonHoldTimer <= HoldLimitTime) {
				m_LandingPoint += deltaTime;
			}
		}
		else if (cntlVec.wReleasedButtons & XINPUT_GAMEPAD_A && m_YarnballSelect) {
			auto pos = ptrTrans->GetPosition();
			auto ball = GetStage()->AddGameObject<YarnBallInstallation>(Vec3(1.25f), Vec3(pos), m_LandingPoint);
			m_LandingPoint = -1.0f;
			m_Status = PointerStatus;
			m_BeforStatus = m_Status;
			m_ButtonHoldTimer = 0.0f;
			m_YarnballSelect = false;
			XAPtr->Start(WstringKey::se_Yarnball, 0, 0.5f);

			auto gm = GameManager::GetInstance();
			gm->AddMissionYarnBall(1);
		}
		
	}

	//初期化
	void PlayerPointer::OnCreate() {
		auto ptrTrans = GetComponent<Transform>();
		ptrTrans->SetScale(m_Scale);
		ptrTrans->SetRotation(m_Rotate);
		ptrTrans->SetPosition(m_Position);

		Mat4x4 spanMat;
		spanMat.affineTransformation(
			Vec3(0.0f),
			Vec3(0.0f),
			Vec3(0.0f),
			Vec3(0.0f)
		);
		SS5ssae::OnCreate();
		SetToAnimeMatrix(spanMat);

		SetFps(30.0f);

		//CollisionObb衝突判定
		auto ptrCol = AddComponent<CollisionObb>();
		ptrCol->SetAfterCollision(AfterCollision::None);
		//ptrCol->SetDrawActive(true);

		//スプライト作成
		Col4 White(1.0f, 1.0f, 1.0f, 1.0f);
		float halfsize = 0.5f;
		vector<VertexPositionColorTexture> vertex = 
		{
			{Vec3(-halfsize, +halfsize, 0), White, Vec2(0.0f, 0.0f)},
			{Vec3(+halfsize, +halfsize, 0), White, Vec2(1.0f, 0.0f)},
			{Vec3(-halfsize, -halfsize, 0), White, Vec2(0.0f, 1.0f)},
			{Vec3(+halfsize, -halfsize, 0), White, Vec2(1.0f, 1.0f)}
		};

		vector<uint16_t> indices =
		{
			0, 1, 2,
			1, 3, 2
		};

		//スプライト作成
		auto ptrStaticDraw = AddComponent<PCTStaticDraw>();
		ptrStaticDraw->CreateOriginalMesh(vertex, indices);
		ptrStaticDraw->SetOriginalMeshUse(true);
		ptrStaticDraw->SetDepthStencilState(DepthStencilState::None);
		//透明処理
		SetAlphaActive(true);
		//レイヤーを10番目に設定
		SetDrawLayer(10);
		//文字列を追加
		//auto ptrString = AddComponent<StringSprite>();
		//ptrString->SetText(L"");
		//ptrString->SetBackColor(Col4(0.0f, 0.0f, 0.0f, 0.5f));
		//ptrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));
	}

	//更新
	void PlayerPointer::OnUpdate() {
		m_InputHandler.PushHandler(GetThis<PlayerPointer>());
		SwitchItem(m_Status);
		SwitchPointer();
		NotOutScreen();
		auto deltaTime = App::GetApp()->GetElapsedTime();
		UpdateAnimeTime(deltaTime);

		//アニメーションの再生
		if (m_AnimLoop) {
			m_AnimPlayTimer += deltaTime;
			SetLooped(true);
		}
		else {
			SetLooped(false);
		}

		if (m_AnimPlayTimer >= 1.0f) {
			m_AnimLoop = false;
			m_AnimPlayTimer = 0.0f;
		}
	}

	void PlayerPointer::OnUpdate2() {
		//DrawString();
	}

	//Aボタンが押された処理
	void PlayerPointer::OnPushA() {

	}
	
	//Xボタンが押された処理
	void PlayerPointer::OnPushX() {

	}
	
	//アイコン選択の場所
	void PlayerPointer::SelectItem() {
		auto gm = GameManager::GetInstance();
		auto objvec = GetStage()->GetGameObjectVec();
		auto XAPtr = App::GetApp()->GetXAudio2Manager();
		for (auto& v : objvec) {
			auto ptrIcon = dynamic_pointer_cast<IconSprite>(v);
			auto ptrBackIcon = dynamic_pointer_cast<BackIcon>(v);
			if (ptrIcon) {
				auto colSphere = ptrIcon->GetComponent<CollisionSphere>(false);
				if (colSphere) {
					Vec3 ret;
					auto colSphere = ptrIcon->GetComponent<CollisionSphere>()->GetSphere();
					auto colObb = GetComponent<CollisionObb>()->GetObb();
					//HitTestで衝突判定
					if (HitTest::SPHERE_OBB(colSphere, colObb, ret)) {
						ptrIcon->SetIsSelect(true);
						gm->SetCurrentDim(ptrIcon->GetTag());
						m_Status = ptrIcon->GetToy();
						m_BeforStatus = m_Status;
						XAPtr->Start(WstringKey::se_Button, 0, 0.5f);
					}
				}
			}
		}
	}

	//ポインターが衝突した時の処理
	void PlayerPointer::SwitchPointer() {
		auto obbPointer = GetComponent<CollisionObb>()->GetObb();
		auto objVec = GetStage()->GetGameObjectVec();
		for (auto& v : objVec) {
			auto ptrIconBG = dynamic_pointer_cast<IconBackGround>(v);
			auto ptrBackIcon = dynamic_pointer_cast<BackIcon>(v);
			auto ptrCatVis = dynamic_pointer_cast<CatChildrenVisual>(v);
			//猫の視界との衝突判定
			if (ptrCatVis) {
				auto colCat = ptrCatVis->GetComponent<CollisionSphere>();
				if (colCat) {
					Vec3 ret;
					auto sphere = colCat->GetSphere();
					if (HitTest::SPHERE_OBB(sphere, obbPointer, ret)) {
						auto ptrCat = ptrCatVis->GetMyParent();
						m_CatSeeFlg = true;
						if (m_CatnipRapidCount >= 3 && m_MoodUpFlg && m_CatSeeTimer < 3.0f) {
							ptrCat->AddMoodGage(1);
							m_MoodUpFlg = false;
							m_CatSeeTimer = 0.0f;
						}
						else if (m_CatSeeTimer > 3.0f) {
							m_CatSeeTimer = 0.0f;
							ptrCat->AddMoodGage(-1);
						}
					}
				}
			}
			if (ptrIconBG) {
				//玩具の背景に入った時ポインターに戻す
				auto colObb = ptrIconBG->GetComponent<CollisionObb>(false);
				if (colObb) {
					auto obbIconBack = colObb->GetObb();
					if (HitTest::OBB_OBB(obbIconBack, obbPointer)) {
						//選択後、一度外に出てからポインターに戻す
						if (m_CheckColOut) {
							GetComponent<Transform>()->SetScale(Vec3(0.5f, 0.5f, 0.5f));
							m_Status = PointerStatus;
							m_CheckColOut = false;
						}
					}
					else if(!m_CheckHitOther){ //「戻るボタン」に衝突してない場合は選択した玩具に戻す
						m_Status = m_BeforStatus;
						m_CheckColOut = true;
					}
				}
			}
			if (ptrBackIcon) {
				//戻るボタンに衝突した時ポインターに戻す
				auto colBIObb = ptrBackIcon->GetComponent<CollisionObb>(false);
				if (colBIObb) {
					auto obbBI = colBIObb->GetObb();
					if (HitTest::OBB_OBB(obbBI, obbPointer)) {
						GetComponent<Transform>()->SetScale(Vec3(0.5f, 0.5f, 0.5f));
						m_Status = PointerStatus;
						m_CheckHitOther = true;
					}
					else{
						m_CheckHitOther = false;
					}
				}
			}
		}
	}

	void PlayerPointer::OnCollisionEnter(shared_ptr<GameObject>& other) {
	}
	void PlayerPointer::OnCollisionExcute(shared_ptr<GameObject>& other) {
		auto cntlVec = App::GetApp()->GetInputDevice().GetControlerVec()[0];
		auto XAPtr = App::GetApp()->GetXAudio2Manager();
		//タグがクッションの時
		if (other->FindTag(L"Tag_Cushion")) {
			auto gm = GameManager::GetInstance();
			if (m_Status == PointerStatus) {
				if (cntlVec.wButtons & XINPUT_GAMEPAD_A) {
					if (!gm->GetHaveFurniture()) {
						gm->SetHaveFurniture(other);
					}
					if (gm->CheckHaveFurniture(other)) {
						auto cushonTrans = other->GetComponent<Transform>();
						auto trans = GetComponent<Transform>();
						auto pointerPos = trans->GetPosition();
						pointerPos.z = 0.4f;
						cushonTrans->SetPosition(pointerPos);
					}
				}
				else
				{
					if (gm->GetHaveFurniture()) {
						gm->SetHaveFurniture(nullptr);
						gm->AddMissionMoveFurniture(1);
					}
				}
			}
		}
		//タグが戻るアイコンの時
		if (other->FindTag(WstringKey::tag_BackIcon)) {
			if (cntlVec.wPressedButtons & XINPUT_GAMEPAD_A) {
				auto XAPtr = App::GetApp()->GetXAudio2Manager();
				XAPtr->Start(L"se_Button", 0, 0.5f);
				//「戻るボタン」を押した時タイトルに戻る
				PostEvent(0.0f, GetThis<PlayerPointer>(), App::GetApp()->GetScene<Scene>(), L"ToTitleStage");
			}
		}
		//タグがスタートボタンの時
		if (other->FindTag(L"Tag_StartButton")) {
			if (cntlVec.wPressedButtons & XINPUT_GAMEPAD_A) {
				auto stage = dynamic_pointer_cast<TitleStage>(GetStage());
				if (stage) {
					if (!stage->GetChangeFlg()) {
						stage->SetChangeFlg(true);
						auto XAPtr = App::GetApp()->GetXAudio2Manager();
						XAPtr->Start(L"se_Button", 0, 0.5f);
					}
				}
				//PostEvent(0.0f, GetThis<PlayerPointer>(), App::GetApp()->GetScene<Scene>(), L"ToLoadStage");
			}
		}
	}
	void PlayerPointer::OnCollisionExit(shared_ptr<GameObject>& other) {
	}

	//おもちゃの切り替え
	void PlayerPointer::SwitchItem(PlayerStatus toy) {
		auto gm = GameManager::GetInstance();
		auto ptrDraw = GetComponent<PCTStaticDraw>();
		auto stage = GetStage();
		shared_ptr<PlayerPointer> ptrPointer;
		Mat4x4 mat;
		Mat4x4 hiddenMat;
		mat.affineTransformation(
			Vec3(0.25f, 0.25f, 0.25f),
			Vec3(0.0f),
			Vec3(0.0f),
			Vec3(0.0f, -0.5f, 0.0f)
		);
		hiddenMat.affineTransformation(
			Vec3(0.0f),
			Vec3(0.0f),
			Vec3(0.0f),
			Vec3(0.0f)
		);

		switch (toy)
		{
		case CatnipStatus:
			ptrDraw->SetOriginalMeshUse(false);
			SetToAnimeMatrix(mat);
			CatnipOperate();
			break;
		case MouseRCStatus:
			ptrDraw->SetTextureResource(WstringKey::tx_MouseRC);
			MouseRCOperate();
			break;
		case YarnBallStatus:
			ptrDraw->SetTextureResource(WstringKey::tx_YarnBall);
			YarnballOperate();
			break;
		case PointerStatus:
			SetToAnimeMatrix(hiddenMat);
			ptrDraw->SetOriginalMeshUse(true);
			ptrDraw->SetTextureResource(WstringKey::tx_Pointer);
			SwitchIconDim();
			PointerMove();
			break;
		default:
			break;
		}
	}

	void PlayerPointer::SwitchIconDim() {
		auto gm = GameManager::GetInstance();
		auto objvec = GetStage()->GetGameObjectVec();
		for (auto& v : objvec) {
			auto ptrIcon = dynamic_pointer_cast<IconSprite>(v);
			if (ptrIcon) {
				if (gm->GetCurrentDim() == WstringKey::tag_UICatnip) {
					ptrIcon->SetIsSelect(false);
				}
				else if (gm->GetCurrentDim() == WstringKey::tag_UIMouseRC) {
					ptrIcon->SetIsSelect(false);
				}
				else if (gm->GetCurrentDim() == WstringKey::tag_UIYarnBall) {
					ptrIcon->SetIsSelect(false);
				}
			}
		}
	}

	//画面外に出さない奴
	void PlayerPointer::NotOutScreen() {
		auto ptrTrans = GetComponent<Transform>();
		auto pos = ptrTrans->GetPosition();
		float MaxHeightScreen = 1.9f;
		float MaxWidthScreen = 3.48f;

		if (MaxWidthScreen < pos.x) {
			pos.x = MaxWidthScreen;
			ptrTrans->SetPosition(pos);
		}
		if (-MaxWidthScreen > pos.x) {
			pos.x = -MaxWidthScreen;
			ptrTrans->SetPosition(pos);
		}
		if (MaxHeightScreen < pos.y) {
			pos.y = MaxHeightScreen;
			ptrTrans->SetPosition(pos);
		}
		if (-MaxHeightScreen > pos.y) {
			pos.y = -MaxHeightScreen;
			ptrTrans->SetPosition(pos);
		}
	}

	//文字列をつける
	void PlayerPointer::DrawString() {
		wstring db = L"デバッグ\n";

		auto pos = GetComponent<Transform>()->GetPosition();
		wstring playerPos = L"Pos:";
		playerPos += L"PosX=" + Util::FloatToWStr(pos.x, 6, Util::FloatModify::Fixed) + L"\t";
		playerPos += L"PosY=" + Util::FloatToWStr(pos.y, 6, Util::FloatModify::Fixed) + L"\t";
		playerPos += L"PosZ=" + Util::FloatToWStr(pos.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring time = L"タイム\n";
		time += L"Time" + Util::FloatToWStr(m_CatnipTimer, 6, Util::FloatModify::Fixed) + L"\n";

		wstring seatime = L"猫タイム\n";
		seatime += L"Time" + Util::FloatToWStr(m_CatSeeTimer, 6, Util::FloatModify::Fixed) + L"\n";

		wstring holdtime = L"長押した時間\n";
		holdtime += L"TIme" + Util::FloatToWStr(m_ButtonHoldTimer, 6, Util::FloatModify::Fixed) + L"\n";

		wstring landingpoint = L"着地地点\n";
		landingpoint += L"landing" + Util::FloatToWStr(m_LandingPoint, 6, Util::FloatModify::Fixed) + L"\n";

		wstring count = L"連打カウント\n";
		count += Util::UintToWStr(m_CatnipRapidCount) + L"\n";

		wstring cushion = L"クッション\n";
		cushion += debug;

		//auto1 cat1mood = GetStage()->GetSharedGameObject<Cat>(L"Cat1")->GetMoodGage();
		//auto cat2mood = GetStage()->GetSharedGameObject<Cat>(L"Cat2")->GetMoodGage();
		//wstring mood = L"猫の気分\n";
		//mood += Util::UintToWStr(cat1mood) + L"\n";
		//mood += Util::UintToWStr(cat2mood) + L"\n";

		auto fps = App::GetApp()->GetStepTimer().GetFramesPerSecond();
		wstring fpsStr(L"FPS: ");
		fpsStr += Util::UintToWStr(fps);
		fpsStr += L"\nElapsedTime: ";
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		fpsStr += Util::FloatToWStr(ElapsedTime);
		fpsStr += L"\n";

		wstring st = db + debug + L"\n" + count + time /*+ mood*/ + playerPos + seatime + fpsStr;

		auto PtrString = GetComponent<StringSprite>();
		PtrString->SetText(st);
	}


	//------------------------------------------------------------------------------
	//毛糸玉の設置
	//------------------------------------------------------------------------------
	YarnBallInstallation::YarnBallInstallation(const shared_ptr<Stage>& StagePtr,
		const Vec3& Scale,
		const Vec3& Position,
		const float& EndPoint
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Position(Position),
		m_EndPoint(EndPoint),
		m_Velocity(0.0f),
		m_Force(0.0f),
		m_Speed(3.0f),
		m_TotalTime(0.0f),
		m_ElapsedTime(0.0f),
		m_BallLandingFlg(false),
		db(L"false")
	{
	}

	YarnBallInstallation::~YarnBallInstallation() {};

	//毛糸玉を投げる
	void YarnBallInstallation::YarnballThrow() {
		auto ptrTrans = GetComponent<Transform>();
		auto deltaTime = App::GetApp()->GetElapsedTime();
		auto pos = ptrTrans->GetPosition();
		auto XAPtr = App::GetApp()->GetXAudio2Manager();

		m_TotalTime += App::GetApp()->GetElapsedTime();
		m_MidPoint = m_EndPoint - m_StartPoint;
		if (m_TotalTime <= 1.0f) {
			pos.y = (1 - m_TotalTime) * (1 - m_TotalTime) * m_StartPoint
					+ 2 * (1 - m_TotalTime) * m_TotalTime * m_MidPoint
					+ m_TotalTime * m_TotalTime * m_EndPoint;
			auto scale = ptrTrans->GetScale();
			scale -= Vec3(0.01f, 0.01f, 0.01f);
			ptrTrans->SetScale(scale);
		}
		else
		{
			m_BallLandingFlg = true;
		}

		//ベジェ曲線
		//t = 経過時間
		//(1 - t) * (1 - t) * 始点
		//+ 2 * (1 - t) * t * 中点
		//+ t * t * 終点

		ptrTrans->SetPosition(pos);
	}

	//初期化
	void YarnBallInstallation::OnCreate() {
		auto ptrTrans = GetComponent<Transform>();
		ptrTrans->SetScale(m_Scale);
		ptrTrans->SetPosition(m_Position);
		m_StartPoint = ptrTrans->GetPosition().y;

		//CollisionObb衝突判定
		auto ptrCol = AddComponent<CollisionObb>();
		ptrCol->SetAfterCollision(AfterCollision::None);
		//ptrCol->SetDrawActive(true);

		//スプライト作成
		Col4 White(1.0f, 1.0f, 1.0f, 1.0f);
		float halfsize = 0.5f;
		vector<VertexPositionColorTexture> vertex =
		{
			{Vec3(-halfsize, +halfsize, 0), White, Vec2(0.0f, 0.0f)},
			{Vec3(+halfsize, +halfsize, 0), White, Vec2(1.0f, 0.0f)},
			{Vec3(-halfsize, -halfsize, 0), White, Vec2(0.0f, 1.0f)},
			{Vec3(+halfsize, -halfsize, 0), White, Vec2(1.0f, 1.0f)}
		};

		vector<uint16_t> indices =
		{
			0, 1, 2,
			1, 3, 2
		};

		//スプライト作成
		auto ptrStaticDraw = AddComponent<PCTStaticDraw>();
		ptrStaticDraw->CreateOriginalMesh(vertex, indices);
		ptrStaticDraw->SetOriginalMeshUse(true);
		ptrStaticDraw->SetTextureResource(WstringKey::tx_YarnBall);
		ptrStaticDraw->SetDepthStencilState(DepthStencilState::None);
		//透明処理
		SetAlphaActive(true);
		//レイヤーを10番目に設定
		SetDrawLayer(9);
		//タグ
		AddTag(WstringKey::tag_YarnBall);
		//文字列を追加
		//auto ptrString = AddComponent<StringSprite>();
		//ptrString->SetText(L"");
		//ptrString->SetBackColor(Col4(0.0f, 0.0f, 0.0f, 0.5f));
		//ptrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));
	}

	//更新
	void YarnBallInstallation::OnUpdate() {
		float deltaTime = App::GetApp()->GetElapsedTime();
		YarnballThrow();
		if (m_BallLandingFlg) {
			m_ElapsedTime += deltaTime;
		}

		if (m_ElapsedTime >= 3.0f) {
			SetDrawActive(false);
			SetUpdateActive(false);
		}
	}

	void YarnBallInstallation::OnUpdate2() {
		//DrawString();
	}

	void YarnBallInstallation::DrawString() {

		wstring st = L"デバッグ\n" + db;

		auto PtrString = GetComponent<StringSprite>();
		PtrString->SetText(st);
	}

	//------------------------------------------------------------------------------
	//ネズミラジコンの設置
	//------------------------------------------------------------------------------
	MouseRCInstallation::MouseRCInstallation(const shared_ptr<Stage>& StagePtr,
		const Vec3& Scale,
		const Vec3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Position(Position),
		m_Force(0),
		m_Velocity(0),
		m_Speed(3.0f),
		m_razinkonSECheck(false),
		m_OffScreen(false)
	{
	}

	MouseRCInstallation::~MouseRCInstallation() {};

	//慣性
	void MouseRCInstallation::ApplyForce() {
		float elapsedTime = App::GetApp()->GetElapsedTime();
		m_Velocity += m_Force * elapsedTime;
		auto ptrTrans = GetComponent<Transform>();
		auto pos = ptrTrans->GetPosition();
		pos += m_Velocity * elapsedTime;
		ptrTrans->SetPosition(pos);
	}

	//ネズミラジコンの移動
	void MouseRCInstallation::MoveMouseRC() {
		auto ptrTrans = GetComponent<Transform>();
		auto pos = ptrTrans->GetPosition();
		pos.x += -1.0f * m_Speed;

		auto ptrArrive = GetBehavior<ArriveSteering>();
		auto force = m_Force;
		force = ptrArrive->Execute(force, m_Velocity, pos);

		m_Force = force;
		ApplyForce();
	}

	//ネズミを画面内に設置
	void MouseRCInstallation::Installation() {
		m_Velocity = Vec3(0.0f);
		SetDrawActive(true);
		SetUpdateActive(true);
		m_OffScreen = false;
		m_razinkonSECheck = true;
	}

	//初期化
	void MouseRCInstallation::OnCreate() {
		auto ptrTrans = GetComponent<Transform>();
		ptrTrans->SetScale(m_Scale);
		ptrTrans->SetPosition(m_Position);

		//CollisionObb衝突判定
		auto ptrCol = AddComponent<CollisionObb>();
		ptrCol->SetAfterCollision(AfterCollision::None);
		//ptrCol->SetDrawActive(true);

		//スプライト作成
		Col4 White(1.0f, 1.0f, 1.0f, 1.0f);
		float halfsize = 0.5f;
		vector<VertexPositionColorTexture> vertex =
		{
			{Vec3(-halfsize, +halfsize, 0), White, Vec2(0.0f, 0.0f)},
			{Vec3(+halfsize, +halfsize, 0), White, Vec2(1.0f, 0.0f)},
			{Vec3(-halfsize, -halfsize, 0), White, Vec2(0.0f, 1.0f)},
			{Vec3(+halfsize, -halfsize, 0), White, Vec2(1.0f, 1.0f)}
		};

		vector<uint16_t> indices =
		{
			0, 1, 2,
			1, 3, 2
		};

		//スプライト作成
		auto ptrStaticDraw = AddComponent<PCTStaticDraw>();
		ptrStaticDraw->CreateOriginalMesh(vertex, indices);
		ptrStaticDraw->SetOriginalMeshUse(true);
		ptrStaticDraw->SetTextureResource(WstringKey::tx_MouseRC);
		ptrStaticDraw->SetDepthStencilState(DepthStencilState::None);
		//透明処理
		SetAlphaActive(true);
		//レイヤーを10番目に設定
		SetDrawLayer(9);
		//タグ
		AddTag(WstringKey::tag_MouseRC);
		//SEの再生フラグ
		m_razinkonSECheck = false;
	}
	
	//更新
	void MouseRCInstallation::OnUpdate() {
		auto XAPtr = App::GetApp()->GetXAudio2Manager();
		auto ptrTrans = GetComponent<Transform>();
		float offScreen = -3.9f;

		m_Force = Vec3(0);

		MoveMouseRC();

		auto pos = ptrTrans->GetPosition();

		//画面外に出たらネズミの非表示・SEの停止
		if (pos.x < offScreen) {
			SetDrawActive(false);
			SetUpdateActive(false);
			m_OffScreen = true;
			XAPtr->Stop(m_AudioItem);
		}

		//SEの再生
		if (m_razinkonSECheck) {
			m_AudioItem = XAPtr->Start(WstringKey::se_Razikon, 0, 0.5f);
			m_razinkonSECheck = false;
		}
	}

	//------------------------------------------------------------------------------
	//おもちゃアイコン
	//------------------------------------------------------------------------------
	IconSprite::IconSprite(const shared_ptr<Stage>& StagePtr,
		const Vec3& Scale,
		const Vec3& Position,
		const wstring TX,
		const PlayerStatus status,
		const wstring tag
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Position(Position),
		m_IsSelect(false),
		m_TX(TX),
		m_Toy(status),
		m_Tag(tag)
	{
	}

	IconSprite::~IconSprite() {}

	void IconSprite::DimIcon() {
		if (m_IsSelect) {
			GetComponent<PCTStaticDraw>()->SetDiffuse(Col4(0.5f, 0.5f, 0.5f, 1.0f));
		}
		else {
			GetComponent<PCTStaticDraw>()->SetDiffuse(Col4(1.0f, 1.0f, 1.0f, 1.0f));
		}
	}

	//初期化
	void IconSprite::OnCreate() {
		auto ptrTrans = GetComponent<Transform>();
		ptrTrans->SetScale(m_Scale);
		ptrTrans->SetPosition(m_Position);

		//衝突判定
		float ColSize = 0.01f;
		auto ptrCol = AddComponent<CollisionSphere>();
		ptrCol->SetMakedDiameter(ColSize);
		ptrCol->SetAfterCollision(AfterCollision::None);
		ptrCol->SetDrawActive(true);

		//テクスチャ設定
		float halfsize = 0.5f;
		Col4 White(1.0f, 1.0f, 1.0f, 1.0f);
		vector<VertexPositionColorTexture> vertex =
		{
			{Vec3(-halfsize, +halfsize, 0), White, Vec2(0,0)},
			{Vec3(+halfsize, +halfsize, 0), White, Vec2(1,0)},
			{Vec3(-halfsize, -halfsize, 0), White, Vec2(0,1)},
			{Vec3(+halfsize, -halfsize, 0), White, Vec2(1,1)}
		};
		vector<uint16_t> indices =
		{
			0, 1, 2,
			1, 3, 2
		};

		//スプライト作成
		auto ptrStaticDraw = AddComponent<PCTStaticDraw>();
		ptrStaticDraw->CreateOriginalMesh(vertex, indices);
		ptrStaticDraw->SetOriginalMeshUse(true);
		ptrStaticDraw->SetTextureResource(m_TX);
		ptrStaticDraw->SetDepthStencilState(DepthStencilState::None);
		SetAlphaActive(true);
		//レイヤーの設定
		SetDrawLayer(9);
	}

	//更新
	void IconSprite::OnUpdate() {
		DimIcon();
	}

	//------------------------------------------------------------------------------
	//戻るアイコン
	//------------------------------------------------------------------------------
	BackIcon::BackIcon(const shared_ptr<Stage>& StagePtr,
		const Vec3& Scale,
		const Vec3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Position(Position)
	{
	}

	BackIcon::~BackIcon() {};

	//初期化
	void BackIcon::OnCreate() {
		auto ptrTrans = GetComponent<Transform>();
		ptrTrans->SetScale(m_Scale);
		ptrTrans->SetPosition(m_Position);

		//衝突判定
		auto ptrCol = AddComponent<CollisionObb>();
		ptrCol->SetAfterCollision(AfterCollision::None);
		//ptrCol->SetDrawActive(true);

		//テクスチャの設定
		float halfsize = 0.5f;
		Col4 White(1.0f, 1.0f, 1.0f, 1.0f);
		vector<VertexPositionColorTexture> vertex =
		{
			{Vec3(-halfsize, +halfsize, 0), White, Vec2(0,0)},
			{Vec3(+halfsize, +halfsize, 0), White, Vec2(1,0)},
			{Vec3(-halfsize, -halfsize, 0), White, Vec2(0,1)},
			{Vec3(+halfsize, -halfsize, 0), White, Vec2(1,1)}
		};
		vector<uint16_t> indices =
		{
			0, 1, 2,
			1, 3, 2
		};

		//スプライト作成
		auto ptrStaticDraw = AddComponent<PCTStaticDraw>();
		ptrStaticDraw->CreateOriginalMesh(vertex, indices);
		ptrStaticDraw->SetOriginalMeshUse(true);
		ptrStaticDraw->SetTextureResource(WstringKey::tx_BackButton);
		ptrStaticDraw->SetDepthStencilState(DepthStencilState::None);
		SetAlphaActive(true);
		//レイヤー設定
		SetDrawLayer(9);
	}

	//更新
	void BackIcon::OnUpdate() {

	}

	//------------------------------------------------------------------------------
	//おもちゃアイコンの背景
	//------------------------------------------------------------------------------
	IconBackGround::IconBackGround(const shared_ptr<Stage>& StagePtr,
		const Vec3& Scale,
		const Vec3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Position(Position)
	{
	}

	IconBackGround::~IconBackGround() {};

	//初期化
	void IconBackGround::OnCreate() {
		auto ptrTrans = GetComponent<Transform>();
		ptrTrans->SetScale(m_Scale);
		ptrTrans->SetPosition(m_Position);

		//衝突判定
		auto ptrCol = AddComponent<CollisionObb>();
		//ptrCol->SetDrawActive(true);
		ptrCol->SetAfterCollision(AfterCollision::None);

		//テクスチャ設定
		float halfsize = 0.5f;
		Col4 White(1.0f, 1.0f, 1.0f, 1.0f);
		vector<VertexPositionColorTexture> vertex =
		{
			{Vec3(-halfsize, +halfsize, 0), White, Vec2(0,0)},
			{Vec3(+halfsize, +halfsize, 0), White, Vec2(1,0)},
			{Vec3(-halfsize, -halfsize, 0), White, Vec2(0,1)},
			{Vec3(+halfsize, -halfsize, 0), White, Vec2(1,1)}
		};

		vector<uint16_t> indices =
		{
			0, 1, 2,
			1, 3, 2
		};

		//スプライトの作成
		auto ptrStaticDraw = AddComponent<PCTStaticDraw>();
		ptrStaticDraw->CreateOriginalMesh(vertex, indices);
		ptrStaticDraw->SetOriginalMeshUse(true);
		Mat4x4 mat;
		mat.affineTransformation(
			Vec3(1.25,1.25,1.25),
			Vec3(0),
			Quat(),
			Vec3(-0.01,0.35,0)
		);
		ptrStaticDraw->SetTextureResource(WstringKey::tx_IconBackGround);
		ptrStaticDraw->SetMeshToTransformMatrix(mat);
		ptrStaticDraw->SetDepthStencilState(DepthStencilState::None);
		SetAlphaActive(true);
		//レイヤーの設定
		SetDrawLayer(8);
		AddTag(L"IconBackGround");
	}

	//更新
	void IconBackGround::OnUpdate() {
		
	}

	// 構築と破棄
	check::check(
		const shared_ptr<Stage>& StagePtr,
		const wstring& BaseDir,
		const Vec3& Scale,
		const Vec3& Pos)
		:
		SS5ssae(StagePtr, BaseDir, L"RussianBule (1).ssae", L"nekojarasi"),
		m_Scale(Scale),
		m_Position(Pos)
	{
	}

	// 初期化
	void check::OnCreate()
	{
		// 大きさ、回転、位置
		auto ptrTransform = GetComponent<Transform>();
		ptrTransform->SetScale(m_Scale);
		ptrTransform->SetPosition(m_Position);
		auto col = AddComponent<CollisionObb>();
		col->SetAfterCollision(AfterCollision::None);
		col->SetDrawActive(true);

		Mat4x4 spanMat; // モデルとトランスフォームの間の差分行列
		spanMat.affineTransformation(
			Vec3(0.25f, 0.25f, 0.25f),
			Vec3(0.0f),
			Vec3(0.0f),
			Vec3(0.0f, -0.5f, 0.0f)
		);
		// 親クラスの初期化呼び出し
		SS5ssae::OnCreate();
		SetToAnimeMatrix(spanMat);
		//値は秒あたりのフレーム数
		SetFps(30.0f);
		// 透明処理
		SetAlphaActive(true);
		// レイヤー
		SetDrawLayer(8);
	}

	void check::OnUpdate()
	{
		// アニメーションの更新
		float elapsedTime = App::GetApp()->GetElapsedTime();
		UpdateAnimeTime(elapsedTime);
	}
}
//end basecross