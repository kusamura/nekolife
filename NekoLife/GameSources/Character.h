/*!
@file Character.h
@brief キャラクターなど
*/

#pragma once
#include "stdafx.h"

namespace basecross{

	// 仮ボックスオブジェクト
	class FixedBox : public GameObject
	{
		// 大きさ、回転、位置
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;

	public:
		// 構築と破棄
		FixedBox(
			const shared_ptr<Stage>& StagePtr,
			const Vec3& Scale,
			const Vec3& Rotation, 
			const Vec3& Position
		);
		~FixedBox();

		// 初期化
		virtual void OnCreate() override;
	};
	// 物理計算するオブジェクトの親クラス
	class ActivePsObject : public GameObject {
		bool m_Selected;
	protected:
		ActivePsObject(const shared_ptr<Stage>& StagePtr) :
			GameObject(StagePtr), m_Selected(false) {}
		virtual ~ActivePsObject() {};
	public:
		void SetSelect(bool b) {
			m_Selected = b;
			auto PtrDraw = AddComponent<BcPNTStaticDraw>();
			if (b) {
				PtrDraw->SetEmissive(Col4(1.0f, 1.0f, 0, 0));
			}
			else {
				PtrDraw->SetEmissive(Col4(0.0f, 0.0f, 0, 0));
			}
		}

		bool IsSelect() {
			return m_Selected;
		}
		// 更新
		virtual void OnUpdate() override;
	};

	// 猫じゃらし
	class Catnip : public ActivePsObject {
		Vec3 m_Scale;
		Quat m_Qt;
		Vec3 m_Position;
	public:
		Catnip(const shared_ptr<Stage>& StagePtr, const Vec3& Scale, const Quat& Qt, const Vec3& Position);
		virtual ~Catnip();
		// 初期化
		virtual void OnCreate() override;
	};

	// 物理計算するアクティブなBox
	class ActivePsBox : public ActivePsObject {
		Vec3 m_Scale;
		Quat m_Qt;
		Vec3 m_Position;
	public:
		ActivePsBox(const shared_ptr<Stage>& StagePtr, const Vec3& Scale, const Quat& Qt, const Vec3& Position);
		virtual ~ActivePsBox();

		virtual void OnCreate() override;
	};


	// ご飯(皿を子オブジェクトに)
	class CatDining : public GameObject {
		Vec3 m_Scale;
		Quat m_Qt;
		Vec3 m_Position;
	public:
		CatDining(const shared_ptr<Stage>& StagePtr, const Vec3& Scale, const Quat& Qt, const Vec3& Position);
		virtual ~CatDining();

		virtual void OnCreate() override;
		virtual void OnUpdate()override;
	};

	// 皿
	class Dish : public GameObject {
		// 大きさ、回転、位置
		Vec3 m_Scale;
		Quat m_Qt;
		Vec3 m_Position;

		// 親オブジェクト
		shared_ptr<GameObject> m_Parent = nullptr;
	public :
		// 構築と破棄
		Dish(
			const shared_ptr<Stage>& StagePtr, 
			const Vec3& Scale, 
			const Quat& Qt,
			const Vec3& Position,
			const shared_ptr<GameObject>& Parent = nullptr);
		virtual ~Dish();

		// 初期化
		virtual void OnCreate() override;

	};

	// カーペット
	class Carpet : public GameObject
	{
		// 大きさ、回転、位置
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;

	public:
		// 構築と破棄
		Carpet(
			const shared_ptr<Stage>& StagePtr, 
			const Vec3& Scale, 
			const Vec3& Rotation,
			const Vec3& Position);
		virtual ~Carpet();

		// 初期化
		virtual void OnCreate() override;
	};

	// クッション
	class Cushion : public GameObject
	{
		// 大きさ、回転、位置
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;

		// テクスチャ
		wstring m_TX;

	public:
		// 構築と破棄
		Cushion(
			const shared_ptr<Stage>& StagePtr,
			const Vec3& Scale,
			const Vec3& Rotation,
			const Vec3& Position,
			const wstring& Tx);
		virtual ~Cushion();

		// 初期化
		virtual void OnCreate()override;

	};

}
//end basecross
