/*!
@file Cat.h
@brief ねこ関係
*/

#pragma once
#include "stdafx.h"

namespace basecross {
	// ねこクラス
	class Cat : public SS5ssae{
		// m_とかばらばらなのあとで直しますすいません
		// ステートマシン
		unique_ptr<StateMachine<Cat>> m_StateMachine;
		// 最初のステート
		shared_ptr<ObjState<Cat>> m_FirstState;
		// ステートが一回切り替わったらそのフレームはもう変えないフラグ
		bool m_ChangeStateFlg = false;

		wstring m_currentAnim;

		// 大きさ、回転、位置
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;

		// 移動速度
		float m_Speed = 1.0f;

		// パーティクルの位置
		const Vec3 m_ParticlePos = Vec3(0.0f, 0.5f, 0.0f);

		// CollisionSphereの大きさ
		float m_CollDiameter = 0.7f;

		// 子オブジェクトの大きさ(親との比較した倍率)
		float m_childVisualMag = 0.8f;
		float m_exclamationMag = 0.35f;
		//子オブジェクト
		shared_ptr<GameObject> m_ChildVisual;
		shared_ptr<GameObject> m_ExclamationMark;

		// 入る位置
		const Vec3 m_EnterPos = Vec3(-2.0f, 1.0f, 0.5f);
		// 出る位置
		const Vec3 m_ExitPos = Vec3(-10.0f, 1.0f, 0.5f);
		// 帰る時間
		float m_ExitTime = 15.0f;
		// 出入り中かフラグ
		bool m_EnterExitFlg = false;

		Mat4x4 m_spanMat; // モデルとトランスフォームの間の差分行列
		Mat4x4 m_smokeSpanMat; // モデルとトランスフォームの間の差分行列(喧嘩の煙用)
		// アニメーション更新用
		float m_animTime;
		// SEループ用
		float m_seTotalTime = 10.0f;
		wstring m_SEKey = L"";
		shared_ptr<SoundItem> m_SE = nullptr;

		// 向き
		float m_Dir = 1.0f;

		// 気分ゲージ
		int m_MoodGage = 3;
		// 気分値マックス状態
		const int MaxMoodGage = 5;
		// ゲージ計算するかしないかフラグ汚い書き方します。後でまとめて直します申し訳

		// 空腹ゲージ
		int m_HungerGage = 0;
		// 空腹マックス状態
		const int MaxHungerGage = 5;

		// ここから仮-----------------------------------------------
		// ゲージアップ時間
		float gageUpTime = 1.0f;
		// ゲージダウン時間
		float gageDownTime = 3.0f;
		// ゲージ時間
		float gageTime = 0.0f;
		// ここまで-------------------------------------------------

		// 放置時間
		float standingTotalTime = 0.0f;
		float standingTime = 10.0f;
		// 放置中フラグ
		bool m_standingFlg = true;

		// 猫見たフラグ
		bool m_findCatFlg = false;
		// 猫vs猫フラグ
		bool m_catfightFlg = false;
		// 片方の猫を描画したい
		bool m_catDrawFlg = false;
		// 猫vs猫時間
		float m_CatfightTime = 5.0f;
		float m_CatfightTotalTime = 0.0f;

		// 気まぐれ時間
		float whimTime = 6.0f;
		float whimTotalTime = 0.0f;
		// 気まぐれフラグ
		bool m_WhimFlg = false;

		// ランダムで位置を決める（仮）
		Vec3 randomPos;
		bool randomFlg = false;
		float coolTime = 2.0f;// 待機時間
		float totalTime = 0.0f;

		// 待機時間(モーションを入れるときにランダムで切り替わるようにしようかと)
		float waitTime = 3.0f;
		float totalWaitTime = 0.0f;
		int randomState = 0;

		// 移動終了フラグ
		bool moveEndFlg = false;

		// ご飯食べたかフラグ
		bool m_finishedEatFlg = false;
		// ご飯中に周り気にするかフラグ(true:気にする,false:気にしない)
		bool m_eatingCareFlg = true;
		// 今気にしてるフラグ
		bool m_careFlg = false;
		// ご飯中に気にする時間
		float careTime = 5.0f;
		float careTotalTime = 0.0f;
		// ボーナスフラグ
		bool m_BonusFlg = false;

		// ご飯の近くまで来たかフラグ
		bool m_nearMealFlg = false;
		// ご飯のちょっと上の位置
		Vec3 nearMealPosDis = Vec3(-0.25f, 0.2f, 0.5f);

		//ご飯中に見つめる時間
		bool lookFlg = false;
		float lookTime = 3.0f;
		float lookTotalTime = 3.0f;
		// 飯食う時間
		float eatTime = 10.0f;
		float eatTotalTime = 0.0f;


		// 当たっているオブジェクトを入れておく
		//shared_ptr<GameObject> m_CollisionObj = nullptr;// 仮
		shared_ptr<GameObject> m_CollisionObjs[5];

		// 離れる距離
		Vec3 m_LeaveDis = Vec3(5.0f, 3.0f, 0.5f);
		// 見つめる距離
		Vec3 m_StareDis = Vec3(3.0f, 1.5f, 0.5f);

		// 当たったオブジェクトとの反応(更新する用)
		void CollisionUpdate();

		// 文字列表示
		void DrawStrings();

	public:
		// 猫のパラメータ設定(引数にいろいろ入れられるように)
		void InitParameters();
		enum CatType { Normal, RussianBlue, Brown };	// ←猫の種類をenumで決めたいがAddの時どうしようかな
		// void InitParametersType(CatType type);

		// 構築と破棄
		Cat(
			const shared_ptr<Stage>& StagePtr,
			const wstring& BaseDir,
			const wstring& XmlfileName,
			const wstring& StartAnimeName,
			const Vec3& Scale,
			const Vec3& Rotation,
			const Vec3& Position,
			const shared_ptr<ObjState<Cat>>& FirstState,
			const CatType& Type = CatType::Normal
		);
		virtual ~Cat();

		// 初期化
		virtual void OnCreate() override;

		// 更新
		virtual void OnUpdate() override;
		virtual void OnUpdate2()override;

		// 入退室
		// Move(Enter：入る、Exit：出る)
		enum Move { Enter, Exit };
		void EnterExitMove(Move move);


		// 向き
		//enum Dir{Left = -1,Right = 1};
		const float Left = -1.0f;
		const float Right = +1.0f;


		// 猫vs猫時間計算
		bool CalcCatfightTime();

		// クールタイム計算
		void CalcCoolTime();

		// ランダム計算
		enum Random {Pos, State};
		void CalcRandom(Random calc);

		// 放置時間測る用
		void CalcStandingTime();

		// 時間測る用 (同じ処理なのでとりあえずまとめた、時間計算系まとめたい)
		enum Calc {Whimsical, Care};
		bool CalcTime(Calc calc);

		// おもちゃと猫の距離
		Vec3 ToyCatDistance();

		// 後で書き直す予定
		// ご飯食べるステートの中身
		void StateEating();

		// SE流す(setTimer:true(10秒ごとにループ), false(1回のみ))
		void StartSE(wstring seKey, bool setTimer = false);
		// 今鳴ってるSE止める
		void StopSE();

		// ゲッターセッター
		// SE
		void SetSEKey(wstring setSE);
		wstring GetSEKey()const;
		shared_ptr<SoundItem> GetSE()const;
		// 前のアニメーション
		void SetCurrentAnimation(wstring setAnim);
		wstring GetCurrentAnimation()const;
		// モデルとトランスフォームの間の差分行列
		Mat4x4 GetSpanMatrix()const;
		// モデルとトランスフォームの間の差分行列（煙）
		Mat4x4 GetSmokeSpanMatrix()const;
		// 猫のタイプ
		CatType GetCatType()const;
		void SetCatType(CatType Type);
		// 猫のタイプ別のアニメーション取得
		wstring GetCatTypeAnim(CatType Type);
		// 向き
		//void SetDir(Dir setDir);
		//Dir GetDir()const;
		void SetDir(float setDir);
		float GetDir()const;
		// 移動速度
		void SetSpeed(float setSpeed);
		float GetSpeed()const;
		// 気分ゲージ
		void SetMoodGage(int setGage);
		void AddMoodGage(int addGage);// 気分ゲージ計算
		int GetMoodGage()const;
		int GetMaxMoodGage()const;// 気分マックス状態
		// 空腹ゲージ
		void SetHungerGage(int setGage);
		void AddHungerGage(int addGage);// 空腹ゲージ計算
		int GetHungerGage()const;
		int GetMaxHungerGage()const;// 空腹ゲージマックス状態
		// 放置時間
		float GetStandingTime()const;
		// 猫vs猫時間初期化
		void InitCatfightTime();
		// フラグ
		// ステートが一回切り替わったらそのフレームはもう変えないフラグ
		void SetChangeStateFlg(bool setFlg);
		bool GetChangeStateFlg()const;
		// 出入り中フラグ
		void SetEnterExitFlg(bool setFlg);
		bool GetEnterExitFlg()const;
		// 放置中フラグ
		void SetStandingFlg(bool setFlg);
		bool GetStandingFlg()const;
		// 猫見たフラグ
		void SetFindCatFlg(bool setFlg);
		bool GetFindCatFlg()const;
		// 猫vs猫フラグ
		void SetCatfightFlg(bool setFlg);
		bool GetCatfightFlg()const;
		// 片方の猫描画フラグ
		void SetCatDrawFlg(bool setFlg);
		bool GetCatDrawFlg()const;
		// ゲージ計算するかしないかフラグ
		enum calcGageFlg{None, Add, Sub};
		void SetCalcGageFlg(calcGageFlg setFlg);
		calcGageFlg GetCalcGageFlg();
		// 気まぐれフラグ
		void SetWhimsicalFlg(bool setFlg);
		bool GetWhimsicalFlg();
		void InitWhimsicalFlg();// 初期化
		// ランダムフラグ
		void SetRandomFlg(bool setFlg);
		bool GetRandomFlg()const;
		// 移動終了フラグ
		void SetMoveEndFlg(bool setFlg);
		bool GetMoveEndFlg()const;
		// ご飯食べたかフラグ
		void SetFinishedEatFlg(bool setFlg);
		bool GetFinishedEatFlg()const;
		// ご飯中に周り気にするかフラグ
		void SetEatingCareFlg(bool setFlg);
		bool GetEatingCareFlg()const;
		// ご飯の近くまで来たかフラグ
		void SetNearMealFlg(bool setFlg);
		bool GetNearMealFlg()const;
		// ボーナスフラグ
		void SetBonusFlg(bool setFlg);
		bool GetBonusFlg()const;
		// ランダム位置
		Vec3 GetRandomPos()const;
		// ランダム位置を強制的に変える
		void SetRandomPos(Vec3 pos);
		void SetRandomPos(float x, float y, float z);
		// ランダムのモーション
		int GetRandomState()const;
		// ステートマシン
		const unique_ptr<StateMachine<Cat>>& GetStateMachine();

		// 子オブジェクトの取得
		// 視界
		shared_ptr<GameObject>& GetChildVisual();
		// ビックリマーク
		shared_ptr<GameObject>& GetChildExclamationMark();
		// ビックリマーク描画設定
		void SetChildExclamationMarkActive(bool setFlg);

		// 当たっているオブジェクト(仮)
		//void SetCollisionObject(shared_ptr<GameObject> collisionObj);
		//shared_ptr<GameObject>& GetCollisionObject();
		// 複数当たっているオブジェクト
		void AddCollisionObject(shared_ptr<GameObject>& addObj);// オブジェクトの追加
		void RemoveCollisionObject(shared_ptr<GameObject>& removeObj);// オブジェクトの削除
		void ResetCollisionObject();// 当たっているオブジェクトの初期化
		bool CheckCollisionObject(shared_ptr<GameObject>& checkObj);// オブジェクトが入っているか確認
		shared_ptr<GameObject>& GetCollisionObjects();	// 当たっているオブジェクトをポインタで持ってくる
		shared_ptr<GameObject> GetCollisionObjectTag(wstring tag);	// タグでオブジェクトを探す
		template <typename T>
		shared_ptr<GameObject> GetCollisionObject();	// クラス名で探す

		// 離れる距離
		Vec3 GetLeaveDistance() const;
		// 見つめる距離
		Vec3 GetStareDistance() const;

		// debug用
		// ランダムステート(あとで消す)
		int ranState;
		Vec3 flatDiseat;
		// 文字列
		wstring upps = L"";
		wstring myState = L"";

	private:
		calcGageFlg m_CalcGageFlg = calcGageFlg::None;
		// 猫のタイプ
		CatType m_Type;
	};

	// 猫ステートクラス
	// Wait(待機)
	class CatWaitState : public ObjState<Cat>
	{
		CatWaitState(){}
	public:
		static shared_ptr<CatWaitState> Instance();
		virtual void Enter(const shared_ptr<Cat>& Obj)override;
		virtual void Execute(const shared_ptr<Cat>& Obj)override;
		virtual void Exit(const shared_ptr<Cat>& Obj)override;

	};

	// Move(移動)
	class CatMoveState : public ObjState<Cat>
	{
		CatMoveState(){}
	public:
		static shared_ptr<CatMoveState> Instance();
		virtual void Enter(const shared_ptr<Cat>& Obj)override;
		virtual void Execute(const shared_ptr<Cat>& Obj)override;
		virtual void Exit(const shared_ptr<Cat>& Obj)override;
	};

	// Find(見つける)
	class CatFindState : public ObjState<Cat>
	{
		CatFindState() {}
	public:
		static shared_ptr<CatFindState> Instance();
		virtual void Enter(const shared_ptr<Cat>& Obj)override;
		virtual void Execute(const shared_ptr<Cat>& Obj)override;
		virtual void Exit(const shared_ptr<Cat>& Obj)override;
	};

	// Approach(ついてくる)
	class CatApproachState : public ObjState<Cat>
	{
		CatApproachState() {}
	public:
		static shared_ptr<CatApproachState> Instance();
		virtual void Enter(const shared_ptr<Cat>& Obj)override;
		virtual void Execute(const shared_ptr<Cat>& Obj)override;
		virtual void Exit(const shared_ptr<Cat>& Obj)override;
	};

	// Escape(逃げる)
	class CatEscapeState : public ObjState<Cat>
	{
		CatEscapeState() {}
	public:
		static shared_ptr<CatEscapeState> Instance();
		virtual void Enter(const shared_ptr<Cat>& Obj)override;
		virtual void Execute(const shared_ptr<Cat>& Obj)override;
		virtual void Exit(const shared_ptr<Cat>& Obj)override;
	};

	// CanPlay(じゃれる)
	class CatCanPlayState : public ObjState<Cat>
	{
		CatCanPlayState(){}

	public :
		static shared_ptr<CatCanPlayState> Instance();
		virtual void Enter(const shared_ptr<Cat>& Obj)override;
		virtual void Execute(const shared_ptr<Cat>& Obj)override;
		virtual void Exit(const shared_ptr<Cat>& Obj)override;
	};

	// Eating(ご飯食べる)
	class CatEatingState : public ObjState<Cat>
	{
		CatEatingState(){}
	public :
		static shared_ptr<CatEatingState> Instance();
		virtual void Enter(const shared_ptr<Cat>& Obj)override;
		virtual void Execute(const shared_ptr<Cat>& Obj)override;
		virtual void Exit(const shared_ptr<Cat>& Obj)override;
	};

	// Enter(入る)
	class CatEnterState : public ObjState<Cat>
	{
		CatEnterState(){}
	public :
		static shared_ptr<CatEnterState> Instance();
		virtual void Enter(const shared_ptr<Cat>& Obj)override;
		virtual void Execute(const shared_ptr<Cat>& Obj)override;
		virtual void Exit(const shared_ptr<Cat>& Obj)override;
	};

	// Exit(出ていく)
	class CatExitState : public ObjState<Cat>
	{
		CatExitState(){}
	public : 
		static shared_ptr<CatExitState> Instance();
		virtual void Enter(const shared_ptr<Cat>& Obj)override;
		virtual void Execute(const shared_ptr<Cat>& Obj)override;
		virtual void Exit(const shared_ptr<Cat>& Obj)override;
	};

	// 猫の視界(子オブジェクト)
	class CatChildrenVisual : public GameObject
	{
		// 大きさ、回転、位置
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;

		// 親との距離
		const Vec3 ParentDistance = Vec3(0.7f, 0.0f, 0.0f);

		// 親オブジェクト
		const shared_ptr<GameObject>& m_ParentPtr;


	public:
		// 構築と破棄
		CatChildrenVisual(
			const shared_ptr<Stage>& StagePtr, 
			const Vec3& Scale,
			const Vec3& Rotation,
			const Vec3& Position,
			const shared_ptr<GameObject>& ParentPtr = nullptr
		);
		virtual ~CatChildrenVisual();

		// 初期化
		virtual void OnCreate() override;
		// 更新
		virtual void OnUpdate() override;

		// 衝突判定
		// 衝突した瞬間
		virtual void OnCollisionEnter(shared_ptr<GameObject>& other)override;

		// 衝突している間
		virtual void OnCollisionExcute(shared_ptr<GameObject>& other)override;

		// 衝突が終わったら
		virtual void OnCollisionExit(shared_ptr<GameObject>& other)override;

		// ゲッター
		// 親と視界の距離
		Vec3 GetParentDistance();
		// 親
		shared_ptr<Cat> GetMyParent();
	};

	// 猫の気になる範囲(子オブジェクト)
	//class CatChildrenCare : public GameObject
	//{
	//	// 大きさ、回転、位置
	//	Vec3 m_Scale;
	//	Vec3 m_Rotation;
	//	Vec3 m_Position;


	//	// 親オブジェクト
	//	const shared_ptr<GameObject>& m_ParentPtr;

	//	// 気になる時間
	//	float m_CareTime = 5.0f;
	//	float m_CareTotalTime = 0.0f;

	//public:
	//	// 構築と破棄
	//	CatChildrenCare(
	//		const shared_ptr<Stage>& StagePtr,
	//		const Vec3& Scale,
	//		const Vec3& Rotation,
	//		const Vec3& Position,
	//		const shared_ptr<GameObject>& ParentPtr = nullptr
	//	);
	//	virtual ~CatChildrenCare();

	//	// 初期化
	//	virtual void OnCreate() override;
	//	// 更新
	//	virtual void OnUpdate() override;

	//	// 衝突判定
	//	// 衝突した瞬間
	//	virtual void OnCollisionEnter(shared_ptr<GameObject>& other)override;

	//	// 衝突している間
	//	virtual void OnCollisionExcute(shared_ptr<GameObject>& other)override;

	//	// 衝突が終わったら
	//	virtual void OnCollisionExit(shared_ptr<GameObject>& other)override;

	//	// ゲッターセッター
	//	// 親
	//	shared_ptr<Cat> GetMyParent();
	//	float GetCareTime();
	//	void SetCareTime();
	//};


	// 猫ビックリマーク
	class CatExclamationMark : public GameObject
	{
		// アニメーション用
		// 色の設定
		const Col4 White = Col4(1.0f, 1.0f, 1.0f, 1.0f);
		// 画像半分
		const float HalfSize = 0.5f;
		// 画像最大値
		float m_maxSize = 1.0f;
		// ひとつ前の頂点
		vector<VertexPositionColorTexture> m_Vertices;
		// インデックス
		vector<uint16_t> m_Indices;
		// UV用
		Rect2D<float> m_AnimTip[1][1];
		// 自身の配列の要素
		int m_AnimTipCol = 0;
		int m_AnimTipRow = 0;


		// 大きさ、回転、位置
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;

		// 親との距離
		const Vec3 ParentDistance = Vec3(0.4f, 0.3f, 0.0f);

		// 親オブジェクト
		const shared_ptr<GameObject>& m_ParentPtr;
		// 親の向き
		float m_parentDir;
		// 親の前の気分ゲージ
		int m_beforeMoodGage;

		// 子オブジェクト
		shared_ptr<GameObject> m_childPtr;

	public:
		// 構築と破棄
		CatExclamationMark(
			const shared_ptr<Stage>& StagePtr,
			const Vec3& Scale,
			const Vec3& Rotation,
			const Vec3& Position,
			const shared_ptr<GameObject>& ParentPtr = nullptr
		);
		virtual ~CatExclamationMark();

		// 初期化
		virtual void OnCreate()override;

		// 更新
		virtual void OnUpdate()override;

		// 画像の更新
		void SpriteUpdate();

		// ゲッター
		// 親の向き
		float GetParentDir()const;
		// 子オブジェクト
		shared_ptr<GameObject>& GetChild();
	};

	// 猫ビックリマークフレーム
	class CatExclamationMarkFrame : public GameObject
	{
		// 大きさ、回転、位置
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;
		// 画像半分
		const float HalfSize = 0.5f;
		// 画像最大値
		float m_maxSize = 1.0f;

		// 親オブジェクト
		const shared_ptr<GameObject>& m_ParentPtr;
		float m_parentDir = 1.0f;

	public : 
		// 構築と破棄
		CatExclamationMarkFrame(
			const shared_ptr<Stage>& StagePtr,
			const Vec3& Scale,
			const Vec3& Rotation,
			const Vec3& Position,
			const shared_ptr<GameObject>& ParentPtr = nullptr
		);
		virtual ~CatExclamationMarkFrame();

		// 初期化
		virtual void OnCreate()override;

		// 更新
		virtual void OnUpdate()override;
	};


	// ロードシーン用ねこクラス
	class LoadCat : public SS5ssae
	{
		// 大きさ、回転、位置
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;

		// アニメーションランダムで切り替え
		int m_animNum = 0;

		wstring m_currentAnim;
		// 1つ前のアニメーション
		wstring m_beforeAnim;
		float m_animTime = 0.0f;
		enum State { wait, sleep, stretch, nails, walk, play, karikari };
	public:
		// 構築と破棄
		LoadCat(
			const shared_ptr<Stage>& StagePtr,
			const wstring& BaseDir,
			const wstring& XmlfileName,
			const wstring& StartAnimeName,
			const Vec3& Scale,
			const Vec3& Rotation,
			const Vec3& Position
		);
		virtual ~LoadCat();

		// 初期化
		virtual void OnCreate() override;
		// 更新
		virtual void OnUpdate()override;
	};


}
//end basecross
