/*!
@file 
@brief 
*/
#pragma once
#include "stdafx.h"
namespace basecross {
	//-----------------------------------------------------------------
	//ゲームマネージャー
	//選択されたステージなどを把握しておく
	//-----------------------------------------------------------------
	class GameManager {
	private:
		GameManager() {
			m_bgmName = L"NONE";
			m_stageName = L"NONE";
		}

		~GameManager() {}

		static GameManager* GM;

		//現在再生しているBGMのタイトル
		wstring m_bgmName;
		//現在のステージの名前
		wstring m_stageName;

		//現在暗くなっているアイコン
		wstring m_CurrentDim;

		// えさが何回でなくなるか(仮に20回餌あげたらなくなる)
		int m_DiningNum = 20;

		// プレイヤーが家具ひとつ持ってるか
		shared_ptr<GameObject> m_HaveFurniture = nullptr;

		// ミッション演出
		bool m_MissionFlg = false;
		// ミッション終了
		bool m_MissionEndFlg = false;

		//ミッションの中身
		int m_Mission = 0;
		//ミッションのマックス値
		int m_MaxMission = 0;

		// ひとつ前のミッションのナンバー
		int m_BeforeMissionNum = 0;
		// 今のミッションのナンバー
		int m_CurrentMissionNum = 0;
		// 仲良くなった猫の数
		int m_CatNum = 0;
		// 仲良くなった猫の数（ミッション用）
		int m_MissionCatNum = 0;
		// 毛糸玉動かした数（ミッション用）
		int m_MissionYarnBall = 0;
		// 家具動かした数（ミッション用）
		int m_MissionMoveFurniture = 0;
		// フェードしてるかのフラグ
		bool m_FadeFlg = false;
		bool m_FadeEndFlg = false;
	public:
		static GameManager* GetInstance();
		// ミッション用
		enum Mission{M1, M2, M3, M4, M5};

		// ミッションの更新
		void MissionUpdate()
		{

			// ミッションの中身
			switch (m_CurrentMissionNum)
			{
			case GameManager::Mission::M1:
				//1匹の猫にえさあげる
				m_MissionCatNum = 0;
				m_Mission = 0;
				m_MaxMission = 1;
				break;
			case GameManager::Mission::M2:
				// 3匹の猫にえさあげる
				m_MissionCatNum = 0;
				m_Mission = 0;
				m_MaxMission = 3;
				// ここでレア出せるといいかな
				break;
			case GameManager::Mission::M3:
				// 毛糸玉一回使う
				m_MissionYarnBall = 0;
				m_Mission = 0;
				m_MaxMission = 1;
				break;
			case GameManager::Mission::M4:
				// クッションの位置を一回変える
				m_MissionMoveFurniture = 0;
				m_Mission = 0;
				m_MaxMission = 1;
				break;
			case GameManager::Mission::M5:
				// とりあえずここまで
				// 10匹の猫にえさあげる
				m_MissionCatNum = 0;
				m_Mission = 0;
				m_MaxMission = 10;
				break;
			default:
				// ミッション終了
				m_MissionEndFlg = true;
				break;
			}
		}

		// m_Missionの更新
		void UpdateMissionGage(int Type)
		{
			switch ((Mission)Type)
			{
			case GameManager::Mission::M1:
				// 猫1匹
				m_Mission = m_MissionCatNum;
				break;
			case GameManager::Mission::M2:
				// 猫3匹
				m_Mission = m_MissionCatNum;
				break;
			case GameManager::Mission::M3:
				// 毛糸玉1個
				m_Mission = m_MissionYarnBall;
				break;
			case GameManager::Mission::M4:
				// クッション1移動
				m_Mission = m_MissionMoveFurniture;
				break;
			case GameManager::Mission::M5:
				// 猫10匹
				m_Mission = m_MissionCatNum;
				break;
			default:
				break;
			}
		}

		//m_bgmNameのゲッター＆セッター
		wstring GetBgmName() {
			return m_bgmName;
		}
		void SetBgmName(wstring bgmName) {
			m_bgmName = bgmName;
		}

		//m_stageNameのゲッター＆セッター
		wstring GetStageName() {
			return m_stageName;
		}
		void SetStageName(wstring stageName) {
			m_stageName = stageName;
		}

		//m_CurrentDimのゲッター＆セッター
		wstring GetCurrentDim() {
			return m_CurrentDim;
		}
		void SetCurrentDim(wstring key) {
			m_CurrentDim = key;
		}

		// m_DiningNumのゲッターセッター
		int GetDiningNum()
		{
			return m_DiningNum;
		}
		void SetDiningNum(int setNum)
		{
			m_DiningNum = setNum;
		}
		void SubDiningNum(int subNum)
		{
			if (m_DiningNum <= 0)return;
			m_DiningNum -= subNum;
		}

		// m_HaveFurnitureのゲッターセッター
		shared_ptr<GameObject>& GetHaveFurniture()
		{
			return m_HaveFurniture;
		}
		void SetHaveFurniture(const shared_ptr<GameObject>& haveObj)
		{
			m_HaveFurniture = haveObj;
		}
		bool CheckHaveFurniture(const shared_ptr<GameObject>& haveObj)
		{
			if (m_HaveFurniture == haveObj)
			{
				return true;
			}
			return false;
		}

		//m_CatNumのゲッター＆セッター
		int GetCatNum()
		{
			return m_CatNum;
		}
		void SetCatNum(int catNum)
		{
			m_CatNum = catNum;
		}
		// 追加
		void AddCatNum(int addNum)
		{
			m_CatNum += addNum;
		}
		// 猫の数リセット
		void ResetCatNum()
		{
			SetCatNum(0);
		}

		// m_CurrentMissionNumのゲッターセッター
		int GetCurrentMissionNum()
		{
			return m_CurrentMissionNum;
		}
		void SetCurrentMissionNum(int setNum)
		{
			m_CurrentMissionNum = setNum;
		}
		void AddCurrentMissionNum(int addNum)
		{
			m_CurrentMissionNum += addNum;
		}
		int GetBeforeMissionNum()
		{
			return m_BeforeMissionNum;
		}
		void SetBeforeMissionNum(int setNum)
		{
			m_BeforeMissionNum = setNum;
		}
		void ResetMission()
		{
			m_BeforeMissionNum = 0;
			m_CurrentMissionNum = 0;
			m_Mission = 0;
			m_MaxMission = 0;
		}
		// m_Missionとm_MaxMissionのゲッター
		int GetMission()
		{
			return m_Mission;
		}
		int GetMaxMission()
		{
			return m_MaxMission;
		}

		// m_MissionCatNumのゲッターセッター
		int GetMissionCatNum()
		{
			return m_MissionCatNum;
		}
		void SetMissionCatNum(int num)
		{
			m_MissionCatNum = num;
		}
		void AddMissionCatNum(int num)
		{
			m_MissionCatNum += num;
		}
		// m_MissionYarnBallのゲッターセッター
		int GetMissionYarnBall()
		{
			return m_MissionYarnBall;
		}
		void SetMissionYarnBall(int num)
		{
			m_MissionYarnBall = num;
		}
		void AddMissionYarnBall(int num)
		{
			m_MissionYarnBall += num;
		}

		// m_MissionMoveFurnitureのゲッターセッター
		int GetMissionMoveFurniture()
		{
			return m_MissionMoveFurniture;
		}
		void SetMissionMoveFurniture(int num)
		{
			m_MissionMoveFurniture = num;
		}
		void AddMissionMoveFurniture(int num)
		{
			m_MissionMoveFurniture += num;
		}

		// ミッション演出フラグ
		bool GetMissionFlg()
		{
			return m_MissionFlg;
		}
		void SetMissionFlg(bool setFlg)
		{
			m_MissionFlg = setFlg;
		}
		// ミッション終了フラグ
		bool GetMissionEndFlg()
		{
			return m_MissionEndFlg;
		}
		void SetMissionEndFlg(bool setFlg)
		{
			m_MissionEndFlg = setFlg;
		}
		// m_Fadeflgのゲッターセッター
		bool GetFadeFlg()
		{
			return m_FadeFlg;
		}
		void SetFadeFlg(bool setFlg)
		{
			m_FadeFlg = setFlg;
		}

		// m_FadeEndflgのゲッターセッター
		bool GetFadeEndFlg()
		{
			return m_FadeEndFlg;
		}
		void SetFadeEndFlg(bool setFlg)
		{
			m_FadeEndFlg = setFlg;
		}

	};
}

//end basecross