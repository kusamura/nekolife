
/*!
@file Scene.cpp
@brief シーン実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross{
	//-------------------------------------------------------------------------
	//ゲームマネージャー
	//選択されているステージを把握しておく
	//-------------------------------------------------------------------------
	GameManager* GameManager::GM = nullptr;
	GameManager* GameManager::GetInstance() {
		if (GM == nullptr) {
			GM = new GameManager();
		}
		return GM;
	}
	//-------------------------------------------------------------------------
	//バックアップ
	//ゲームのデータを保存
	//-------------------------------------------------------------------------
	BackUp* BackUp::backUp = nullptr;
	BackUp* BackUp::GetInstance() {
		if (backUp == nullptr) {
			backUp = new BackUp();
		}
		return backUp;
	}

	//-------------------------------------------------------------------------
	//シーン
	//-------------------------------------------------------------------------

	void Scene::CreateResources()
	{
		wstring dataDir;
		// データディレクトリを取得
		App::GetApp()->GetDataDirectory(dataDir);
		wstring textureDir = dataDir + L"Textures\\";
		wstring audioDir = dataDir + L"Audios\\";

		struct TxPair 
		{
			wstring Key;
			wstring FileName;
		};

		struct AuPair
		{
			wstring Key;
			wstring FileName;
		};

		// 仮
		// Texture
			//--------------------------------------------------------------------------------------
		///	ゲームシーン
		//--------------------------------------------------------------------------------------
		TxPair txPairs[] =
		{
			// タイトルから必要
			{WstringKey::tx_Title, L"nekolife2.png"},
			{WstringKey::tx_StartButton, L"スタートボタン2.png"},
			{WstringKey::tx_BG, L"background3.png"},
			{WstringKey::tx_BG2, L"result_back.png"},
			{L"Tx_White", L"TX_White.png"},
			{WstringKey::tx_Pointer, L"Pointer.png"},
			// ゲームステージ内
			{L"TX_CatVisual", L"visual.png"},
			{L"TX_Catnip", L"IMG_0052.PNG"},
			{WstringKey::tx_MouseRC, L"Mouse.png"},
			{WstringKey::tx_YarnBall, L"Yarnball.png"},
			{WstringKey::tx_CatDining, L"karikari.png"},
			{WstringKey::tx_UICatnipButton, L"猫じゃらし.png"},
			{WstringKey::tx_UIMouseRCButton, L"UIMouse.png"},
			{WstringKey::tx_UIYarnBall, L"UIYarnball.png"},
			{WstringKey::tx_BackButton, L"BackButton.png"},
			{WstringKey::tx_IconBackGround, L"IconBackGround.png"},
			{L"TX_Cheek", L"wa.png"},
			{WstringKey::tx_CatExclamationMark, L"exclamationmark2.png"},
			{WstringKey::tx_CatExclamationMarkFrame, L"exclamationmark1.png"},
			{L"Tx_MoodParticle", L"heart.png"}
		};

		//Audio関連
		AuPair auPairs[] =
		{
			// BGM
			{WstringKey::bgm_TitleStage, L"TitleStageBGM.wav"},
			{WstringKey::bgm_GameStage, L"GameStageBGM.wav"},
			// SE
			{L"se_Button", L"Bottan2.wav"},
			{L"nya", L"cat niya-.wav"},
			{WstringKey::se_Gorogoro, L"gorogoro.wav"},
			{L"se_Catfight", L"Catfight2.wav"},
			{L"karikari", L"CatEating.wav"},
			{WstringKey::se_CatnipSwing, L"Swing.wav"},
			{WstringKey::se_Button, L"Select.wav"},
			{WstringKey::se_Razikon, L"Razikon1.wav"},
			{WstringKey::se_Yarnball, L"keitodama.wav"}
		};

		for (auto& pair : txPairs) {
			wstring strTexture = textureDir + pair.FileName;
			App::GetApp()->RegisterTexture(pair.Key, strTexture);
		}

		for (auto& pair : auPairs) {
			wstring strAudio = audioDir + pair.FileName;
			App::GetApp()->RegisterWav(pair.Key, strAudio);
		}
	}
	//--------------------------------------------------------------------------------------
	///	ゲームシーン
	//--------------------------------------------------------------------------------------
	void Scene::OnCreate(){
		// リソース生成
		//CreateResources();

		try {
			//カーソル
			::ShowCursor(false);
			//クリアする色を設定
			Col4 Col;
			Col.set(31.0f / 255.0f, 30.0f / 255.0f, 71.0f / 255.0f, 255.0f / 255.0f);
			SetClearColor(Col);
			//自分自身にイベントを送る
			//これにより各ステージやオブジェクトがCreate時にシーンにアクセスできる
			PostEvent(0.0f, GetThis<ObjectInterface>(), GetThis<Scene>(), L"ToCopyRightStage");
		}
		catch (...) {
			throw;
		}
	}

	Scene::~Scene() {
	}

	void Scene::OnEvent(const shared_ptr<Event>& event) {
		if (event->m_MsgStr == L"ToGameStage") {
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage>();
			//BGMの再生
			auto gm = GameManager::GetInstance();
			auto XAPtr = App::GetApp()->GetXAudio2Manager();
			if (gm->GetBgmName() != WstringKey::bgm_GameStage) {
				XAPtr->Stop(m_AudioItem);
			}
			gm->SetStageName(L"ToGameStage");
			m_AudioItem = XAPtr->Start(WstringKey::bgm_GameStage, XAUDIO2_LOOP_INFINITE, 0.5f);
			gm->SetBgmName(WstringKey::bgm_GameStage);
		}
		else if (event->m_MsgStr == L"ToTitleStage") 
		{
			//アクティブステージの設定
			ResetActiveStage<TitleStage>();
			//BGMの再生
			auto gm = GameManager::GetInstance();
			auto XAPtr = App::GetApp()->GetXAudio2Manager();
			if (gm->GetBgmName() != WstringKey::bgm_TitleStage) {
				XAPtr->Stop(m_AudioItem);
			}
			gm->SetStageName(L"ToTitleStage");
			m_AudioItem = XAPtr->Start(WstringKey::bgm_TitleStage, XAUDIO2_LOOP_INFINITE, 0.5f);
			gm->SetBgmName(WstringKey::bgm_TitleStage);
		}
		else if (event->m_MsgStr == L"ToCopyRightStage")
		{
			//アクティブステージの設定
			ResetActiveStage<CopyRightStage>();
			auto gm = GameManager::GetInstance();
			gm->SetStageName(L"ToCopyRightStage");
			auto XAPtr = App::GetApp()->GetXAudio2Manager();
			XAPtr->Stop(m_AudioItem);
		}
		else if (event->m_MsgStr == L"ToResultStage")
		{
			//アクティブステージの設定
			ResetActiveStage<ResultStage>();
			auto gm = GameManager::GetInstance();
			gm->SetStageName(L"ToResultStage");
			auto XAPtr = App::GetApp()->GetXAudio2Manager();
			XAPtr->Stop(m_AudioItem);
		}
		else if (event->m_MsgStr == L"ToLoadStage")
		{
			// アクティブステージの設定
			ResetActiveStage<LoadStage>();
			auto gm = GameManager::GetInstance();
			gm->SetStageName(L"ToLoadStage");
			auto XAPtr = App::GetApp()->GetXAudio2Manager();
			XAPtr->Stop(m_AudioItem);
		}
	}

}
//end basecross
