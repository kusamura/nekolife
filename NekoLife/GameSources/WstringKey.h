
#pragma once
#include "stdafx.h"

namespace WstringKey {
	// 今度まとめて統一させる
	// テクスチャキー

	//--------------------------------------------------------------------------------------
	///	テクスチャ関連
	//--------------------------------------------------------------------------------------
	const wstring tx_Title = L"TX_TitleLogo";
	// 背景
	const wstring tx_BG = L"TX_BackGround";
	const wstring tx_BG2 = L"TX_BackGround2;";// 2はリザルトとかローディングで使えそう
	// 猫
	const wstring tx_Cat = L"TX_Cat";
	// 猫ビックリマーク
	const wstring tx_CatExclamationMarkFrame = L"TX_CatExclamationMarkFrame";
	const wstring tx_CatExclamationMark = L"TX_CatExclamationMark";
	// ご飯
	const wstring tx_CatDining = L"TX_CatDining";
	// ポインター
	const wstring tx_Pointer = L"TX_Pointer";
	const wstring tx_DownPointer = L"TX_DownPointer";
	// 猫じゃらし、ネズミ、毛糸玉
	const wstring tx_Catnip = L"TX_Catnip";
	const wstring tx_MouseRC = L"TX_MouseRC";
	const wstring tx_YarnBall = L"TX_YarnBall";
	// 猫じゃらしボタン
	const wstring tx_UICatnipButton = L"TX_UICatnipButton";
	// ネズミラジコンボタン
	const wstring tx_UIMouseRCButton = L"TX_UIMouseRCButton";
	// 毛糸ボタン
	const wstring tx_UIYarnBall = L"TX_UIYarnBall";
	// 戻るボタン
	const wstring tx_BackButton = L"TX_BackButton";
	// スタートボタン
	const wstring tx_StartButton = L"TX_StartButton";
	//アイコンの背景
	const wstring tx_IconBackGround = L"TX_IconBackGround";

	//--------------------------------------------------------------------------------------
	///	オーディオ関連
	//--------------------------------------------------------------------------------------
	const wstring se_Gorogoro = L"Se_Gorogoro";
	const wstring se_Eating = L"SE_Eating";
	const wstring se_CatnipSwing = L"SE_CatnipSwing";
	const wstring se_Button = L"SE_Button";
	const wstring se_Razikon = L"SE_Razikon";
	const wstring se_Yarnball = L"SE_Yarnball";
	const wstring bgm_GameStage = L"Bgm_GameStage";
	const wstring bgm_TitleStage = L"Bgm_TitleStage";

	//--------------------------------------------------------------------------------------
	///	タグ関連
	//--------------------------------------------------------------------------------------
	// 背景
	const wstring tag_BG = L"Tag_BackGround";
	const wstring tag_IconBG = L"Tag_IconBackGround";
	//猫
	const wstring tag_Cat = L"tag_Cat";
	// ご飯
	const wstring tag_Meal = L"Tag_Meal";
	// おもちゃ全部にこれつけてくださいおねがいします
	// あとは判別できるようにもう一つずつ
	const wstring tag_Toy = L"Tag_Toy";
	const wstring tag_Catnip = L"Tag_Catnip";
	const wstring tag_MouseRC = L"Tag_MouseRC";
	const wstring tag_YarnBall = L"Tag_YarnBall";
	//おもちゃのアイコン
	const wstring tag_Icon = L"Tag_Icon";
	const wstring tag_UICatnip = L"Tag_UICatnip";
	const wstring tag_UIMouseRC = L"Tag_UIMouseRC";
	const wstring tag_UIYarnBall = L"Tag_UIYarnBall";
	//戻るアイコン
	const wstring tag_BackIcon = L"Tag_BackIcon";

}

//end basecross

