/*!
@file Player.h
@brief プレイヤーなど
*/

#pragma once
#include "stdafx.h"

namespace basecross{
	//おもちゃの列挙型
	enum PlayerStatus {
		CatnipStatus,
		MouseRCStatus,
		YarnBallStatus,
		//YarnBallInstallation,
		PointerStatus
	};

	//------------------------------------------------------------------------------
	//マウスのクラス
	//------------------------------------------------------------------------------
	class PlayerMouse : public GameObject {
		Point2D<int> m_MousePoint;
		wstring m_MouseDown;
		wstring m_MouseUp;
		Vec3 m_Latepos;
		float m_Timer;
		float m_speed;
		void DrawString();
	public:
		//コンストラクタ
		PlayerMouse(const shared_ptr<Stage>& StagePtr);
		//デストラクタ
		~PlayerMouse();

		//初期化
		virtual void OnCreate();
		//更新
		virtual void OnUpdate();
		virtual void OnUpdate2();
		//マウスとカメラのレイの取得
		void GetMouseRay(Vec3& Near, Vec3& Far);
		//左クリックされた時
		void OnLButtonClick();
		//選択されたオブジェクトを解除
		void SelectClear();
		//左クリックが離された時
		void OnLButtonUp();
		//アクセサ
		Point2D<int> GetMousePoint() {
			return m_MousePoint;
		}
		//マウスの速度を測定
		float GetMouseMeasu();
	};

	//------------------------------------------------------------------------------
	//ポインターのクラス
	//------------------------------------------------------------------------------
	class PlayerPointer : public SS5ssae {
		Vec3 m_Scale;
		Vec3 m_Rotate;
		Vec3 m_Position;
		Vec3 m_Velocity;
		Vec3 m_Force;
		InputHandler<PlayerPointer> m_InputHandler;
		PlayerStatus m_Status; //玩具の判別
		PlayerStatus m_BeforStatus; //一つ前の状態を入れる
		bool m_CheckColOut; //ポインターが衝突判定から出たかどうかのフラグ
		bool m_CheckHitOther; //ポインターが他のオブジェクトに当たってたら
		bool m_StartTime; //タイマー開始フラグ
		bool m_EndTime; //タイマー終了フラグ
		bool m_MoodUpFlg; //猫の気分が上がったかのフラグ
		bool m_CatSeeFlg;; //猫が見ているのかフラグ
		bool m_isInIconBG; //UIの背景に入ってるかフラグ
		bool m_YarnballSelect; //毛糸玉を選択してるかフラグ
		bool m_AnimLoop; //アニメーションがループするかフラグ
		int m_CatnipRapidCount; //連打カウント
		float m_LandingPoint; //着地地点
		float m_AnimPlayTimer; //アニメーションの再生時間
		float m_CatnipTimer;
		float m_CatSeeTimer;
		float m_ButtonHoldTimer; //Aボタンを長押した時間
		shared_ptr<SoundItem> m_AudioItem;
		wstring debug;
	public:
		PlayerPointer(const shared_ptr<Stage>& StagePtr, 
			const Vec3& Scale, 
			const Vec3& Rotate, 
			const Vec3& Position, 
			const wstring& BaseDir);
		virtual ~PlayerPointer();
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
		virtual void OnUpdate2() override;
		//プレイヤーの移動
		void PlayerMove(CONTROLER_STATE cntlVec);
		//ポインターの操作
		void PointerMove();
		//猫じゃらしの操作
		void CatnipOperate();
		//ネズミラジコンの操作
		void MouseRCOperate();
		//毛糸玉の操作
		void YarnballOperate();
		//Aボタン・Xボタンの処理
		void OnPushA();
		void OnPushX();
		//アイテム選択
		void SelectItem();
		//アイテム切り替え
		void SwitchItem(PlayerStatus Status);
		//アイコンを暗くする判断
		void SwitchIconDim();
		//ポインター衝突判定
		void SwitchPointer();
		//画面外に行かないように
		void NotOutScreen();
		//OnCollisionGO判定
		virtual void OnCollisionEnter(shared_ptr<GameObject>& other);
		virtual void OnCollisionExcute(shared_ptr<GameObject>& other);
		virtual void OnCollisionExit(shared_ptr<GameObject>& other);
		//デバッグ文字列
		void DrawString();
		//m_CheckColOutのセッター
		void SetCheckColOut(bool ColOut) {
			m_CheckColOut = ColOut;
		}
		//m_CatnipRapidCountのゲッター
		int GetCatnipRapidCount() {
			return m_CatnipRapidCount;
		}
	};

	//------------------------------------------------------------------------------
	//毛糸玉
	//------------------------------------------------------------------------------
	class YarnBallInstallation : public GameObject {
		Vec3 m_Scale;
		Vec3 m_Position;
		Vec3 m_Velocity;
		Vec3 m_Force;
		float m_StartPoint;
		float m_MidPoint;
		float m_EndPoint;
		float m_Speed;
		float m_TotalTime;
		float m_ElapsedTime;
		bool m_BallLandingFlg;
		shared_ptr<SoundItem> m_AudioItem;
		wstring db;
	public:
		YarnBallInstallation(const shared_ptr<Stage>& StagePtr, const Vec3& Scale, const Vec3& Position, const float& EndPoint);
		virtual ~YarnBallInstallation();
		//ボールの移動
		void YarnballThrow();
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
		virtual void OnUpdate2() override;
		void DrawString();
	};


	//------------------------------------------------------------------------------
	//ネズミラジコン
	//------------------------------------------------------------------------------
	class MouseRCInstallation : public GameObject {
		Vec3 m_Scale;
		Vec3 m_Position;
		Vec3 m_Force;
		Vec3 m_Velocity;
		bool m_razinkonSECheck;
		bool m_OffScreen;
		float m_Speed;
		shared_ptr<SoundItem> m_AudioItem;
	public:
		MouseRCInstallation(const shared_ptr<Stage>& StagePtr, const Vec3& Scale, const Vec3& Position);
		virtual ~MouseRCInstallation();
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
		//慣性
		void ApplyForce();
		//ネズミラジコンの移動
		void MoveMouseRC();
		//ネズミを画面に設置
		void Installation();

		//アクセサ
		bool GetOffScreen() {
			return m_OffScreen;
		}
	};

	//------------------------------------------------------------------------------
	//おもちゃアイコン
	//------------------------------------------------------------------------------
	class IconSprite : public GameObject {
		Vec3 m_Scale;
		Vec3 m_Position;
		bool m_IsSelect;
		wstring m_TX;
		wstring m_Tag;
		PlayerStatus m_Toy;
	public:
		IconSprite(const shared_ptr<Stage>& StagePtr, const Vec3& Scale, const Vec3& Position, const wstring TX, const PlayerStatus status, const wstring tag);
		virtual ~IconSprite();
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
		//選択時アイコンを暗くする
		void DimIcon();
		//アクセサ
		void SetIsSelect(bool isSelect) {
			m_IsSelect = isSelect;
		}
		PlayerStatus GetToy() {
			return m_Toy;
		}
		wstring GetTag() {
			return m_Tag;
		}
	};

	class BackIcon : public GameObject {
		Vec3 m_Scale;
		Vec3 m_Position;
	public:
		BackIcon(const shared_ptr<Stage>& StagePtr, const Vec3& Scale, const Vec3& Position);
		virtual ~BackIcon();
		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate()override;
	};

	//------------------------------------------------------------------------------
	//おもちゃアイコンの背景
	//------------------------------------------------------------------------------
	class IconBackGround : public GameObject {
		Vec3 m_Scale;
		Vec3 m_Position;
	public:
		IconBackGround(const shared_ptr<Stage>& StagePtr, const Vec3& Scale, const Vec3& Position);
		virtual ~IconBackGround();
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
	};

	class check : public SS5ssae
	{
		// 大きさ、回転、位置
		Vec3 m_Scale;
		Vec3 m_Position;
	public:
		// 構築と破棄
		check(
			const shared_ptr<Stage>& StagePtr,
			const wstring& BaseDir,
			const Vec3& Scale,
			const Vec3& Pos
		);
		virtual ~check() {};
		// 初期化
		virtual void OnCreate() override;
		virtual void OnUpdate()override;
	};
}
//end basecross

