/*!
@file Character.cpp
@brief キャラクターなど実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross{

	// 仮オブジェクト
	// 構築と破棄
	FixedBox::FixedBox(const shared_ptr<Stage>& StagePtr, 
		const Vec3& Scale,
		const Vec3& Rotation, 
		const Vec3& Position)
		:
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{

	}
	FixedBox::~FixedBox(){}

	// 初期化
	void FixedBox::OnCreate()
	{
		// 大きさ、回転、位置
		auto ptrTransform = GetComponent<Transform>();
		ptrTransform->SetScale(m_Scale);
		ptrTransform->SetRotation(m_Rotation);
		ptrTransform->SetPosition(m_Position);

		// CollisionObb衝突判定をつける
		auto ptrColl = AddComponent<CollisionObb>();
		ptrColl->SetFixed(true);

		// タグをつける
		AddTag(L"FixedBox");

		//影をつける（シャドウマップを描画する）
		auto shadowPtr = AddComponent<Shadowmap>();
		//影の形（メッシュ）を設定
		shadowPtr->SetMeshResource(L"DEFAULT_CUBE");
		auto ptrDraw = AddComponent<BcPNTStaticDraw>();
		ptrDraw->SetMeshResource(L"DEFAULT_CUBE");
		ptrDraw->SetFogEnabled(true);
		ptrDraw->SetOwnShadowActive(true);
	}

	// 物理計算するアクティブなオブジェクトの親
	void ActivePsObject::OnUpdate() {
		if (!IsSelect()) {
			return;
		}
		Vec3 Near, Far;
		auto GameObjects = GetTypeStage<GameStage>()->GetGameObjectVec();
		auto PsPtr = GetDynamicComponent<RigidbodySingle>(false);
		for (auto& v : GameObjects) {
			auto PlayerMousePtr = dynamic_pointer_cast<PlayerMouse>(v);
			if (PlayerMousePtr) {
				PlayerMousePtr->GetMouseRay(Near, Far);
				if (PsPtr) {
					auto PsPos = PsPtr->GetPosition();
					float t;
					Vec3 RayPos;
					// 現在位置と一番近いRay上の点を得る
					HitTest::ClosetPtPointSegment(PsPos, Near, Far, t, RayPos);
					Vec3 ToVec = RayPos - PsPos;
					ToVec *= 90.0f;
					ToVec.setZ(0.0f);
					PsPtr->WakeUp();
					PsPtr->SetLinearVelocity(ToVec);
				}
			}
		}
	}

	// 猫じゃらし
	Catnip::Catnip(const shared_ptr<Stage>& StagePtr,
		const Vec3& Scale,
		const Quat& Qt,
		const Vec3& Position
	) :
		ActivePsObject(StagePtr),
		m_Scale(Scale),
		m_Qt(Qt),
		m_Position(Position)
	{

	}

	Catnip::~Catnip(){}

	// 初期化
	void Catnip::OnCreate() {
		auto ptrTransform = GetComponent<Transform>();
		ptrTransform->SetScale(m_Scale);
		ptrTransform->SetQuaternion(m_Qt);
		ptrTransform->SetPosition(m_Position);

		// 衝突判定
		auto ptrCol = AddComponent<CollisionObb>();
		ptrCol->SetAfterCollision(AfterCollision::None);

		Col4 White(1.0f, 1.0f, 1.0f, 1.0f);
		float halfsize = 0.5f;
		vector<VertexPositionColorTexture> vertices = 
		{
			{Vec3(-halfsize, +halfsize, 0), White, Vec2( 0.0f, 0.0f )},
			{Vec3(+halfsize, +halfsize, 0), White, Vec2( 1.0f, 0.0f )},
			{Vec3(-halfsize, -halfsize, 0), White, Vec2( 0.0f, 1.0f )},
			{Vec3(+halfsize, -halfsize, 0), White, Vec2( 1.0f, 1.0f )}
		};

		vector<uint16_t> indices =
		{
			0, 1, 2,
			1, 3, 2
		};

		auto ptrSpriteDraw = AddComponent<PCTStaticDraw>();
		ptrSpriteDraw->CreateOriginalMesh(vertices, indices);
		ptrSpriteDraw->SetOriginalMeshUse(true);
		ptrSpriteDraw->SetTextureResource(L"TX_Catnip");
		ptrSpriteDraw->SetDepthStencilState(DepthStencilState::None);
		SetAlphaActive(true);

		//物理計算
		GetStage()->SetPhysicsActive(true);
		PsBoxParam param(ptrTransform->GetWorldMatrix(), 0.0f, true, PsMotionType::MotionTypeActive);
		auto PsPtr = AddComponent<RigidbodyBox>(param);
		PsPtr->SetDrawActive(true);
	}

	// 物理計算するアクティブなBox
	ActivePsBox::ActivePsBox(const shared_ptr<Stage>& StagePtr,
		const Vec3& Scale,
		const Quat& Qt,
		const Vec3& Position
	) :
		ActivePsObject(StagePtr),
		m_Scale(Scale),
		m_Qt(Qt),
		m_Position(Position)
	{
	}

	ActivePsBox::~ActivePsBox() {}

	// 初期化
	void ActivePsBox::OnCreate() {

	}

	// ごはん(皿を子オブジェクトに)
	CatDining::CatDining(const shared_ptr<Stage>& StagePtr,
		const Vec3& Scale,
		const Quat& Qt,
		const Vec3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Qt(Qt),
		m_Position(Position)
	{

	}

	CatDining::~CatDining() {}

	//初期化
	void CatDining::OnCreate() {
		auto ptrTrans = GetComponent<Transform>();
		ptrTrans->SetScale(m_Scale);
		ptrTrans->SetQuaternion(m_Qt);
		ptrTrans->SetPosition(m_Position);

		// 衝突判定
		auto ptrCol = AddComponent<CollisionObb>();
		ptrCol->SetAfterCollision(AfterCollision::None);
		ptrCol->SetMakedSize(0.8f);

		// タグをつける
		AddTag(WstringKey::tag_Meal);

		Col4 White(1.0f, 1.0f, 1.0f, 1.0f);
		float halfsize = 0.5f;
		vector<VertexPositionColorTexture> vertices =
		{
			{Vec3(-halfsize, +halfsize, 0), White, Vec2(0.0f, 0.0f)},
			{Vec3(+halfsize, +halfsize, 0), White, Vec2(1.0f, 0.0f)},
			{Vec3(-halfsize, -halfsize, 0), White, Vec2(0.0f, 1.0f)},
			{Vec3(+halfsize, -halfsize, 0), White, Vec2(1.0f, 1.0f)}
		};
		vector<uint16_t> indices =
		{
			0, 1, 2,
			1, 3, 2
		};

		// レイヤー
		SetDrawLayer(4);

		auto ptrSpriteDraw = AddComponent<PCTStaticDraw>();
		ptrSpriteDraw->CreateOriginalMesh(vertices, indices);
		ptrSpriteDraw->SetOriginalMeshUse(true);
		ptrSpriteDraw->SetTextureResource(WstringKey::tx_CatDining);
		ptrSpriteDraw->SetDepthStencilState(DepthStencilState::None);
		SetAlphaActive(true);

		// 子オブジェクト(皿)生成
		GetStage()->AddGameObject<Dish>(m_Scale, m_Qt, m_Position, GetThis<GameObject>());
	}

	void CatDining::OnUpdate()
	{
		//auto gm = GameManager::GetInstance();
		//if (gm->GetDiningNum() <= 0)
		//{
		//	SetDrawActive(false);
		//	SetUpdateActive(false);
		//}
	}

	// 皿
	// 構築と破棄
	Dish::Dish(
		const shared_ptr<Stage>& StagePtr,
		const Vec3& Scale,
		const Quat& Qt,
		const Vec3& Position,
		const shared_ptr<GameObject>& Parent)
		:
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Qt(Qt),
		m_Position(Position),
		m_Parent(Parent)
	{
	}
	Dish::~Dish(){}

	// 初期化
	void Dish::OnCreate()
	{
		// 親オブジェクトの設定
		auto ptrTrans = GetComponent<Transform>();
		ptrTrans->SetParent(m_Parent);
		// 親に合わせて大きさ、回転、位置の設定
		auto ptrMyTrans = AddComponent<Transform>();
		ptrMyTrans->SetScale(m_Scale);
		ptrMyTrans->SetQuaternion(m_Qt);
		ptrMyTrans->SetPosition(0.0f, 0.0f, 0.0f);


		// 描画するテクスチャの設定
		Col4 White(1.0f, 1.0f, 1.0f, 1.0f);
		float halfsize = 0.5f;
		vector<VertexPositionColorTexture> vertices =
		{
			{Vec3(-halfsize, +halfsize, 0), White, Vec2(0.0f, 0.0f)},
			{Vec3(+halfsize, +halfsize, 0), White, Vec2(1.0f, 0.0f)},
			{Vec3(-halfsize, -halfsize, 0), White, Vec2(0.0f, 1.0f)},
			{Vec3(+halfsize, -halfsize, 0), White, Vec2(1.0f, 1.0f)}
		};
		vector<uint16_t> indices =
		{
			0, 1, 2,
			1, 3, 2
		};

		// レイヤー
		SetDrawLayer(3);

		// 頂点とインデックスを指定してスプライト生成
		auto ptrSpriteDraw = AddComponent<PCTStaticDraw>();
		ptrSpriteDraw->CreateOriginalMesh(vertices, indices);
		ptrSpriteDraw->SetOriginalMeshUse(true);
		ptrSpriteDraw->SetTextureResource(L"TX_Dish");
		ptrSpriteDraw->SetDepthStencilState(DepthStencilState::None);
		SetAlphaActive(true);
	}

	// カーペット
	// 構築と破棄
	Carpet::Carpet(
		const shared_ptr<Stage>& StagePtr, 
		const Vec3& Scale, 
		const Vec3& Rotation, 
		const Vec3& Position)
		:
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	Carpet::~Carpet(){}

	// 初期化
	void Carpet::OnCreate()
	{
		// 大きさ、回転、位置
		auto ptrTrans = GetComponent<Transform>();
		ptrTrans->SetScale(m_Scale);
		ptrTrans->SetRotation(m_Rotation);
		ptrTrans->SetPosition(m_Position);

		// 衝突判定
		auto ptrColl = AddComponent<CollisionObb>();
		ptrColl->SetAfterCollision(AfterCollision::None);// トリガー設定
		//ptrColl->SetDrawActive(true);// デバッグ用

		// タグをつける
		AddTag(L"tag_Carpet");
		AddTag(L"tag_Furniture");// 家具
		// レイヤー
		SetDrawLayer(1);
	}

	// クッション
	// 構築と破棄
	Cushion::Cushion(
		const shared_ptr<Stage>& StagePtr,
		const Vec3& Scale,
		const Vec3& Rotation,
		const Vec3& Position,
		const wstring& Tx
	)
		:
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position),
		m_TX(Tx)
	{
	}
	Cushion::~Cushion() {}

	// 初期化
	void Cushion::OnCreate()
	{
		// 大きさ、回転、位置
		auto ptrTrans = GetComponent<Transform>();
		ptrTrans->SetScale(m_Scale);
		ptrTrans->SetRotation(m_Rotation);
		ptrTrans->SetPosition(m_Position);

		//衝突判定
		auto ptrColl = AddComponent<CollisionObb>();
		ptrColl->SetAfterCollision(AfterCollision::None);// トリガー設定
		//ptrColl->SetDrawActive(true);// デバッグ用

		// 描画するテクスチャの設定
		Col4 White = Col4(1.0f, 1.0f, 1.0f, 1.0f);// 白
		// 画像の半分
		float halfSize = 0.5f;
		vector<VertexPositionColorTexture> vertices =
		{
			{Vec3(-halfSize, +halfSize, 0), White, Vec2(0.0f, 0.0f)},
			{Vec3(+halfSize, +halfSize, 0), White, Vec2(1.0f, 0.0f)},
			{Vec3(-halfSize, -halfSize, 0), White, Vec2(0.0f, 1.0f)},
			{Vec3(+halfSize, -halfSize, 0), White, Vec2(1.0f, 1.0f)}
		};
		vector<uint16_t> indices =
		{
			0, 1, 2,
			1, 3, 2
		};

		// コリジョンに合わせて画像大きさ、回転、位置
		Mat4x4 mat;
		mat.affineTransformation(
			Vec3(3.0f, 3.0f, 3.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			Vec3(0.0f, 0.0f, 0.0f),
			Vec3(0.0f, -0.4f, 0.0f)
		);

		// 頂点とインデックスを指定してスプライト生成
		auto ptrDraw = AddComponent<PCTStaticDraw>();
		ptrDraw->CreateOriginalMesh(vertices, indices);
		ptrDraw->SetOriginalMeshUse(true);
		ptrDraw->SetTextureResource(m_TX);
		ptrDraw->SetDepthStencilState(DepthStencilState::None);
		ptrDraw->SetMeshToTransformMatrix(mat);

		// 透明処理
		SetAlphaActive(true);

		// タグをつける
		AddTag(L"Tag_Cushion");
		AddTag(L"tag_Furniture");// 家具
		// レイヤー
		SetDrawLayer(1);
	}

}
//end basecross
