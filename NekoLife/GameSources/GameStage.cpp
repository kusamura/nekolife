/*!
@file GameStage.cpp
@brief ゲームステージ実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス実体
	//--------------------------------------------------------------------------------------
	void GameStage::CreateViewLight() {
		const Vec3 eye(0.0f, 0.0f, -5.0f);
		const Vec3 at(0.0f);
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrCamera = ObjectFactory::Create<Camera>();
		PtrView->SetCamera(PtrCamera);
		PtrCamera->SetEye(eye);
		PtrCamera->SetAt(at);
		//マルチライトの作成
		auto PtrMultiLight = CreateLight<MultiLight>();
		//デフォルトのライティングを指定
		PtrMultiLight->SetDefaultLighting();
	}

	// スプライトの生成
	void GameStage::CreateSprite()
	{
		// 背景
		AddGameObject<BackGround>(true, Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f));

		// ミッションゲージ
		AddGameObject<MissionGageFrame>(true, Vec2(500.0f, 150.0f), Vec2(450.0f, -450.0f), L"TX_MissionGageFrame2", 10);
		AddGameObject<MissionGage>(true, Vec2(500.0f, 100.0f), Vec2(450.0f, -450.0f), L"TX_MissionGage");
		AddGameObject<MissionGageFrame>(true, Vec2(500.0f, 100.0f), Vec2(450.0f, -450.0f), L"TX_MissionGageFrame", 13);
		// SpriteとなっていますがPCTStaticDrawになりました
		AddGameObject<MissionSprite>( Vec3(0.5f, 0.5f, 0.5f), Vec3(0.0f, 0.0f, 0.0f), Vec3(1.65f, -1.7f, 0.0f), L"TX_MissionMark", 14);

		// ミッション文字
		// SharedGameObjectにいれて呼び出し
		// 1
		auto MissionStr = CreateSharedObjectGroup(L"MissionStr");

		auto missionStr1 = AddGameObject<Sprite>(true, Vec2(450.0f, 75.0f), Vec2(700.0f, -450.0f), L"TX_MissionStr1", 14);
		MissionStr->IntoGroup(missionStr1);
		missionStr1->SetDrawActive(true);
		// 2
		auto missionStr2 = AddGameObject<Sprite>(true, Vec2(450.0f, 75.0f), Vec2(700.0f, -450.0f), L"TX_MissionStr2", 14);
		MissionStr->IntoGroup(missionStr2);
		missionStr2->SetDrawActive(false);
		// 3
		auto missionStr3 = AddGameObject<Sprite>(true, Vec2(450.0f, 75.0f), Vec2(700.0f, -450.0f), L"TX_MissionStr3", 14);
		MissionStr->IntoGroup(missionStr3);
		missionStr3->SetDrawActive(false);
		// 4
		auto missionStr4 = AddGameObject<Sprite>(true, Vec2(450.0f, 75.0f), Vec2(700.0f, -450.0f), L"TX_MissionStr4", 14);
		MissionStr->IntoGroup(missionStr4);
		missionStr4->SetDrawActive(false);
		// 5
		auto missionStr5 = AddGameObject<Sprite>(true, Vec2(450.0f, 75.0f), Vec2(700.0f, -450.0f), L"TX_MissionStr5", 14);
		MissionStr->IntoGroup(missionStr5);
		missionStr5->SetDrawActive(false);

		// フェード
		AddGameObject<Fade>(true, Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f),WstringKey::tx_BG2);
		// フェード設定
		auto gm = GameManager::GetInstance();
		gm->SetFadeFlg(true);
		gm->SetFadeEndFlg(false);

	}

	// オブジェクトの生成
	void GameStage::CreateGameObject()
	{
		//AddGameObject<FixedBox>(Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(2.0f, 0.0f, 0.0f));

		CreateCat();
		CreateIcon();
		CreateBackGround();
		CreatePlayerPointer();
		CreateParticle();
		//CreateCatnip();
		//auto ptrPlayerMouse = AddGameObject<PlayerMouse>();
		//SetSharedGameObject(L"PlayerMouse", ptrPlayerMouse);

		//　その他オブジェクト(家具)
		// カーペット
		AddGameObject<Carpet>(Vec3(0.5f, 0.5f, 0.5f), Vec3(0.0f, 0.0f, 0.0f), Vec3(1.3f, -1.1f, 0.5f));
		AddGameObject<Carpet>(Vec3(0.5f, 0.5f, 0.5f), Vec3(0.0f, 0.0f, 0.0f), Vec3(2.7f, -0.5f, 0.0f));
		// クッション
		AddGameObject<Cushion>(Vec3(0.4f, 0.4f, 0.4f), Vec3(0.0f, 0.0f, 0.0f), Vec3(-1.5f, 1.3f, 0.4f), L"Tx_Cushion1");
		AddGameObject<Cushion>(Vec3(0.4f, 0.4f, 0.4f), Vec3(0.0f, 0.0f, 0.0f), Vec3(-2.7f, 0.7f, 0.4f), L"Tx_Cushion2");
	}

	//アイテムのアイコンの作成
	void GameStage::CreateIcon()
	{
		AddGameObject<IconSprite>(Vec3(0.4f, 0.4f, 0), Vec3(-0.8f, -1.5f, 0.0f), WstringKey::tx_UICatnipButton, PlayerStatus::CatnipStatus, WstringKey::tag_UICatnip)->AddTag(WstringKey::tag_Icon);// 仮タグ付け
		AddGameObject<IconSprite>(Vec3(0.4f, 0.4f, 0), Vec3(0.0f, -1.5f, 0.0f), WstringKey::tx_UIMouseRCButton, PlayerStatus::MouseRCStatus, WstringKey::tag_UIMouseRC)->AddTag(WstringKey::tag_Icon);
		AddGameObject<IconSprite>(Vec3(0.4f, 0.4f, 0), Vec3(0.8f, -1.5f, 0.0f), WstringKey::tx_UIYarnBall, PlayerStatus::YarnBallStatus, WstringKey::tag_UIYarnBall)->AddTag(WstringKey::tag_Icon);
		AddGameObject<BackIcon>(Vec3(0.5f, 0.5f, 0), Vec3(3.0f, 1.7f, 0.0f))->AddTag(WstringKey::tag_BackIcon);
	}

	//アイコン背景の作成
	void GameStage::CreateBackGround() {
		auto ItemIB = AddGameObject<IconBackGround>(Vec3(2.5f, 2.5f, 0.0f), Vec3(0.0f, -2.3f, 0.0f));
		ItemIB->AddTag(WstringKey::tag_IconBG);
	}

	// 猫関係
	void GameStage::CreateCat()
	{
		wstring dataDir;
		App::GetApp()->GetDataDirectory(dataDir);
		wstring animDir = dataDir + L"Animations\\";
		//AddGameObject<C>(animDir, L"RussianBule (1).ssae", L"wait", Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, 0.0f, 0.0f));
		auto cat1 = AddGameObject<Cat>(animDir, L"RussianBule3.ssae", L"wait", Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, 0.0f, 0.5f), CatWaitState::Instance());
		SetSharedGameObject(L"Cat1", cat1);
		auto cat2 = AddGameObject<Cat>(animDir, L"RussianBule3.ssae", L"wait", Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(-2.0f, 0.3f, 0.5f), CatWaitState::Instance());
		SetSharedGameObject(L"Cat2", cat2);
	}

	// プレイヤーポインター
	void GameStage::CreatePlayerPointer() {
		wstring dataDir;
		App::GetApp()->GetDataDirectory(dataDir);
		wstring animDir = dataDir + L"Animations\\";
		auto ptrPointer = AddGameObject<PlayerPointer>(Vec3(0.5f, 0.5f, 0.5f), Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, -1.0f, 0.0f), animDir);
		SetSharedGameObject(L"PlayerPointer", ptrPointer);
		//AddGameObject<check>(animDir, Vec3(0.5f, 0.5f, 0.5f), Vec3(0.0f, 0.0f, 0.0f));
		auto ptrMouseRC = AddGameObject<MouseRCInstallation>(Vec3(0.5f), Vec3(-4.0f, 0.0f, 0.0f));
		SetSharedGameObject(L"MouseRC", ptrMouseRC);
	}

	// 猫じゃらし
	void GameStage::CreateCatnip()
	{
		auto ptrCatnip = AddGameObject<Catnip>(Vec3(0.25f, 0.5f, 0.5f), Quat(), Vec3(3.0f, -1.5f, 0.0f));
		SetSharedGameObject(L"Catnip", ptrCatnip);
	}

	// パーティクルの生成
	void GameStage::CreateParticle()
	{
		//気分パーティクル
		auto particlePtr = AddGameObject<MoodParticle>(L"Tx_MoodParticle");
		SetSharedGameObject(L"MoodParticle", particlePtr);
	}

	void GameStage::OnCreate() {
		try {
			//ビューとライトの作成
			CreateViewLight();

			// ランダムな値にするための
			srand((unsigned int)time(NULL));

			// ミッションの設定
			auto gm = GameManager::GetInstance();
			gm->SetCurrentMissionNum(gm->Mission::M1);
			gm->MissionUpdate();

			// スプライトの生成
			CreateSprite();
			// オブジェクトの生成
			CreateGameObject();
			auto ptrDining = AddGameObject<CatDining>(Vec3(0.5f, 0.5f, 0.5f), Quat(), Vec3(-3.0f, -1.5f, 0.5f));
			SetSharedGameObject(L"CatDining", ptrDining);
		}
		catch (...) {
			throw;
		}
	}

	// 更新
	void GameStage::OnUpdate()
	{
		// ミッションゲージの更新
		UpdateMissionGage();
	}

	// ミッションゲージの更新
	void GameStage::UpdateMissionGage()
	{
		auto gm = GameManager::GetInstance();
		gm->UpdateMissionGage(gm->GetCurrentMissionNum());
		if (gm->GetMission() == gm->GetMaxMission())
		{
			if (gm->GetMissionEndFlg())return;
			if (!gm->GetMissionFlg() && !m_OneCheckFlg) {
				// 演出はじめ
				gm->SetMissionFlg(true);
				m_OneCheckFlg = true;
			}
			if (!gm->GetMissionFlg() && m_OneCheckFlg) {
				// 演出終わったら更新
				//一個前のミッションの文字消す
				if (gm->GetCurrentMissionNum() < gm->Mission::M5)
				{
					auto obj = GetSharedObjectGroup(L"MissionStr")->at((size_t)gm->GetCurrentMissionNum());
					obj->SetDrawActive(false);
				}
				gm->AddCurrentMissionNum(1);
				gm->MissionUpdate();
				m_OneCheckFlg = false;
				// 文字呼び出して見えるように
				if (gm->GetCurrentMissionNum() > gm->Mission::M5)return;
				auto missionObj = GetSharedObjectGroup(L"MissionStr")->at((size_t)gm->GetCurrentMissionNum());
				missionObj->SetDrawActive(true);
			}
		}
	}

	//--------------------------------------------------------------------------------------
	//	タイトルステージクラス実体
	//--------------------------------------------------------------------------------------
	void TitleStage::CreateViewLight() {
		const Vec3 eye(0.0f, 0.0f, -5.0f);
		const Vec3 at(0.0f);
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrCamera = ObjectFactory::Create<Camera>();
		PtrView->SetCamera(PtrCamera);
		PtrCamera->SetEye(eye);
		PtrCamera->SetAt(at);
		//マルチライトの作成
		auto PtrMultiLight = CreateLight<MultiLight>();
		//デフォルトのライティングを指定
		PtrMultiLight->SetDefaultLighting();
	}

	// スプライトの生成
	void TitleStage::CreateSprite()
	{
		// 背景
		AddGameObject<BackGround>(true, Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f));
		// タイトルロゴ
		AddGameObject<TitleSprite>(true, Vec2(1024.0f, 512.0f), Vec2(0.0f, 200.0f));
		// スタートボタン
		AddGameObject<StartSprite>(true, Vec3(0.8f), Vec3(0.0f, -0.75f, 0.0f))->AddTag(L"Tag_StartButton");
		// フェード
		auto fade = AddGameObject<Fade>(true, Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f), WstringKey::tx_BG2, 0.0f);
		auto gm = GameManager::GetInstance();
		gm->SetFadeFlg(true);
		gm->SetFadeEndFlg(true);
	}

	// 初期化
	void TitleStage::OnCreate()
	{
		BackUp::GetInstance()->ClearBackUpAll();
		//ビューとライトの作成
		CreateViewLight();
		// スプライトの生成
		CreateSprite();

		// 前ゲームで仲良くなった猫初期化
		auto gm = GameManager::GetInstance();
		gm->ResetCatNum();
		
		wstring dataDir;
		App::GetApp()->GetDataDirectory(dataDir);
		wstring animDir = dataDir + L"Animations\\";
		// 猫
		AddGameObject<Cat>(animDir, L"RussianBule3.ssae", L"wait", Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(-10.0f, 1.0f, 0.0f), CatEnterState::Instance());
		auto ptrPointer = AddGameObject<PlayerPointer>(Vec3(0.5f, 0.5f, 0.5f), Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, -1.0f, 0.0f), animDir);
		SetSharedGameObject(L"PlayerPointer", ptrPointer);
	}

	// 更新
	void TitleStage::OnUpdate()
	{
		if (!m_ChangeFlg) {
			//PushButton();
		}
		else
		{
			auto gm = GameManager::GetInstance();
			for (auto obj : GetGameObjectVec())
			{
				if (auto StartObj = dynamic_pointer_cast<StartSprite>(obj))
				{
					if (StartObj->GetEndFlg())
					{
						if (gm->GetFadeFlg()) {
							gm->SetFadeEndFlg(false);
							gm->SetFadeFlg(false);
						}
						if (gm->GetFadeEndFlg()) {
							PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToLoadStage");
						}
					}
					return;
				}
			}
		}

	}

	// ボタン処理
	void TitleStage::PushButton()
	{
		//コントローラの取得
		auto cntlVec = App::GetApp()->GetInputDevice().GetControlerVec()[0];
		// ボタンの処理
		if (cntlVec.bConnected) {

			if (cntlVec.wPressedButtons & XINPUT_GAMEPAD_A)
			{
				for (auto obj : GetGameObjectVec())
				{
					if (auto StartObj = dynamic_pointer_cast<StartSprite>(obj))
					{
						StartObj->ResetTotalTime();
					}
				}
				m_ChangeFlg = true;
				auto XAPtr = App::GetApp()->GetXAudio2Manager();
				XAPtr->Start(L"se_Button", 0, 0.5f);
			}
		}
	}



	// ゲッター
	// 切り替えフラグ
	bool TitleStage::GetChangeFlg()const
	{
		return m_ChangeFlg;
	}
	void TitleStage::SetChangeFlg(bool setFlg)
	{
		m_ChangeFlg = setFlg;
	}

	//--------------------------------------------------------------------------------------
	//	コピーライトステージクラス実体
	//--------------------------------------------------------------------------------------
	// コピーライトステージからロードステージまでで使うリソースのロード
	void CopyRightStage::LoadResources()
	{
		wstring dataDir;
		// データディレクトリを取得
		App::GetApp()->GetDataDirectory(dataDir);
		wstring textureDir = dataDir + L"Textures\\";
		wstring audioDir = dataDir + L"Audios\\";

		struct TxPair
		{
			wstring Key;
			wstring FileName;
		};

		struct AuPair
		{
			wstring Key;
			wstring FileName;
		};

		// Texture
		TxPair txPairs[] =
		{
			// コピーライトから必要
			{WstringKey::tx_Title, L"nekolife2.png"},
			{WstringKey::tx_StartButton, L"スタートボタン2.png"},
			{L"Tx_White", L"TX_White.png"},
			{WstringKey::tx_BG, L"Background.png"},
			{WstringKey::tx_BG2, L"result_back.png"},
			{WstringKey::tx_CatExclamationMark, L"exclamationmark2.png"},
			{WstringKey::tx_CatExclamationMarkFrame, L"exclamationmark1.png"},
			{WstringKey::tx_Pointer, L"Pointer.png"},
			{WstringKey::tx_DownPointer, L"Pointer2.png"},
			{L"Tx_Cushion1", L"Cushion1.png"},
			{L"Tx_Cushion2", L"Cushion2.png"},
			{L"CopyRight", L"copywrite2.png"}
		};

		//Audio関連
		AuPair auPairs[] =
		{
			// BGM
			{WstringKey::bgm_TitleStage, L"TitleStageBGM.wav"},
			// SE
			{L"nya", L"cat niya-.wav"},
			{L"se_Button", L"Bottan2.wav"}
		};

		for (auto& pair : txPairs) {
			wstring strTexture = textureDir + pair.FileName;
			App::GetApp()->RegisterTexture(pair.Key, strTexture);
		}

		for (auto& pair : auPairs) {
			wstring strAudio = audioDir + pair.FileName;
			App::GetApp()->RegisterWav(pair.Key, strAudio);
		}

	}

	//ビューとライトの作成
	void CopyRightStage::CreateViewLight() {
		const Vec3 eye(0.0f, 0.0f, -5.0f);
		const Vec3 at(0.0f);
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrCamera = ObjectFactory::Create<Camera>();
		PtrView->SetCamera(PtrCamera);
		PtrCamera->SetEye(eye);
		PtrCamera->SetAt(at);
		//マルチライトの作成
		auto PtrMultiLight = CreateLight<MultiLight>();
		//デフォルトのライティングを指定
		PtrMultiLight->SetDefaultLighting();
	}

	// スプライトの生成
	void CopyRightStage::CreateSprite()
	{
		// 背景
		Col4 Black = Col4(0.0f, 0.0f, 0.0f, 1.0f);// 黒
		AddGameObject<Sprite>(true, Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f), L"Tx_White", -1);

		wstring dataDir;
		App::GetApp()->GetDataDirectory(dataDir);
		wstring animDir = dataDir + L"Animations\\";
		//  SpriteStudioの著作権表記
		auto ssSp = AddGameObject<SSCopyRight>(animDir, L"splash1024_instance.ssae", L"in", Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, 0.0f, 0.0f));
		SetSharedGameObject(L"SSCopyRight", ssSp);

		// コピーライト表示
		// とりあえず仮で
		auto sp = AddGameObject<Sprite>(true, Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f), L"CopyRight", 3);
		sp->SetDrawActive(false);
		SetSharedGameObject(L"SpriteCopyRight", sp);

		auto fade = AddGameObject<Fade>(true, Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f), L"Tx_White", 0.0f);
		auto gm = GameManager::GetInstance();
		gm->SetFadeFlg(true);
		gm->SetFadeEndFlg(true);
	}

	// 初期化
	void CopyRightStage::OnCreate()
	{
		// コピーライトステージからロードステージまでで使うリソースのロード
		LoadResources();

		//ビューとライトの作成
		CreateViewLight();
		// スプライトの生成
		CreateSprite();
	}

	//更新
	void CopyRightStage::OnUpdate()
	{
		auto elapsedTime = App::GetApp()->GetElapsedTime();
		auto gm = GameManager::GetInstance();
		auto SSAnim = GetSharedGameObject<SSCopyRight>(L"SSCopyRight");
		// アニメーションが終わったらフェードアウト（1）
		if (!m_SSAnimEnd) {
			if (SSAnim->IsAnimeEnd()) {
				m_SSAnimEnd = true;
				// フェードアウト(1)開始
				gm->SetFadeFlg(false);
				gm->SetFadeEndFlg(false);
			}
		}
		if (!gm->GetFadeFlg() && gm->GetFadeEndFlg())
		{
			if (m_FadeOutNum1End)
			{
				// フェードアウト(2)が終わったら
				// タイトルステージ切り替え
				PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToTitleStage");
			}
			else
			{
				// フェードアウト（1）終わったらコピーライトを見えなくする（このときに別コピーライト表示）
				SSAnim->SetDrawActive(false);
				SSAnim->SetUpdateActive(false);
				auto sp = GetSharedGameObject<Sprite>(L"SpriteCopyRight");
				sp->SetDrawActive(true);
				m_WaitTotalTime += elapsedTime;
				// m_WaitTime分待ったら
				if (m_WaitTotalTime >= m_WaitTime)
				{
					m_WaitTotalTime = 0.0f;
					m_WaitTime = 2.0f;

					// フェードインして別のコピーライト表示
					m_FadeOutNum1End = true;
					gm->SetFadeFlg(true);
					gm->SetFadeEndFlg(false);
				}
			}
		}
		// m_WaitTime分待ったらフェードアウト(2)
		if (m_FadeOutNum1End)
		{

			if (gm->GetFadeFlg() && gm->GetFadeEndFlg())
			{
				m_WaitTotalTime += elapsedTime;
				// m_WaitTime分待ったら
				if (m_WaitTotalTime >= m_WaitTime)
				{
					m_WaitTotalTime = 0.0f;

					// フェードアウト(2)開始
					gm->SetFadeFlg(false);
					gm->SetFadeEndFlg(false);
				}
			}
		}
	}

	//--------------------------------------------------------------------------------------
	//	リザルトステージクラス実体
	//--------------------------------------------------------------------------------------
	//ビューとライトの作成
	void ResultStage::CreateViewLight() {
		const Vec3 eye(0.0f, 0.0f, -5.0f);
		const Vec3 at(0.0f);
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrCamera = ObjectFactory::Create<Camera>();
		PtrView->SetCamera(PtrCamera);
		PtrCamera->SetEye(eye);
		PtrCamera->SetAt(at);
		//マルチライトの作成
		auto PtrMultiLight = CreateLight<MultiLight>();
		//デフォルトのライティングを指定
		PtrMultiLight->SetDefaultLighting();
	}

	// スプライトの生成
	void ResultStage::CreateSprite()
	{
		// 背景
		AddGameObject<BackGround>(true, Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f), WstringKey::tx_BG2);
	}

	// 初期化
	void ResultStage::OnCreate()
	{
		//ビューとライトの作成
		CreateViewLight();
		// スプライトの生成
		CreateSprite();
	}

	// 更新
	void ResultStage::OnUpdate()
	{
		//コントローラの取得
		auto cntlVec = App::GetApp()->GetInputDevice().GetControlerVec()[0];
		// ボタンの処理
		if (cntlVec.bConnected) {

			if (cntlVec.wPressedButtons & XINPUT_GAMEPAD_A)
			{
				PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToGameStage");
			}
			if (cntlVec.wPressedButtons & XINPUT_GAMEPAD_B)
			{
				PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToTitleStage");
			}

		}
	}

	//--------------------------------------------------------------------------------------
	//	ロードステージクラス実体
	//--------------------------------------------------------------------------------------
	// スタティック変数
	bool LoadStage::m_Loaded = false;
	std::mutex LoadStage::mtx;

	// リソースロード用スレッド（スタティック）
	void LoadStage::LoadRecourceFunc()
	{
		mtx.lock();
		m_Loaded = false;
		mtx.unlock();

		// ゲーム内のリソースロード
		wstring dataDir;
		// データディレクトリを取得
		App::GetApp()->GetDataDirectory(dataDir);
		wstring textureDir = dataDir + L"Textures\\";
		wstring audioDir = dataDir + L"Audios\\";

		struct TxPair
		{
			wstring Key;
			wstring FileName;
		};

		struct AuPair
		{
			wstring Key;
			wstring FileName;
		};

		// 仮
		// Texture
		TxPair txPairs[] =
		{
			// ゲームステージ内
			{L"TX_CatVisual", L"visual.png"},
			{L"TX_Catnip", L"IMG_0052.PNG"},
			{WstringKey::tx_MouseRC, L"Mouse.png"},
			{WstringKey::tx_YarnBall, L"Yarnball.png"},
			{WstringKey::tx_CatDining, L"karikari2.png"},
			{L"TX_Dish", L"dish.png"},
			{WstringKey::tx_Pointer, L"Pointer.png"},
			{WstringKey::tx_DownPointer, L"Pointer2.png"},
			{WstringKey::tx_UICatnipButton, L"猫じゃらし.png"},
			{WstringKey::tx_UIMouseRCButton, L"UIMouse.png"},
			{WstringKey::tx_UIYarnBall, L"UIYarnball.png"},
			{WstringKey::tx_BackButton, L"BackButton.png"},
			{WstringKey::tx_IconBackGround, L"IconBackGround.png"},
			{L"TX_Cheek", L"wa.png"},
			{L"Tx_MoodParticle", L"heart.png"},
			{L"TX_MissionGage", L"Gauge5-5psd.png"},
			{L"TX_MissionGageFrame", L"Gauge2.png"},
			{L"TX_MissionGageFrame2", L"Gauge5.png"},
			{L"TX_MissionMark", L"Gauge4.png"},
			{L"TX_MissionStr1", L"misson1.png"},
			{L"TX_MissionStr2", L"misson2.png"},
			{L"TX_MissionStr3", L"misson3.png"},
			{L"TX_MissionStr4", L"misson4.png"},
			{L"TX_MissionStr5", L"misson5.png"}
		};

		//Audio関連
		AuPair auPairs[] =
		{
			// BGM
			{WstringKey::bgm_GameStage, L"GameStageBGM.wav"},
			// SE
			{L"nya", L"cat niya-.wav"},
			{WstringKey::se_Gorogoro, L"gorogoro.wav"},
			{L"se_Catfight", L"Catfight2.wav"},
			{L"karikari", L"CatEating.wav"},
			{WstringKey::se_CatnipSwing, L"Swing.wav"},
			{WstringKey::se_Razikon, L"Razikon1.wav"},
			{WstringKey::se_Yarnball, L"keitodama.wav"},
			{WstringKey::se_Button, L"Select.wav"}
		};

		for (auto& pair : txPairs) {
			wstring strTexture = textureDir + pair.FileName;
			App::GetApp()->RegisterTexture(pair.Key, strTexture);
		}

		for (auto& pair : auPairs) {
			wstring strAudio = audioDir + pair.FileName;
			App::GetApp()->RegisterWav(pair.Key, strAudio);
		}

		mtx.lock();
		m_Loaded = true;
		mtx.unlock();
	}

	//ビューとライトの作成
	void LoadStage::CreateViewLight() {
		const Vec3 eye(0.0f, 0.0f, -5.0f);
		const Vec3 at(0.0f);
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrCamera = ObjectFactory::Create<Camera>();
		PtrView->SetCamera(PtrCamera);
		PtrCamera->SetEye(eye);
		PtrCamera->SetAt(at);
		//マルチライトの作成
		auto PtrMultiLight = CreateLight<MultiLight>();
		//デフォルトのライティングを指定
		PtrMultiLight->SetDefaultLighting();
	}

	// スプライトの生成
	void LoadStage::CreateSprite()
	{
		// 背景
		AddGameObject<BackGround>(true, Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f), WstringKey::tx_BG2);

		// ロードシーン用猫
		wstring dataDir;
		App::GetApp()->GetDataDirectory(dataDir);
		wstring animDir = dataDir + L"Animations\\";
		AddGameObject<LoadCat>(animDir, L"RussianBule3.ssae", L"wait", Vec3(-1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(2.6f, -1.5f, 0.0f));
	}

	// 初期化
	void LoadStage::OnCreate()
	{
		// ゲームステージ内のリソースを読み込むスレッドのスタート
		std::thread LoadThread(LoadRecourceFunc);
		// 終了までは待たない
		LoadThread.detach();

		//ビューとライトの作成
		CreateViewLight();
		// スプライトの生成
		CreateSprite();
	}

	// 更新
	void LoadStage::OnUpdate()
	{
		if (m_Loaded)
		{
			m_TotalTime += App::GetApp()->GetElapsedTime();
			if (m_TotalTime >= m_WaitTime)
			{
				m_TotalTime = 0.0f;
				PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToGameStage");
			}
		}
	}

}
//end basecross
