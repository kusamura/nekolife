/*!
@file 
@brief 
*/
#pragma once
#include "stdafx.h"
namespace basecross {
	//-----------------------------------------------------------------
	//バックアップ
	//ゲームのデータを保存
	//-----------------------------------------------------------------
	class BackUp {
	private:
		BackUp() {}

		~BackUp() {}

		static BackUp* backUp;

		// ゲーム画面で戻るボタン押したときバックアップとったっていうフラグ。
		// とってなかったらデータ使わないように
		bool m_BackUpFlg = false;

		// プレイヤー
		// 位置
		Vec3 m_BackUpPlayerPos;
		// ステート
		shared_ptr<ObjState<Cat>> m_BackUpPlayerState;

		// 猫（二匹）
		// 位置

	public:
		static BackUp* GetInstance();

		// バックアップデータの初期化
		void ClearBackUpAll()
		{
			m_BackUpFlg = false;
			
			// プレイヤーのデータ
			m_BackUpPlayerPos = Vec3(0.0f, -1.0f, 0.0f);
			m_BackUpPlayerState = nullptr;
		}

		// プレイヤーのバックアップデータ
		// セッター
		void SetBackUpPlayerPos(const Vec3& pos)
		{
			m_BackUpPlayerPos = pos;
		}
		// ゲッター
		Vec3 GetBackUpPlayerPos()const
		{
			return m_BackUpPlayerPos;
		}
	};
}

//end basecross