/*!
@file Particle.h
@brief パーティクルクラス
*/

#pragma once
#include "stdafx.h"

namespace basecross {
	// 気分パーティクルクラス
	class MoodParticle : public MultiParticle
	{
		// テクスチャキー
		wstring m_TextureKey;
	public :
		// 構築と破棄
		MoodParticle(shared_ptr<Stage>& StagePtr, const wstring& TxKey);
		virtual ~MoodParticle();

		// 初期化
		virtual void OnCreate()override;
		// 表示
		void InsertMoodParticle(const Vec3& Pos, const Col4& Color = Col4(1.0f, 1.0f, 1.0f, 1.0f));

	};
}

//end basecross

