/*!
@file GameStage.h
@brief ゲームステージ
*/

#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス
	//--------------------------------------------------------------------------------------
	class GameStage : public Stage {
		// 一回だけしかやらない用
		bool m_OneCheckFlg = false;

		//ビューの作成
		void CreateViewLight();
		// スプライトの生成
		void CreateSprite();
		// オブジェクトの生成
		void CreateGameObject();
		//アイテムのアイコンの生成
		void CreateIcon();
		//アイコン背景の生成
		void CreateBackGround();
		// 猫用(猫関係)
		void CreateCat();
		// プレイヤーポインター
		void CreatePlayerPointer();
		// 猫じゃらし
		void CreateCatnip();
		// パーティクルの生成
		void CreateParticle();

		// ミッションゲージの更新
		void UpdateMissionGage();

		// degrad変換
		float ConvertRadian(float deg)
		{
			return deg * (XM_PI / 180.0f);
		}
		float ConvertDegree(float rad)
		{
			return rad * (180.0f / XM_PI);
		}
	public:
		//構築と破棄
		GameStage() :Stage() {}
		virtual ~GameStage() {}
		//初期化
		virtual void OnCreate()override;
		// 更新
		virtual void OnUpdate()override;
	};


	//--------------------------------------------------------------------------------------
	//	タイトルステージクラス
	//--------------------------------------------------------------------------------------
	class TitleStage : public Stage
	{
		// 切り替えフラグ
		bool m_ChangeFlg = false;

		//ビューの作成
		void CreateViewLight();
		// スプライトの生成
		void CreateSprite();

		// ボタン処理
		void PushButton();
	public:
		//構築と破棄
		TitleStage() :Stage() {}
		virtual ~TitleStage() {}
		//初期化
		virtual void OnCreate()override;
		// 更新
		virtual void OnUpdate()override;

		// ゲッター
		// 切り替えフラグ
		bool GetChangeFlg()const;
		void SetChangeFlg(bool setFlg);
	};

	//--------------------------------------------------------------------------------------
	//	コピーライトステージクラス
	//--------------------------------------------------------------------------------------
	class CopyRightStage : public Stage {
		// フェードアウト一回目終了フラグ
		bool m_FadeOutNum1End = false;
		// SSアニメーション終了フラグ
		bool m_SSAnimEnd = false;

		// 待ち時間
		float m_WaitTime = 1.0f;
		float m_WaitTotalTime = 0.0f;

		//ビューの作成
		void CreateViewLight();
		// スプライトの生成
		void CreateSprite();

		// コピーライトステージからロードステージまでで使うリソースのロード
		void LoadResources();
	public:
		//構築と破棄
		CopyRightStage() :Stage() {}
		virtual ~CopyRightStage() {}
		//初期化
		virtual void OnCreate()override;
		// 更新
		virtual void OnUpdate()override;
	};

	//--------------------------------------------------------------------------------------
	//	リザルトステージクラス
	//--------------------------------------------------------------------------------------
	class ResultStage : public Stage {
		//ビューの作成
		void CreateViewLight();
		// スプライトの生成
		void CreateSprite();
	public:
		//	構築と破棄
		ResultStage() : Stage(){}
		virtual ~ResultStage(){}
		// 初期化
		virtual void OnCreate()override;
		// 更新
		virtual void OnUpdate()override;
	};

	//--------------------------------------------------------------------------------------
	// ロードステージクラス
	//--------------------------------------------------------------------------------------
	class LoadStage : public Stage {
		// ロード終わって一定時間待ち
		float m_TotalTime = 0.0f;
		float m_WaitTime = 2.0f;

		//ビューの作成
		void CreateViewLight();
		// スプライトの生成
		void CreateSprite();
		// リソースロード用スレッド（スタティック）
		static void LoadRecourceFunc();
		// ロード終了を知らせるフラグ
		static bool m_Loaded;
		// ミューテックス
		static std::mutex mtx;
	public:
		//	構築と破棄
		LoadStage() : Stage() {}
		virtual ~LoadStage() {}
		// 初期化
		virtual void OnCreate()override;
		// 更新
		virtual void OnUpdate()override;
	};

}
//end basecross

